## Modulo Folha de Ponto

## Passos para rodar o projeto

1. Instale o Vagrant e o VirtualBox.
2. Clone este repositório.
3. Dentro do diretório do projeto execute o comando `vagrant up`.
4. Acesse http://localhost:8000