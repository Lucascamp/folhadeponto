$(document).ready(function($) {
  $('.flashError').fadeOut(0);
  $('.flashSuccess').fadeOut(0);  
  var Funcionarios;
  
  $("#bt_salvar").click(function(event) {
    salvar();
  });

  $('.unidadeFuncionarios').change(function(){
    var unidade = this.value;
    var items;
    
    $('.flashError').fadeOut(500);
    $('.flashSuccess').fadeOut(500);  

    $.post('getfuncionariosunidade2', {unidade: unidade}, function(data) {  
      Funcionarios = data.funcionarios;
      $("#tbodyfuncionarios").empty();
      var table ='';
      var contratadas = data.contratadas;
      var optionsContratadas = '';
      for (var key in contratadas) {
          var obj = contratadas[key];
          optionsContratadas += '<option value="'+key+'">'+obj+'</option>';
      }

      var atividades = data.atividades;
      var optionsatividades = '';
      for (var key in atividades) {
          var obj = atividades[key];
          optionsatividades += '<option value="'+key+'">'+obj+'</option>';
      }

      var centrocusto = data.centrocusto;
      var optionscentrocusto = '';
      for (var key in centrocusto) {
          var obj = centrocusto[key];
          optionscentrocusto += '<option value="'+key+'">'+obj+'</option>';
      }

      $.each(data.funcionarios,function(index,item){
        table += '<tr class="trmultifun">';
        table += '<input type="hidden" value="'+index+'" id="cod_funcionario'+index+'" name="cod_funcionario'+index+'">';
        table += '<td>'+item+'</td>';
        table += '<td><input class="mask-time multifunc form-control" id="hora_entrada'+index+'" size="2" name="hora_entrada'+index+'" type="text"/></td>';
        table += '<td><input class="mask-time multifunc form-control" id="intervalo_inicio'+index+'" size="2" name="intervalo_inicio'+index+'" type="text"/></td>';
        table += '<td><input class="mask-time multifunc form-control" id="intervalo_fim'+index+'" size="2" name="intervalo_fim'+index+'" type="text"/></td>';
        table += '<td><input class="mask-time multifunc form-control" id="hora_saida'+index+'" size="2" name="hora_saida'+index+'" type="text"/></td>';
        table += '<td id="total'+index+'">00:00</td>';
        table += '<td><input class="mask-time multifunc form-control" id="hora_faturada'+index+'" size="2" name="hora_faturada'+index+'" type="text"/></td>';
        table += '<td><select class="multifunc" id="hora_contratada'+index+'" name="hora_contratada'+index+'">'+optionsContratadas+'</select></td>';
        table += '<td><input class="mask-time2 multifunc form-control" size="6" id="tempo_exposicao'+index+'" name="tempo_exposicao'+index+'" type="text"></td>';
        table += '<td><select class="multifunc"id="atividade'+index+'" name="atividade'+index+'">'+optionsatividades+'</select></td>';
        table += '<td><select class="multifunc" id="contab'+index+'" name="contab'+index+'">'+optionscentrocusto+'</select></td>';
        table += '<td><input class="form-control multifunc" placeholder="Observações"  id="observacoes'+index+'" name="observacoes'+index+'" type="text"></td>';
        table += '<td>'+
                    '<div class="btn-group benficios" role="group">'+
                      '<button type="button" id="btbenva'+index+'" class="btn btn-default btn-group-xs" data-toggle="button">Va</button>'+
                      '<button type="button" id="btbenvt'+index+'" class="btn btn-default btn-group-xs" data-toggle="button">Vt</button>'+
                      '<button type="button" id="btbenam'+index+'" class="btn btn-default btn-group-xs" data-toggle="button">Am</button>'+
                    '</div>'+
                  '</td>';
        table += '</tr>';
      });
      $("#tbodyfuncionarios").append(table);
      $(".mask-time").mask("99:99");
      $(".mask-time2").mask("99:99:99");

      
      $.each(data.funcionarios,function(index,item){
        var entrada = $('#hora_entrada'+index).val();
        var intervaloinicio = $('#intervalo_inicio'+index).val();
        var intervalofim = $('#intervalo_fim'+index).val();
        var saida = $('#hora_saida'+index).val();
        $('#hora_entrada'+index).blur(function(event) {
          entrada = $('#hora_entrada'+index).val();
          atualizatotal(index,entrada,intervaloinicio,intervalofim,saida);
        });
        $('#intervalo_inicio'+index).blur(function(event) {
          intervaloinicio = $('#intervalo_inicio'+index).val();
          atualizatotal(index,entrada,intervaloinicio,intervalofim,saida);
        });
        $('#intervalo_fim'+index).blur(function(event) {
          intervalofim = $('#intervalo_fim'+index).val();
          atualizatotal(index,entrada,intervaloinicio,intervalofim,saida);
        });
        $('#hora_saida'+index).blur(function(event) {
          saida = $('#hora_saida'+index).val();
          atualizatotal(index,entrada,intervaloinicio,intervalofim,saida);
        });
        $('#hora_contratada'+index).val(8)
        $('#hora_contratada'+index).change();
      });
    });
  });

  function atualizatotal(id,horaini,interini,interfim,horafim){

    if((horaini == '') || (interini == '') || (interfim == '') || (horafim == '')){
      
    }else{
      var entrada = horaini.split(":");
      var saida   = horafim.split(":");

      var minutosEntrada = parseInt(entrada[0]*60)+parseInt(entrada[1]);
      var minutosSaida   = parseInt(saida[0]*60)+parseInt(saida[1]);

      var intEntrada  = interini;
      var intSaida    = interfim;

      if(intSaida == '00:00')
        intSaida = '24:00'; 

      if(intEntrada == '00:00')
        intEntrada = '24:00';     

      var intEntrada = intEntrada.split(":");
      var intSaida   = intSaida.split(":");

      var minutosIntEntrada = parseInt(intEntrada[0]*60)+parseInt(intEntrada[1]);
      var minutosIntSaida   = parseInt(intSaida[0]*60)+parseInt(intSaida[1]);

      if(intEntrada == intSaida)
        var intervalo = 0;
      else
        var intervalo = Math.abs(minutosIntEntrada - minutosIntSaida);

      if(minutosSaida - minutosEntrada > 0)
        var diff = Math.abs(minutosSaida - minutosEntrada - intervalo);
      else
        var diff = Math.abs(1440 - minutosEntrada + minutosSaida - intervalo);

      var hours     =  Math.floor(diff/60);
      var minutes   = (diff%60);

      if(hours < 9 && minutes <9)
        var total = "0"+hours +":0"+minutes;
      else if(hours < 9)
        var total = "0"+hours +":"+minutes;
      else if(minutes < 9)
        var total = hours +":0"+minutes;
      else
        var total = hours +":"+minutes;      
      $('#total'+id).text(total);
    }
  }

  function salvar(){
    var dia = $("#data").val();
    var unidade = $("#unidade").val();
    var created_by = $("#created_by").val();
    var mensagem = '';
    var mensagemErro = '';
    var espaco = '';

    $.each(Funcionarios,function(index,item){
      var cod_funcionario   = $("#cod_funcionario"+index).val();
      var hora_entrada      = $("#hora_entrada"+index).val();
      var hora_saida        = $("#hora_saida"+index).val();
      var intervalo_inicio  = $("#intervalo_inicio"+index).val();
      var intervalo_fim     = $("#intervalo_fim"+index).val();
      var hora_faturada     = $("#hora_faturada"+index).val();
      var hora_contratada   = $("#hora_contratada"+index).val();
      var cod_item_contab   = $("#contab"+index).val();
      var atividade         = $("#atividade"+index).val();
      var tempo_exposicao   = $("#tempo_exposicao"+index).val();
      var observacoes       = $("#observacoes"+index).val();
      var beneficios = [];
      
      if($("#btbenva"+index).hasClass('active')){
        beneficios[beneficios.length] = 'va';
      }
      if($("#btbenvt"+index).hasClass('active')){
        beneficios[beneficios.length] = 'vt';
      }
      if($("#btbenam"+index).hasClass('active')){
        beneficios[beneficios.length] = 'am';
      }

      var ben = beneficios.toString();

      $.post('/horarios/variosfuncionarios2', {
        data                : dia,
        unidade             : unidade,
        created_by          : created_by,
        cod_funcionario     : cod_funcionario,
        hora_entrada        : hora_entrada,
        hora_saida          : hora_saida ,     
        intervalo_inicio    : intervalo_inicio,
        intervalo_fim       : intervalo_fim,
        hora_faturada       : hora_faturada,
        hora_contratada     : hora_contratada,
        cod_item_contab     : cod_item_contab,
        atividade           : atividade,
        tempo_exposicao     : tempo_exposicao,
        observacoes         : observacoes,
        beneficios          : ben,
      }, function(data) { 

        espaco += '<br>';

        if (data.success)
        {
          mensagem+=item+' Marcação criada com sucesso! <br>';
        }
        else if(data.exist)
        {
          mensagemErro+=item+' Já cadastrado para esse dia! <br>';
        }
        else if(data.superior)
        {
          mensagemErro+=item+' Superior ao período vigente! <br>';
        }
        else
        {
          mensagemErro+=item+' Dados Insuficientes para marcação! <br>';
        }
        
        
        $("#tbodyfuncionarios").empty();
        $('#unidade').val('');
        $("#unidade").trigger("chosen:updated");

        $("#espaco").html(espaco);
        $('#flashSuccess').html(mensagem).fadeIn(100);
        $('#flashError').html(mensagemErro).fadeIn(100);

      });
    });

   
  }
});