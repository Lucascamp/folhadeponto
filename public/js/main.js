var $ = jQuery.noConflict();
//main function
jQuery(function($){ 
    $(document).ready(function($) {
    
    });
  /*Masks*/
  $(".mask-date").mask("99/99/9999");
  $(".mask-time").mask("99:99");
  $(".mask-money").mask("9999.99");
  $(".mask-time2").mask("99:99:99");

  /*Select Funcionario*/    
  $('.chosen-select').chosen({
    no_results_text: "Nenhum resultado encontrado",
    width:"80%"
  }).addClass('form-control');


  /*Menu App*/

  $('#activator').click(function(){$('#box').animate({'top':'66px'},500);});

  $('#login_abrir') .click(function(){$('#menu_login').animate({'top':'-60px'},500);});
  $('#login_fechar').click(function(){$('#menu_login').animate({'top':'-300px'},500);	});

  $('#menu_login').animate({'top':'-300px'},1800);

  $('#menuopcoes_abrir').click(function(){$('#menuopcoes').animate({'top':'65px'},500);});
  $('#menuopcoes_fechar').click(function(){$('#menuopcoes').animate({'top':'-400px'},500);});

  $('#horarios_abrir').click(function(){$('#menuhorarios').animate({'top':'65px'},500);});
  $('#horarios_fechar').click(function(){$('#menuhorarios').animate({'top':'-400px'},500);});

  $(".toggle_container").hide(); 
  $(".trigger").click(function(){
   $(this).toggleClass("active").next().slideToggle("slow");
   return false;
 });

  /*Valida ponto esconde e mostra campo de motivo*/
  function getNum(element, attrPrefix) {
    //set prefix, get number
    var prefix = attrPrefix;
    var num = element.attr("id").substring((prefix.length));
    return num;
  }

  $(".validar1:checkbox").change(function() {
    var num = getNum($(this), "id1_");
    if ($(this).is(":checked")) {
      $("#div1_" + num).fadeOut();
    } else {
      $("#div1_" + num).fadeIn();
    }
  });

  $(".validar2:checkbox").change(function() {
    var num = getNum($(this), "id2_");
    if ($(this).is(":checked")) {
      $("#div2_" + num).fadeOut();
    } else {
      $("#div2_" + num).fadeIn();
    }
  });
  
  $(".motivo").hide();

  /*Seta os campos do fomulario caso seja necessario inserir valores*/
  $("#atividade").change(function() {

  $value = $('option:selected', $(this)).val();

  if ($value == 'Falta' || $value == 'Folga' || $value == 'Férias' || $value == 'Atestado' || $value == 'Exame Médico'
      || $value == 'Afastado' || $value == 'Demitido' || $value == 'Treinamento' || $value == 'Suspenso') {
    $('#hora_entrada').val('00:00');
    $('#intervalo_inicio').val('00:00');
    $('#intervalo_fim').val('00:00');
    $('#hora_saida').val('00:00');
    $('#hora_faturada').val('00:00');

    $('#hora_contratada').val(0);
    $("#hora_contratada").trigger("chosen:updated");

    $('#somahoras').html('00:00');
    $("#tempo_exposicao").fadeOut();
    $("#divhorario").fadeOut();
      if($value == 'Atestado' || $value == 'Exame Médico') {
        $("#divcontratada").fadeIn();
      }else{
        $("#divcontratada").fadeOut();
      }
  } else {
    $('#hora_entrada');
    $('#intervalo_inicio');
    $('#intervalo_fim');
    $('#hora_saida');
    $('#hora_faturada');
    $("#divhorario").fadeIn();
    $("#divcontratada").fadeIn();
    $("#tempo_exposicao").fadeIn();
  }
});

  /*Função para impressão*/
	$('#imprimir').on('click', function(){// busca os dados ao clicar no botão
		$(".area_impressao").printArea({
      mode       : "iframe",
      standard   : "html5",
      popTitle   : 'relatorio',
      popClose   : false,
      extraCss   : 'css/impressao_paisagem.css',
      extraHead  : '',
      retainAttr : ["id","class","style"],
            printDelay : 1000, // tempo de atraso na impressao
            printAlert : true,
            printMsg   : 'Aguarde a impressão'
          });
  });

  $('#dvMessage').hide();
  $('#dvLoading').hide();
  $(".mensagem_aguarde").on('click', function(){
  $('#dvLoading').fadeIn(500);
  $('#dvMessage').fadeIn(2000);
  $('#screen').css({ opacity: 0.7, 'width':$(document).width(),'height':$(document).height()});
  });

  /* exportar excel cristiano */
  $("#exporta_excel_xls").on('click', function(){                    
  var data='<table>'+$(".area_impressao").html().replace(/<a\/?[^>]+>/gi, '')+'</table>';
  $('body').prepend("<form method='post' action='/libraries/exporta_excel.php' style='display:none' id='ReportTableData'><input type='text' name='tableData' value='"+data+"' ></form>");
   $('#ReportTableData').submit().remove();
   return false;
  });

  $(".ferias:checkbox").change(function() {
    if ($(this).is(":checked")) 
      $("#divferias").fadeIn(300);
    else 
      $("#divferias").fadeOut(300);
  });
  $("#divferias").hide();

            //   $('#cod_funcionario').change(function(){
            //   var cod_func = this.value;
            //   var allVals =  [];

            //   $('#beneficio_tipo_vt').prop('checked', false);
            //   $('#beneficio_tipo_va').prop('checked', false);
            //   $('#beneficio_tipo_am').prop('checked', false);

            //   $.post('checkbox', {cod_funcionario: cod_func}, function(response) {      

            //     var checkboxes = '';
            //     $.each(response, function(index, value){
            //       if(value=='vt')
            //         $('#beneficio_tipo_vt').prop('checked', true);
            //       if(value=='va')
            //         $('#beneficio_tipo_va').prop('checked', true);
            //       if(value=='am')
            //         $('#beneficio_tipo_am').prop('checked', true);
            //     });

            //   $("input[type='checkbox'][name^='beneficio_tipo']:checked").each(function() {
            //         allVals.push($(this).val()); 
            //       }); 
            //       $("input[name='beneficios']").val(allVals);

            //   })
              
            // });

    $('#cod_funcionario').change(function(){
        var cod_func = this.value;
        var allVals =  [];

        $('#beneficio_tipo_vt').prop('checked', false);
        $('#beneficio_tipo_va').prop('checked', false);
        $('#beneficio_tipo_am').prop('checked', false);

        $.post('getdadosmarcacao', {cod_funcionario: cod_func}, function(response) {      

        var checkboxes = '';
        var beneficios = response.beneficios;
        var un = response.unidade;
        beneficios.forEach(function(entry) {
            if(entry=='vt')
            $('#beneficio_tipo_vt').prop('checked', true);
            if(entry=='va')
            $('#beneficio_tipo_va').prop('checked', true);
            if(entry=='am')
            $('#beneficio_tipo_am').prop('checked', true);
        });

        $("input[type='checkbox'][name^='beneficio_tipo']:checked").each(function() {
        allVals.push($(this).val()); 
        }); 
        $("input[name='beneficios']").val(allVals);
        $("#unidade").val(un).change();
        $("#unidade_chosen span").text(un);
        console.log($("#unidade").val());
        })
       
    });

  $('.unidadeFuncionarios').change(function(){
    var unidade = this.value;
    var items;

    $.post('getfuncionariosunidade', {unidade: unidade}, function(data) {  

      $.each(data,function(index,item){
        items+="<option value='"+index+"'>"+item+"</option>";
      });

      $("#funcionariosUnidade").html(items); 

      $("#funcionariosUnidade").trigger("chosen:updated");
    });
  });


  /*salva os checks no input beneficios*/    
    $("#beneficios-inputs .beneficio").click(function(){
        var allVals =  [];
      $("input[type='checkbox'][name^='beneficio_tipo']:checked").each(function() {
       allVals.push($(this).val()); 
     });  
      $("input[name='beneficios']").val(allVals);
    });

    /*datepicker*/
    $('.datepicker').datepicker({format: 'dd/mm/yyyy', autoclose: true}).on('changeDate', function(ev){
      if('#data'){
        var date = $('#data').val();
        var day = date.substr(0,5);
        var holidays = [];
        
        var unit = $('#unidade').val();
        
      $.post('holiday', {unidade: unit}, function(response) {      
        $.each(response, function(index, value){
          holidays.push(value);
        });
        
      if (jQuery.inArray(day, holidays) != -1)
          $('#atividade option[value="Feriado"]').prop('selected', 'selected');
        else
          $('#atividade option[value="Hora Direta"]').prop('selected', 'selected');

      });
    }

  });

  //DESATIVA O ENTER, PASSANDO PARA O PROXIMO CAMPO A ESQUERDA
  $('input:text').bind("keydown", function(e) {
    var n = $("input:text").length;
    if (e.which == 13){ //Enter key
      e.preventDefault(); //Skip default behavior of the enter key
      var nextIndex = $('input:text').index(this) + 1;
      if(nextIndex < n)
        $('input:text')[nextIndex].focus();
        else{
          $('input:text')[nextIndex-1].blur();
          }
    }
  });

  $("#hora_saida").focusout(function(){
    var entrada = $('#hora_entrada').val();
    var saida   = $('#hora_saida').val();

    var entrada = entrada.split(":");
    var saida   = saida.split(":");

    var minutosEntrada = parseInt(entrada[0]*60)+parseInt(entrada[1]);
    var minutosSaida   = parseInt(saida[0]*60)+parseInt(saida[1]);

    var intEntrada  = $('#intervalo_inicio').val();
    var intSaida  = $('#intervalo_fim').val();

    if(intSaida == '00:00')
      intSaida = '24:00'; 

    if(intEntrada == '00:00')
      intEntrada = '24:00';     

    var intEntrada = intEntrada.split(":");
    var intSaida   = intSaida.split(":");

    var minutosIntEntrada = parseInt(intEntrada[0]*60)+parseInt(intEntrada[1]);
    var minutosIntSaida   = parseInt(intSaida[0]*60)+parseInt(intSaida[1]);

    if(intEntrada == intSaida)
      var intervalo = 0;
    else
      var intervalo = Math.abs(minutosIntEntrada - minutosIntSaida);

    if(minutosSaida - minutosEntrada > 0)
      var diff = Math.abs(minutosSaida - minutosEntrada - intervalo);
    else
      var diff = Math.abs(1440 - minutosEntrada + minutosSaida - intervalo);

    var hours     =  Math.floor(diff/60);
    var minutes   = (diff%60);

    if(hours < 9 && minutes <9)
      var total = "0"+hours +":0"+minutes;
    else if(hours < 9)
      var total = "0"+hours +":"+minutes;
    else if(minutes < 9)
      var total = hours +":0"+minutes;
    else
      var total = hours +":"+minutes;

    $('#somahoras').html(total);
  });

  $("#observacoes").hide();
  $('#obs').on('click', function(){
      $("#observacoes").show();
  });

  $("#imgexportar").hide();
  $('#exportar').on('click', function(){
       $("#exportar").hide();
       $("#imgexportar").fadeIn(500);
       $("#imgexportar").delay(32300).fadeOut();
  });


  $('.page-alert').hide();
    //Show alert
    $('button[data-toggle="page-alert"]').click(function(e) {
        e.preventDefault();
        var id = $(this).attr('data-toggle-id');
        var alert = $('#alert-' + id);
        var timeOut;
        alert.appendTo('.page-alerts');
        alert.slideDown();
        
        //Is autoclosing alert
        var delay = $(this).attr('data-delay');
        if(delay != undefined)
        {
            delay = parseInt(delay);
            clearTimeout(timeOut);
            timeOut = window.setTimeout(function() {
                    alert.slideUp();
                }, delay);
        }
    });
    
    //Close alert
    $('.page-alert .close').click(function(e) {
        e.preventDefault();
        $(this).closest('.page-alert').slideUp();
    });

    $('.time').click(function(){
      var id = $(this).attr('id');
      var dt = new Date();
      
      if(dt.getMinutes() < 10)
        var minutes = '0'+dt.getMinutes();
      else
        var minutes = dt.getMinutes();
      
      var time = dt.getHours() + ":" + minutes;    

      $('#input_'+id).val(time);  
    });


    $('.addBancoBtn').click(function() {
      $("#add_to_funcionario").val($(this).attr('data_value'));
    });

    $('.subBancoBtn').click(function() {
      $("#sub_from_funcionario").val($(this).attr('data_value'));
    });

    $('.legenda').tooltip('hide');

    $('[data-tooltip="tooltip"]').tooltip('hide');


    $('.fecharperiodo').click(function() 
    {
      alert('Não se esqueça de abrir o proximo período');
    });

    $('.validadores').click(function() {
      var id = $(this).attr('id');
      var usuario = $("#usuario"+id).val();
      var funcionarios = $("#funcionarios"+id).val();

      $.post('salvarValidadores', { usuario : usuario, funcionarios : funcionarios });
    });

});//end main

 function excluir(id, $tr) 
 { 
  var mensagem = $('<div></div>');
  mensagem.append('<b>Você tem certeza que deseja excluir o registro '+id+'?');    

  BootstrapDialog.show({
    title: 'EXCLUIR REGISTRO',
    message: mensagem,
    closable: false,
    type: 'type-danger',
    buttons: [{label: 'EXCLUIR',
    cssClass: 'btn-danger',
    action: function(dialogItself){
      $.post('horarios/'+ id, {
        _method: 'DELETE',
      }, function(){                            
        $tr.hide('slow');
      });

      dialogItself.close();                        
    }
  }, {label: 'Cancelar',
  action: function(dialogItself){
    dialogItself.close();
  }
}]
});
}




