$(document).ready(function($) {
	var datai ='';
	var dataf ='';

	$("#dataini").click(function(event) {
		datai = '';
		dataf = '';
	});
	$("#datafim").click(function(event) {
		datai = '';
		dataf = '';
	});

	
	$("#Periodos").chosen().change(function(event) {
		var Periodo   = $("#Periodos").val();
		$.post(
			'/relatorios/marcacoesgetdatas',
			{
				periodo : Periodo,
			},
			function(data){
				if(data.success){
					datai = data.datainicio;
					dataf = data.datafim;
					var Ai = datai.substr(0, 4);
					var Mi = datai.substr(5, 2);
					var Di = datai.substr(8, 2);
					var strdatai = Di+'/'+Mi+'/'+Ai;
					$("#dataini").val(strdatai);
					
					var Af = dataf.substr(0, 4);
					var Mf = dataf.substr(5, 2);
					var Df = dataf.substr(8, 2);
					var strdataf = Df+'/'+Mf+'/'+Af;
					$("#datafim").val(strdataf);
				}
			},
			'json'
		);
	});

	$("#bt-gerarrelatorio").click(function(event) {
		carregadados();
	});


function carregadadosporun(){
	if($("#dataini").val() != '' || $("#datafim").val() != ''){
		var dtini;
		var dtfim;
		if(datai != ''){
			dtini = datai;	
		}else{
			dti = $("#dataini").val();
			var Ai = dti.substr(6, 4);
			var Mi = dti.substr(3, 2);
			var Di = dti.substr(0, 2);
			dtini= Ai+'-'+Mi+'-'+Di+' 00:00:00';
		}
		if(dataf != ''){
			dtfim = dataf;	
		}else{
			dtf = $("#datafim").val();
			var Af = dtf.substr(6, 4);
			var Mf = dtf.substr(3, 2);
			var Df = dtf.substr(0, 2);
			dtfim = Af+'-'+Mf+'-'+Df+' 00:00:00';
		}
		$.post(
			'/relatorios/marcacoesporunidade',
			{
				dtini   : dtini,
				dtfim   : dtfim
			},
			function(data){
				if(data.success){
					var table = '';
					table += '<tr><td colspan="2"></td></tr><tr><td colspan="2">Total por unidade</td></tr><tr>'+
								'<th width="10%">Unidade</th>'+
							    '<th width="10%">Somatório</th>'+
							' </tr>';
					var horarios = data.horarios;
					horarios.forEach(function(element, index){
						table += '<tr>'+
								'<td>'+element.unidade+'</td>'+
								'<td>'+element.somatorio+'</td>'+
							'</tr>';
					});
					$("tfoot").append(table);
				}
			},
			'json'
		);
	}else{
	
	}
}


function carregadados(){
	var un = $("#UN").val();
		if($("#dataini").val() != '' || $("#datafim").val() != ''){
			var dtini;
			var dtfim;
			if(datai != ''){
				dtini = datai;	
			}else{
				dti = $("#dataini").val();
				var Ai = dti.substr(6, 4);
				var Mi = dti.substr(3, 2);
				var Di = dti.substr(0, 2);
				dtini= Ai+'-'+Mi+'-'+Di+' 00:00:00';
			}
			if(dataf != ''){
				dtfim = dataf;	
			}else{
				dtf = $("#datafim").val();
				var Af = dtf.substr(6, 4);
				var Mf = dtf.substr(3, 2);
				var Df = dtf.substr(0, 2);
				dtfim = Af+'-'+Mf+'-'+Df+' 00:00:00';
			}

			$.post(
				'/relatorios/marcacoes2',
				{
					un      : un,
					dtini   : dtini,
					dtfim   : dtfim
				},
				function(data){
					if(data.success){
						var datainicio = data.datainicio.date;
						// var datafinal = data.datafinal.date;
						var Ai = datainicio.substr(0, 4);
						var Mi = datainicio.substr(5, 2);
						var Di = datainicio.substr(8, 2);
						// var Af = datafinal.substr(0, 4);
						// var Mf = datafinal.substr(5, 2);
						// var Df = datafinal.substr(8, 2);

						datainicio = Di+'/'+Mi+'/'+Ai;
						// datafinal  = Df+'/'+Mf+'/'+Af;
						var table = '';
						table += '<div class="area_impressao" name="area_impressao" id="area_impressao">'+
								'<table width="100%" height="73" class="tablefolha">'+
									'<tr>'+
										'<td width="50%" align="center"><center><b><font size="5" face="Arial">Marcações por dia no Período</font></b></center>';
										
						if(un != 0){
							table += '<right>Unidade: '+un+' - Período: de '+$("#dataini").val()+' a '+$("#datafim").val()+'</right></td>';
						}else{table += '<right>Período: de '+$("#dataini").val()+' a '+$("#datafim").val()+'</right></td>';}
						table += '</tr>'+
								'</table>'+

									'<div class="clear"><br></div>'+

									'<div width="100%">	'+
										'<div width="50%" align="left">'+
											'<table width="20%" class="zebra3" align="left">'+
											   ' <thead>'+
											        '<tr>'+
											        	'<th width="10%">Data</th>'+
											           ' <th width="10%">Somatório</th>'+
											       ' </tr>'+
											    '</thead>'+
											    '<tbody>';
						var horarios = data.horarios;
						var i = 0;
						var cont = horarios.length;
						var data1 = SomarData(datainicio,1);
						while(i < cont){
							if(datainicio == horarios[i].data){
								table += '<tr>'+
											'<td>'+horarios[i].data+'</td>'+
											'<td>'+horarios[i].somatorio+'</td>'+
										'</tr>';
								i++;
							}else{
								table += '<tr>'+
											'<td>'+datainicio+'</td>'+
											'<td>-</td>'+
										'</tr>';
							}
							datainicio = SomarData(datainicio,1);
						}
						table += '</tbody>'+
								 '<tfoot>'+
								 '</tfoot>'+
											'</table>'+
										'</div>'+
										'<div width="50%" align="center">		'+
											'<canvas id="daily-reports" width="900" height="400"></canvas>		'+
											'<div id="fim"></div>'+
										'</div>'+
									'</div>'+

								'</div>	'+
								'<div class="clear"><br></div>'+
								'<div class="clear"><br></div>'
						$("#relatorio").empty();
						$("#relatorio").append(table);
						
						carregadadosporun();
					
						var ctx = document.getElementById("daily-reports").getContext("2d");
						var chart = {
							labels: data.datasJson,
							datasets: [{
								data: data.somatorioJson,
								fillColor : "#0DA31F",
								strokeColor : "#000000",
								pointColor : "#150EE3",
							}],
						};
						new Chart(ctx).Line(chart, { bezierCurve:false });
						$('html, body').animate({
						    scrollTop: $("#fim").offset().top
						}, 1500);
					}else{
						$("#relatorio").empty();
						$("#relatorio").append('<div class="alert alert-danger alert-dismissable">'+
			                                        '<center>'+data.mensagem+'</center>'+
			                                    '</div>');
					}
				},
				'json'
			);
		}else{
			$("#relatorio").empty();
			$("#relatorio").append('<div class="alert alert-danger alert-dismissable">'+
                                        '<center>Escolha um periodo ou um intervalo</center>'+
                                    '</div>');
		}
}
	function SomarData(txtData,DiasAdd){
		var d = new Date();
		d.setTime(Date.parse(txtData.split("/").reverse().join("/"))+(86400000*(DiasAdd)))
		var DataFinal;	
		if(d.getDate() < 10){
			DataFinal = "0"+d.getDate().toString()+"/";
		}
		else{	
			DataFinal = d.getDate().toString()+"/";	
		}
		if((d.getMonth()+1) < 10){
			DataFinal += "0"+(d.getMonth()+1).toString()+"/"+d.getFullYear().toString();
		}
		else{
			DataFinal += (d.getMonth()+1).toString()+"/"+d.getFullYear().toString();
		}
		return DataFinal;
	}
	function SomarData2(txtData,DiasAdd){
		var d = new Date();
		d.setTime(Date.parse(txtData.split("/").join("/"))+(86400000*(DiasAdd)))
		var DataFinal = '';	
		if((d.getMonth()+1) < 10){
			DataFinal += d.getFullYear().toString()+"-"+"0"+(d.getMonth()+1).toString()+"-";
		}
		else{
			DataFinal += d.getFullYear().toString()+"-"+(d.getMonth()+1).toString()+"-";
		}
		if(d.getDate() < 10){
			DataFinal += "0"+d.getDate().toString()+" 00:00:00.000000";
		}
		else{	
			DataFinal += d.getDate().toString()+" 00:00:00.000000";	
		}
		
		return DataFinal;
	}
});