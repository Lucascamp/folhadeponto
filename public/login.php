<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale = 1.0" />
<meta name="author" content="Compvix - Soluções em Teclologia e Cristiano Guilherme - www.cristianoguilherme.com" />
<meta name="description" content="Sistema de Programação de RX | Módulo Ponto" />
<title>Sistema RX | Módulo Ponto</title>
<style>
html{width:100%;height:100%;background:url(images/bg.jpg) repeat #a3a4a6;}
body {background:url("images/top_bg.jpg") no-repeat top center;margin:0px auto;padding:0px;font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#423932;width:100%;height:100%;}
ol, ul {
  list-style: none;
}
/*
 * Copyright (c) 2012-2013 Thibaut Courouble
 * http://www.cssflow.com
 *
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 */
.login-submit, .login-submit:before, .login-submit:after {
  background: #a3a4a6  0 0 repeat;
}
.login {
  position: relative;
  margin: 80px auto;
  width: 400px;
  padding-right: 32px;
  font-weight: 300;
  color: #fff;
  text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.8);
}
.login p {
  margin: 0 0 10px;
}

input, button, label {
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 15px;
  font-weight: 300;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

input[type=text], input[type=password] {
  padding: 0 10px;
  width: 300px;
  height: 40px;
  color: #bbb;
  text-shadow: 1px 1px 1px black;
  background: rgba(0, 0, 0, 0.16);
  border: 0;
  border-radius: 5px;
  -webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.06);
  box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.06);
}
input[type=text]:focus, input[type=password]:focus {
  color: white;
  background: rgba(0, 0, 0, 0.1);
  outline: 0;
}

label {
  float: left;
  width: 100px;
  line-height: 40px;
  padding-right: 10px;
  font-weight: 100;
  text-align: right;
  letter-spacing: 1px;
}

.forgot-password {
  padding-left: 100px;
  font-size: 13px;
  font-weight: 100;
  letter-spacing: 1px;
}
.login-submit {
  position: absolute;
  top: 12px;
  right: 0;
  width: 48px;
  height: 48px;
  padding: 8px;
  border-radius: 32px;
}
.login-submit:before, .login-submit:after {
  content: '';
  z-index: 1;
  position: absolute;
}
.login-submit:before {
  top: 28px;
  left: -4px;
  width: 4px;
  height: 10px;
  -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.06);
  box-shadow: inset 0 1px rgba(255, 255, 255, 0.06);
}
.login-submit:after {
  top: -4px;
  bottom: -4px;
  right: -4px;
  width: 36px;
}
.login-button {
  position: relative;
  z-index: 2;
  width: 48px;
  height: 48px;
  padding: 0 0 48px;
  /* Fix wrong positioning in Firefox 9 & older (bug 450418) */
  text-indent: 120%;
  white-space: nowrap;
  overflow: hidden;
  background: none;
  border: 0;
  border-radius: 24px;
  cursor: pointer;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.2), 0 1px rgba(255, 255, 255, 0.1);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.2), 0 1px rgba(255, 255, 255, 0.1);
  /* Must use another pseudo element for the gradient background because Webkit */
  /* clips the background incorrectly inside elements with a border-radius.     */
}
.login-button:before {
  content: '';
  position: absolute;
  top: 5px;
  bottom: 5px;
  left: 5px;
  right: 5px;
  background: #00a2d3;
  border-radius: 24px;
 
}
.login-button:active:before {
  background: #0591ba;
  background-image: -webkit-linear-gradient(top, #0591ba, #00a2d3);
  background-image: -moz-linear-gradient(top, #0591ba, #00a2d3);
  background-image: -o-linear-gradient(top, #0591ba, #00a2d3);
  background-image: linear-gradient(to bottom, #0591ba, #00a2d3);
}
.login-button:after {
  content: '';
  position: absolute;
  top: 15px;
  left: 12px;
  width: 25px;
  height: 19px;
  background: url("images/arrow.png") 0 0 no-repeat;
}

::-moz-focus-inner {
  border: 0;
  padding: 0;
}

.lt-ie9 input[type=text], .lt-ie9 input[type=password] {
  line-height: 40px;
  background: #282828;
}
.lt-ie9 .login-submit {
  position: absolute;
  top: 12px;
  right: -28px;
  padding: 4px;
}
.lt-ie9 .login-submit:before, .lt-ie9 .login-submit:after {
  display: none;
}
.lt-ie9 .login-button {
  line-height: 48px;
}
.lt-ie9 .about {
  background: #313131;
}
.logo{width:220px; margin:auto; height:192px;}
</style>

  </head>
<body>
<?php include("./comandos.php"); ?>

<div class="logo">
	<a href="index.html"><img src="images/logo.png" alt="" title="" border="0" /></a>
</div> 

 <form method="post" action="?cmd=logar" class="login">
    <p>
		<label for="login">Login:</label>
		<input type="text" name="login" id="login" value="">
    </p>
    <p>
		<label for="password">Senha:</label>
		<input type="password" name="senha" id="senha" value="">
    </p>
		<input type="hidden" name="idioma" id="idioma" value="ptbr">
    <p class="login-submit">
      <button type="submit" class="login-button">Login</button>
    </p>
  </form>
 
 <div class="login" id="marcadagua">
   <img src="images/img_marcadagua.png" alt="" title="" border="0" />
 </div>

</body>
</html>
