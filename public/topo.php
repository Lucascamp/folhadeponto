<?php include("../RXMOBILE/comandos.php"); 
//checa_login(); 
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('America/Sao_Paulo');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
<meta name="viewport" content="initial-scale = 1.0" />
<meta name="author" content="Compvix - Solu��es em Teclologia e Cristiano Guilherme - www.cristianoguilherme.com" />
<meta name="description" content="Sistema de Programa��o de RX | M�dulo Ponto" />
<title>Sistema RX | M�dulo Ponto</title>

<!-- Tema principal -->
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />

	
<!-- Plugin JQuery -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- Plugin Valida��o para input -->
<script type="text/javascript" src="js/jquery.validate.js"></script>
<!-- Plugin Formato de Pre�o para input -->
<script type="text/javascript" src="js/jquery.price_format.1.4.js"></script>
<!-- Plugin Masked Input -->
<script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
<!-- Plugin PrintArea -->
<script src="js/jquery.PrintArea.js" type="text/javascript"></script>
<!-- Plugin chosen select to autocomplete -->
<script src="./lib/chosen/chosen.jquery.js" type="text/javascript"></script>
<link href="./lib/chosen/chosen.css" rel="stylesheet" type="text/css" />

	
	<!-- Plugin Bootstrap Twitter Modal Dialog -->
	<link href="./lib/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
	<script src="./lib/bootstrap/bootstrap.min.js"></script>
	<link href="./lib/bootstrap/bootstrap-dialog.css" rel="stylesheet" type="text/css" />
	<script src="./lib/bootstrap/bootstrap-dialog.js"></script>


<script type="text/javascript">
function AjustaLargura() {
	$('#main_container').css('width',$(window).width);
}
// jQuery shorthand for document.onload

var $ = jQuery.noConflict();
	$(function() {
		$('#activator').click(function(){
				$('#box').animate({'top':'66px'},500);
		});
		$('#boxclose').click(function(){
				$('#box').animate({'top':'-400px'},500);
		});
		$('#menuopcoes_abrir').click(function(){
				$('#menuopcoes').animate({'top':'65px'},500);
		});
		$('#menuopcoes_fechar').click(function(){
				$('#menuopcoes').animate({'top':'-400px'},500);
		});
	
	});

	$(document).ready(function(){
		$(".toggle_container").hide(); 
		$(".trigger").click(function(){
			$(this).toggleClass("active").next().slideToggle("slow");
			return false;
		});
	//DESATIVA O ENTER, PASSANDO PARA O PROXIMO CAMPO A ESQUERDA
	
	$('input:text').bind("keydown", function(e) {
		var n = $("input:text").length;
		if (e.which == 13){ //Enter key
			e.preventDefault(); //Skip default behavior of the enter key
			var nextIndex = $('input:text').index(this) + 1;
			if(nextIndex < n)
				$('input:text')[nextIndex].focus();
				else{
					$('input:text')[nextIndex-1].blur();
					$('.input_cadastrar').click();
				}
		}
	});
	// SOMENTE LETRAS,VIRGULA E PONTO, N�O ACEITA ACENTOS ,  EXCETO campos que tem mascara de valores
	$('input:text').not("input[class^='mascara']").keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z0-9 ,.@/]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) { return true; }
		e.preventDefault();	return false;
	});	
	
	// COLOCA TUDO EM MAIUSCULO, EXCETO campos que tem mascara de valores
	$('input[type=text]').not("input[class^='mascara'][class^='informacao']").change(function() {
		$(this).not("input[class^='minusculo']").val($(this).val().toUpperCase());
	});
	// AJUDA F1
	/*$("body").bind("keydown",function(event) {
            // F1 pressed
			event.returnValue = false;
            if (event.keyCode == 112) {
                setTimeout("window.location.href='./ajuda';",1000);
            }
        });
*/

	
});
// DESATIVA FUN��ES F1 A F12, SUBSTITUI F1 PARA AJUDA DO SISTEMA
function disableFunctionKeys(e) {
    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
	if (e.keyCode == '112') {
		e.preventDefault();
		setTimeout("window.location.href='./ajuda';",500);
	}
    /* para todas as teclas de fun��o
	if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
        e.preventDefault();
    }
*/
	};
$(document).ready(function() {
    $(document).on('keydown', disableFunctionKeys);
});


<!-- Hide Mobiles Browser Navigation Bar -->	
	window.addEventListener("load",function() {
	// Set a timeout...
	setTimeout(function(){
	// Hide the address bar!
	window.scrollTo(0, 1);
	}, 0);
	});


</script>

</head>


<body>


<div id="header">
		<a href="index.php" class="left_bt">INICIO</a>
		<span>SISTEMA RX <?php echo $versao?></span>
		<a href="../rxmobile/ajuda" class="right_bt" id="ajuda"><img src=".../images/ajuda.png" alt="CLIQUE AQUI OU PRESSIONE F1" title="PRECISA DE AJUDA? CLIQUE AQUI OU PRESSIONE F1" border="0" /></a>
    </div>	
<div id="main_container">
    
        <div class="box" id="box">
        	<div class="box_content">
            
            	<div class="box_content_tab">
                PESQUISAR
                </div>
                <div class="box_content_center">
                <div class="form_content">
				<form id="pesquisar" name="pesquisar" method="POST" action="pesquisar.php?modo=pesquisar">
				PESQUISE AQUI: <input size="20" type="text" name="pesquisar" value="" id="pesquisar"/>
				<select size="1" name="tb"><option selected>Selecione o local da busca</option>
				<option value="tb_programacao">PROGRAMA��O</option>
				<option value="tb_go">G.O.</option>
				</select>
				<input type="submit" value="PESQUISAR" name="CADASTRAR">
				</form>
				<a class="boxclose" id="boxclose">FECHAR</a>
                </div> 
                
                <div class="clear"></div>
                </div>
			
           </div>
        </div>
		
		
	<div class="content">