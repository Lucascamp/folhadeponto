
<div id="footer">
<a href="#" class="left_bt" id="menuopcoes_abrir">MENU</a>
<span>USUÁRIO: <?php echo $_SESSION['login_usuario'];?> </span>
<a onclick="jQuery('html, body').animate( { scrollTop: 0 }, 'slow' );"  href="javascript:void(0);" title="SUBIR A TELA" class="right_bt"><img src="images/top.png" alt="" title="" border="0" /></a>
</div>
<script>
jQuery(function($){
	$(".mascara_data").mask("99/99/9999");
	$(".mascara_data_diames").mask("99/99");
	$(".mascara_data_hora").mask("99/99/9999 ?99:99:99");
	$(".mascara_tel").mask("(99)99999-999?9");
	$(".mascara_hora").mask("99:99:99");
	$(".mascara_validade").mask("9?99");
	$(".mascara_qtditens").mask("9?99");
	$(".mascara_contador").mask("9?99");
	$(".mascara_2digitos").mask("9?9");
	$(".mascara_km").mask("9?9999");
});
$('.mascara_valor').priceFormat({
    prefix: 'R$',
    centsSeparator: '.',
	clearPrefix: true
});

</script>
</body>
</html>