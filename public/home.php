<?php include("../../RXMOBILE/comandos.php"); checa_login(); 
header('Content-Type: text/html; charset=utf8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale = 1.0" />
<meta name="author" content="Compvix - Soluções em Teclologia e Cristiano Guilherme - www.cristianoguilherme.com" />
<meta name="description" content="Sistema de Programação de RX | Módulo Ponto" />
<title>Sistema RX | Módulo Ponto</title>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<script type="text/javascript" src="js/jquery.min.js"></script>

<script type="text/javascript">
var $ = jQuery.noConflict();
	$(function() {
		$('#activator').click(function(){$('#box').animate({'top':'66px'},500);});

		$('#horarios_abrir') .click(function(){$('#menu_horarios').animate({'top':'65px'},500);});
		$('#horarios_fechar').click(function(){$('#menu_horarios').animate({'top':'-430px'},500);});
		
		$('#beneficios_abrir') .click(function(){$('#menu_beneficios').animate({'top':'65px'},500);});
		$('#beneficios_fechar').click(function(){$('#menu_beneficios').animate({'top':'-430px'},500);});

		$('#relatorios_abrir') .click(function(){$('#menu_relatorios').animate({'top':'65px'},500);});
		$('#relatorios_fechar').click(function(){$('#menu_relatorios').animate({'top':'-430px'},500);});
				
		$('#login_abrir') .click(function(){$('#menu_login').animate({'top':'-60px'},500);});
		$('#login_fechar').click(function(){$('#menu_login').animate({'top':'-300px'},500);	});
		$('#menu_login').animate({'top':'-300px'},1800);
		
	});

	$(document).ready(function(){
	
	$(".toggle_container").hide(); 
		$(".trigger").click(function(){
			$(this).toggleClass("active").next().slideToggle("slow");
		return false;
	});
	
	});
</script>
<!-- Hide Mobiles Browser Navigation Bar -->
<script type="text/javascript">
	window.addEventListener("load",function() {
	// Set a timeout...
	setTimeout(function(){
	// Hide the address bar!
	window.scrollTo(0, 1);
	}, 1);
	});
</script>

</head>

<body>

<?php

$conexaoBD = conectar($bdBanco);
$query = "SELECT nome,login,cod FROM tb_usuario WHERE login = '".$_SESSION['login_usuario']."'";
$result = $conexaoBD->query($query);
$valor = $result->fetch_array();
$conexaoBD->close();

?>

<div id="main_container">
	<div class="logo">
			<img src="images/logo.png" alt="" title="" border="0" />
		</div>
		
		<div class="box" id="menu_horarios">
            <div class="box_content">
            	<div class="box_content_tab">HORÁRIOS</div>
					<div class="box_content_center">
						<div class="form_content">
                          	<div class="menu">
							<ul>
								<li><a href="/horarios/create"><img src="images/ico_adicionar.png" border="0" alt="" title=""/>ADICIONAR MARCAÇÃO</a></li>
								<li><a href="/horarios"><img src="images/ico_relatorio_simples.png" border="0" alt="" title=""/>VISUALIZAR RELATÓRIO</a></li>
							</ul>
							</div>
							<a class="boxclose" id="horarios_fechar">FECHAR</a>
						</div>
						<div class="clear"></div>  
					</div>
				</div>
        </div>	

		<div class="box" id="menu_beneficios">
            <div class="box_content">
            	<div class="box_content_tab">BENEFÍCIOS</div>
					<div class="box_content_center">
						<div class="form_content">
                          	<div class="menu">
							<ul>
								<li><a href="/beneficios/create"><img src="images/ico_adicionar.png" border="0" alt="" title=""/>ADICIONAR MARCAÇÃO</a></li>
								<li><a href="/beneficios"><img src="images/ico_relatorio_simples.png" border="0" alt="" title=""/>VISUALIZAR RELATÓRIO</a></li>
							</ul>
							</div>
							<a class="boxclose" id="beneficios_fechar">FECHAR</a>
						</div>
						<div class="clear"></div>  
					</div>
				</div>
        </div>
				<div class="box" id="menu_relatorios">
            <div class="box_content">
            	<div class="box_content_tab">RELATÓRIOS</div>
					<div class="box_content_center">
						<div class="form_content">
                          	<div class="menu">
							<ul>
								<li><a href="#"><img src="images/ico_relatorio_simples.png" border="0" alt="" title=""/>RELATÓRIO POR CENTRO DE CUSTO</a></li>
								<li><a href="#"><img src="images/ico_relatorio_simples.png" border="0" alt="" title=""/>RELATÓRIO POR FUNCIONÁRIO</a></li>
							</ul>
							</div>
							<a class="boxclose" id="relatorios_fechar">FECHAR</a>
						</div>
						<div class="clear"></div>  
					</div>
				</div>
        </div>
		<div class="box_transparente" id="menu_login">
            <div class="box_content">
            	<div class="box_content_tab">MINHAS CONFIGURAÇÕES DE USUÁRIO</div>
					<div class="box_content_center_transparente">
						<div class="form_content">
                          	<div class="menu">
							<ul>
								<li><a href="cadastro_usuario.php?modo=editar&tb=tb_usuario&cod=<? echo $valor['cod']; ?>"><img src="images/ico_alterarsenha.png" border="0" alt="" title=""/>ALTERAR SENHA</a></li>
								<li><a href="?cmd=deslogar"><img src="images/ico_logoff.png" border="0" alt="" title=""/>SAIR DO SISTEMA</a></li>
							</ul>
							</div>
							<table border="0" width="100%" cellpadding="0"><tr><td width="33%"><img id="login_abrir" src="images/img_abrir_menu.png" alt="ABRIR MENU LOGIN"><img id="login_fechar" src="images/img_fechar_menu.png" alt="FECHAR MENU LOGIN"></td><td align="center" width="33%">Olá <? echo $valor['nome']; ?></td><td width="33%">&nbsp;</td></tr></table>
						</div>
						<div class="clear"></div>  
					</div>
				</div>
        </div>
		
   
	<div class="menu">
    	<ul>
			<? if (checa_permissao('permissao_administrador')) { ?>	<li><a href="#" id="horarios_abrir"><img src="images/ico_horarios.png" border="0" alt="" title=""/>HORÁRIOS</a></li>	<? } ?>
			<? if (checa_permissao('permissao_administrador')) { ?>	<li><a href="#" id="beneficios_abrir"><img src="images/ico_beneficios.png" border="0" alt="" title=""/>BENEFÍCIOS</a></li>	<? } ?>
			<br>
			<? if (checa_permissao('permissao_operador'))   { ?>	<li><a href="/horarios/create" id="marcar_ponto"><img src="images/ico_marcaponto.png" border="0" alt="" title=""/>MARCAR MEU PONTO</a></li>	<? } ?>
		</ul>
    </div>


</div>


</body>
</html>
