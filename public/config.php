<?
//----------------------
// Arquivo CONFIG.PHP
// Parte integrante do FBSW (Framework Base de Sistemas Web)
// Desenvolvido por Cristiano Guilherme - www.cristianoguilherme.com
// V. 2.0  - 22/08/2013
//----------------------

// par�metros de configura��o do banco de dados
$bdServidor         = "localhost"; // servidor
$bdUsuario          = "root";  // nome do usu�rio
$bdSenha            = "usbw";    // senha
$bdBanco            = "bd_rx2";    // nome do banco de dados
$bdServidorPROTHEUS = "localhost"; // servidor
$bdUsuarioPROTHEUS  = "root";  // nome do usu�rio
$bdSenhaPROTHEUS    = "usbw";    // senha
$bdBancoPROTHEUS    = "bd_rx";    // nome do banco de dados

// par�metros do sistema
$url_sistema          = "http://localhost/rxmobile/"; 
$tamanho_do_arquivo  = 5 * 1024 * 1024;       // = 5MB tamanho em KB
$pasta_localizacao    = "dados/localizacao/";  // local onde ser� armazenado a foto da planta onde o �tem esta localizado
$pasta_nota_fiscal    = "dados/nota_fiscal/"; // local onde ser� armazenado as digitaliza��es das notas fiscais
$pasta_foto           = "dados/foto/";        // local onde ser� armazenado as fotos do �tem cadastrado
$email_helpdesk       = "helpdesk@cristianoguilherme.com"; // email para registro da abertura dos chamados realizados pelo sistema
$email_sistema        = "monitoramento@cristianoguilherme.com"; //email para uso do sistema ao enviar um chamado aberto

$pasta_progarquivos   = "dados/progarquivos/";  // local onde ser� armazenado a foto da planta onde o �tem esta localizado

$arquivo_log_erros = './log/log_de_erros.log';

$versao = "1.0 BETA 26/12/2013";

$porta	= "8060";

?>