<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PontosPeriodoFechado extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ponto:FecharPeriodo';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fecha os pontos dos periodos já fechados';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Fechando os pontos dos períodos já fechados');

		$periodos =  DB::table('tb_periodo')
                        ->where('fechado', 1)
                        ->whereNull('deleted_at')
                        ->get();
 
		foreach($periodos as $periodo)
		{
			$inicio = $periodo->periodo_inicio;
			$fim 	= $periodo->periodo_fim;

			DB::table('tb_horario')
            			->where('fechado', 0)
            			->whereBetween('data', array($inicio, $fim ))
            			->update(array('fechado' => 1));
		}

		$this->info('Pontos fechados com sucesso');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
