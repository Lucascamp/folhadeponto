<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

class UpdateTempoExposicao extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ponto:updateTFE';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Atualiza tempo de exposicao da tabela GO';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * get data to create the inserts in table.
	 *
	 * @return mixed
	 */
	public function getHorarios()
	{        
		

	    //Busca dados
		$horarios = DB::table('tb_horario')
						->whereBetween('data', array($periodoInicio, $periodoFinal))
						->where('deleted_at', null)
						->get();

		return $horarios;
	}

	public function getGo($data)
	{        
		$go = DB::table('tb_go')
				->select('cod_go', 'clie_cod', 'equi_func1_mat', 'equi_func2_mat','equi_func3_mat', 'equi_func4_mat')
				->where('data', $data)
				->get();

		return $go;
	}

	public function getTempoExposicao($codgo, $cliente, $data)
	{        
		$query = "SELECT  SEC_TO_TIME( SUM(TIME_TO_SEC(temp.tempoexposicao))) AS soma
					FROM(
					     SELECT
						 prog.tempoexposicao
						 FROM tb_programacao prog
						 WHERE prog.cod_go = '".$codgo."'
						 AND prog.data = '".$data."'
						 AND prog.item_excluido = 'N'
					     AND prog.clie_cod = '".$cliente."'
					     AND prog.deleted_at is null
						) AS temp";

		$connection = DB::connection();		
		$results = $connection->select($query);

		$result = reset($results);
		
		return $result;
	}

	public function getCodFuncionario($matricula)
	{        
		$cod_funcionario = DB::table('tb_funcionario')
								->where('matricula_qt', $matricula)
								->pluck('cod');

		return $cod_funcionario;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$periodoInicio = DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_inicio');
		$periodoInicio  = Carbon::createFromFormat('Y-m-d H:i:s', $periodoInicio);

		$periodoFinal  = DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_fim');
		$periodoFinal  = Carbon::createFromFormat('Y-m-d H:i:s', $periodoFinal);
		$periodoFinal->addDay();

		$diasPeriodo = $periodoInicio->diffInDays($periodoFinal);

 		foreach(range(1, $diasPeriodo) as $index)
 		{
 			$gos = $this->getGo($periodoInicio);

 			foreach($gos as $go)
 			{
	 			if($go)
	 			{
	 				$tfe = $this->getTempoExposicao($go->cod_go, $go->clie_cod, $periodoInicio);
	 				
	 				if($tfe->soma != null && $tfe->soma != '00:00:00')
					{
						for($i=1; $i<5; $i++)
						{	
							$mat = 'equi_func'.$i.'_mat';
							if($mat !=null && $mat != '')
							{ 
								$cod_funcionario = $this->getCodFuncionario($go->$mat);
								
								DB::table('tb_horario')
									->where('cod_funcionario', $cod_funcionario)
									->where('data', $periodoInicio->format('Y-m-d'))
									->update(array('tempo_exposicao' =>  $tfe->soma));
							}
						}
					}
	 			}
	 		}
 			
 			$periodoInicio->addDay();
 		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
