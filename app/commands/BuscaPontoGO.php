<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

class BuscaPontoGO extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ponto:sync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cria pontos a partir das GOs';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	/**
	 * get data to create the inserts in table.
	 *
	 * @return mixed
	 */
	public function getHorariosRdo()
	{        
		 //seleciona periodo em aberto
		$periodoInicio = DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_inicio');
		$periodoFinal  = DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_fim');
		$periodoFinal  = Carbon::createFromFormat('Y-m-d H:i:s', $periodoFinal);
		$periodoFinal->addDay();
		//dia adicionado pra query abaixo pegar todo periodo pois o periodo é a data+'00:00:00'
		//os dados do ultimo dia tb devem ser criados

	    //Busca dados
		$rdo = DB::table('tb_rdo')
		->select('tb_rdo.cod_go', 'tb_rdo.codlocalizacao', 'tb_rdo.item_contab', 'tb_rdo.clienteentrada',
				 'tb_rdo.clientesaida', 'tb_rdo.hora_faturada', 'tb_rdo.hora_contratada' , 'tb_go.equi_func1_mat',
				 'tb_go.equi_func2_mat', 'tb_go.equi_func3_mat', 'tb_go.equi_func4_mat')
		->join('tb_go', 'tb_rdo.cod_go', '=', 'tb_go.cod_go')
		->whereNull('tb_rdo.deleted_at')
		->whereNotNull('tb_rdo.clienteentrada')
		->whereBetween('tb_rdo.clienteentrada', array($periodoInicio, $periodoFinal))
		->get();
		
		return $rdo;
	}

	/**
	 * get interval.
	 *
	 * @return mixed
	 */
	public function getIntervalRdo($entrada, $saida)
	{
		//criação dos intervalos
					$intervaloInicio = clone $entrada;  				
					$intervaloFim = clone $entrada;

				//pega o total trabalhado para gerar os intervalos
					$diff = $entrada->diffInMinutes($saida);
					$half = $diff/2;
				//menor que 4 horas sem intervalo

				//de 6 a 8 horas 15 min intervalo
					if(($diff > 360) && ($diff < 495))        
					{
						$intervaloInicio->addMinutes($half);
						$intervaloFim->addMinutes(($half+15));
					}
				// de 8 a 12 horas 1 hora de intervalo
					else if(($diff > 495) && ($diff < 780))	
					{
						$intervaloInicio->addMinutes($half);
						$intervaloFim->addMinutes(($half+60));
					}
				// de 12 horas em diante 2 horas intervalo
					else if($diff >= 780) 
					{
						$intervaloInicio->addMinutes(360);
						$intervaloFim->addMinutes(480);
					}

		return array($intervaloInicio, $intervaloFim);

	}

	/**
	 * get holidays in the year
	 * @return int minutes between the begin and end of break time
	 */
	public static function getHolidays()
	{
		 return DB::table('tb_feriado')->lists('feriado', 'cod');
	}

	/**
	 * get holidays in the year
	 * @return int minutes between the begin and end of break time
	 */
	public static function getFuncionarios($func_mat_qt)
	{
		return  DB::table('tb_funcionario')->where('matricula_qt', $func_mat_qt)->pluck('cod');
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('criando os pontos a partir de uma GO');
		
		$rdos = $this->getHorariosRdo();
		
		//cria os dados para inserção
		foreach ($rdos as $rdo)
		{
			$func1 			  = $rdo->equi_func1_mat;
			$func2 			  = $rdo->equi_func2_mat;
			$func3 			  = $rdo->equi_func3_mat;
			$func4 			  = $rdo->equi_func4_mat;
			$codgo 			  = $rdo->cod_go;
			$item   		  = $rdo->item_contab;
			$entrada   		  = $rdo->clienteentrada;
			$saida 	  		  = $rdo->clientesaida;
			$hora_faturada 	  = $rdo->hora_faturada;
			$hora_contratada  = $rdo->hora_contratada;
			$unidade		  = $rdo->codlocalizacao;

			$itemcontab = DB::table('tb_item_contab')->where('CTD_ITEM', $item)->pluck('cod');

			for($i=1; $i<5 ; $i++)
			{
				$func = 'equi_func'.$i.'_mat';

				$codfunc[$i] = $this->getFuncionarios($rdo->$func);

				if(!$codfunc[$i])
					unset($codfunc[$i]);
			}
				
			$now  = Carbon::now();
			
			if(isset($entrada)&&isset($saida))
			{					
				if(($entrada > '0000-00-00 00:00:00')&&($saida > '0000-00-00 00:00:00') && ($saida>$entrada))
				{
					//pega os horarios de saida e entrada e transforma em carbon
					$entrada = Carbon::createFromFormat('Y-m-d H:i:s', $entrada);
					$saida 	 = Carbon::createFromFormat('Y-m-d H:i:s', $saida);
					
					//pega a data do ponto
					$datago = clone $entrada;       
					$datago = substr($datago,0,10);	   

					//cria horas faturadas
					$hora_faturada = Carbon::createFromFormat('Y-m-d H:i:s', $datago.' '.$hora_faturada);
					$hora_contratada = Carbon::createFromFormat('Y-m-d H:i:s', $datago.' '.$hora_contratada)->hour;

					//busca os intervalos
					$intervalos = $this->getIntervalRdo($entrada, $saida);
					$intervaloInicio = $intervalos[0];
					$intervaloFim 	 = $intervalos[1];

					//verifica se a data é feriado
					$atividade = 'Hora Direta';

					$feriados = $this->getHolidays();
					
					$feriadoCheck = substr($entrada->format('d/m/Y'), 0,5);

					if(in_array($feriadoCheck, $feriados))
						$atividade = 'Feriado';

					//cria os apontamentos	
					foreach ($codfunc as $cod_funcionario) 
					{
						$checaCriado = DB::table('tb_horario')
										->where('cod_funcionario', $cod_funcionario )
										->where('data', $datago)
										->first();

						//Atualiza o registro caso algo mude na RDO
						
						
						//Cria o registro se ainda não existir
						if(!$checaCriado)
						{
							DB::table('tb_horario')->insert(
								array(
									'cod_funcionario'  	=> $cod_funcionario,
									'unidade' 			=> $unidade,
									'data' 			   	=> $datago,
									'hora_entrada' 	   	=> $entrada,
									'hora_saida' 	   	=> $saida,
									'intervalo_inicio' 	=> $intervaloInicio,
									'intervalo_fim' 	=> $intervaloFim,
									'hora_faturada' 	=> $hora_faturada,
									'hora_contratada'  	=> $hora_contratada,
									'atividade' 	 	=> $atividade,
									'cod_item_contab'   => $itemcontab,
									'created_at'  	 	=> $now,
									'created_by'  	 	=> 'Sistema',
									)
							);

						}
						elseif(isset($checaCriado) && $checaCriado->created_by=='Sistema' && !isset($checaCriado->updated_by))
						{
							DB::table('tb_horario')
							->where('cod_funcionario', $cod_funcionario)
							->where('data', $datago)
							->update(array(
									'hora_entrada' 	 	=> $entrada,
									'hora_saida' 		=> $saida,
									'intervalo_inicio' 	=> $intervaloInicio,
									'intervalo_fim' 	=> $intervaloFim,
									'updated_at'  	 	=> $now,
									)
								);
						}
					}
				}
			}
		}

		$this->info('Pontos criados com sucesso.');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
