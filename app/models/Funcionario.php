<?php

class Funcionario extends Eloquent 
{

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('cod');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table 		= 'tb_funcionario';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey   = 'cod';
	

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;


	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		'matricula' => 'required',
		'nome' => 'required',
		'funcao' => 'required',
		'mope' => 'required',
		'asnt' => 'required',
		'cnen' => 'required',
		'ftbs' => 'required',
		'snqc' => 'required',
		'unidade' => 'required'
	);

	/**
	 * Define the relationship with horarios
	 */
	public function horarios()
	{
		return $this->hasMany('Horario', 'cod_funcionario');
	}

	/**
	 * Define the relationship with beneficios
	 */
	public function beneficios()
	{
		return $this->hasMany('Beneficio', 'cod_funcionario');
	}

	/**
	 * Define the relationship with Guia	 
	 */
	public function guias()
	{
		return $this->belongsToMany('Guia', 'tb_funcionario_go','funcionario_cod', 'go_cod');
	}

	/**
	 * Define the relationship with horarios
	 */
	public function banco()
	{
		return $this->hasMany('Banco', 'cod_funcionario');
	}


}