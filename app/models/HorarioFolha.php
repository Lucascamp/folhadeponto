<?php

use Carbon\Carbon;

class HorarioFolha extends Horario
{
	/**
	 * Type of table qt|a5
	 * @var string
	 */
	protected static $type = 'qt';

	/**
	 * Intervalo entre empresas em minutos.
	 * @var int
	 */
	const intervalo = 60;

	/**
	 * set the type to print qt|a5
	 * @var string
	 */
	public static function setType($type){
		self::$type = $type;
    }

	/**
	 * Convert seconds in a Carbon date
	 * @param  integer $seconds
	 * @return \Carbon\Carbon
	 */
	public function convertSecondsToCarbon($seconds)
	{
		
		$hours 	   = floor($seconds / 3600);
		$minutes   = (int) (($seconds-($hours*3600)) / 60);
		$seconds   = (int) $seconds % 60;	

		return Carbon::createFromTime($hours, $minutes, $seconds);
	}

    /**
	 * set the type to print qt|a5
	 * @param string key get the string data from the fields to create a carbon date
	 * @var carbon carbon date
	 */
	public function convertAttributeToCarbon($key)
	{
		return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes[$key]);
	}

	/**
	 * return a day worked time in minutes
	 * @return integer minutes worked
	 */
	public function getWorkedTimeInMinutes()
	{				
		$hora_saida   = $this->convertAttributeToCarbon('hora_saida');
		$hora_entrada = $this->convertAttributeToCarbon('hora_entrada');

		$timeDiff 		= $hora_saida->diffInMinutes($hora_entrada);
		$breakTimeDiff 	= $this->getBreakTime();
		$timeDiff 	   -= $breakTimeDiff;

		return abs($timeDiff);
	}


	/**
	 * return worked time divided by 2 in minutes
	 * @return integer minutes worked
	 */
	public function getHalfWorkedTimeInMinutes()
	{
		
		if(($this->atividade=='Atestado')||($this->atividade=='Exame Médico'))
			$minutes = ($this->hora_contratada * 60);

		elseif($this->atividade=='Afastado')
			$minutes = 0;

		else
			$minutes = $this->getWorkedTimeInMinutes();
		
		return $minutes / 2;
	}

	/**
	 * return worked time 
	 * @return integer minutes worked
	 */
	public function getWorkedHours()
	{
		if($this->funcionario->data_demissao_qt == null || $this->funcionario->data_demissao_qt == '')
			$this->funcionario->data_demissao_qt = $this->data->addYear();
		if($this->funcionario->data_demissao_a5 == null || $this->funcionario->data_demissao_a5 == '')
			$this->funcionario->data_demissao_a5 = $this->data->addYear();

		if($this->data > $this->funcionario->data_demissao_qt || $this->data > $this->funcionario->data_demissao_a5)
			return Horario::getWorkedHours();
		else
			return $this->getWorkedHalfHours();	
	}

	/**
	 * Get the worked hours
	 * @return Carbon  A Carbon instance with the time Worked
	 */
	public function getClientHours()
	{		
		$client = Carbon::createFromTime($this->hora_faturada->hour, $this->hora_faturada->minute);
			
		$zerohour = Carbon::createFromTime(0,0);

		return $this->convertMinutesToCarbon($zerohour->diffInMinutes($client)/2);
	}

	/**
	 * Get the worked hours
	 * @return Carbon  A Carbon instance with the time Worked
	 */
	public function getDisposicaoHours()
	{		
		$disposicao = $this->getWorkedHours()->diffInMinutes($this->getClientHours());
		return $this->convertMinutesToCarbon($disposicao);
	}

	/**
	 * return worked extra time 
	 * @return integer minutes worked
	 */
	public function getExtraHours()
	{
		return $this->getHalfExtraHours();	
	}

	/**
	 * return worked time divided by 2
	 * @return integer minutes worked
	 */
	public function getWorkedHalfHours()
	{		
		$workedMinutes = $this->getHalfWorkedTimeInMinutes();
		return $this->convertMinutesToCarbon($workedMinutes);
	}

		/**
	 * return worked extra time divided by 2
	 * @return integer minutes worked
	 */
	public function getHalfExtraHours()
	{		
		/*$feriados = $this->getHolidays();*/
		if((($this->data->dayOfWeek == Carbon::SUNDAY) 
			/*|| (in_array(substr($this->data->format('d/m/Y'), 0,5), $feriados)))*/
			||($this->atividade  == 'Feriado'))
			&& (($this->atividade != 'Embarcado')&&($this->atividade != 'Vigia')&&($this->atividade != 'Domingo horas normais')))
		{
			return $this->getWorkedHours();
		}
		
		$contractedMinutes = intval($this->hora_contratada * 60);
		$workedMinutes 	   = $this->getWorkedTimeInMinutes();
		
		if($workedMinutes > $contractedMinutes)
		{
			return $this->convertMinutesToCarbon(($workedMinutes - $contractedMinutes)/2);
		}
		return $this->convertMinutesToCarbon(0);
	}

	/**
	 * Retorna a hora de Entrada
	 * @param  string $value
	 * @return Carbon
	 */
	public function getHoraEntradaAttribute($value)
	{		
		
		if($this->funcionario->data_demissao_qt == null || $this->funcionario->data_demissao_qt == '')
			$this->funcionario->data_demissao_qt = $this->data->addYear();
		if($this->funcionario->data_demissao_a5 == null || $this->funcionario->data_demissao_a5 == '')
			$this->funcionario->data_demissao_a5 = $this->data->addYear();

		if($this->data > $this->funcionario->data_demissao_qt || $this->data > $this->funcionario->data_demissao_a5)
			return Carbon::createFromFormat('Y-m-d H:i:s', $value);

		if(self::$type == 'qt')
			return $this->getHoraEntradaQt($value);

		else if (self::$type == 'a5')
			return $this->getHoraEntradaA5();
	}

	/**
	 * Retorna a hora de Saída
	 * @param  string $value
	 * @return Carbon
	 */
	public function getHoraSaidaAttribute($value)
	{		

		if($this->funcionario->data_demissao_qt == null || $this->funcionario->data_demissao_qt == '')
			$this->funcionario->data_demissao_qt = $this->data->addYear();
		if($this->funcionario->data_demissao_a5 == null || $this->funcionario->data_demissao_a5 == '')
			$this->funcionario->data_demissao_a5 = $this->data->addYear();

		if($this->data > $this->funcionario->data_demissao_qt || $this->data > $this->funcionario->data_demissao_a5)
			return Carbon::createFromFormat('Y-m-d H:i:s', $value);

		if(self::$type == 'qt')
			return $this->getHoraSaidaQt();
		else if (self::$type == 'a5')
			return $this->getHoraSaidaA5($value);
	}	

	// /**
	//  * set the begin of interval
	//  * @param  string $value
	//  * @return Carbon
	//  */
	// public function getIntervaloInicioAttribute($value)
	// {		
	// 	return Carbon::createFromTime(0);
	// }

	// /**
	//  * set the end of interval
	//  * @param  string $value
	//  * @return Carbon
	//  */
	// public function getIntervaloFimAttribute($value)
	// {		
	// 	return Carbon::createFromTime(0);
	// }

	/**
	 * Retorna a hora Noturna
	 * @param  string $value
	 * @return Carbon
	 */
	public function getNightHours()
	{		
		if(self::$type == 'a5')
			return $this->getNightHoursA5();
	    else
	    	return $this->getNightHoursQT();
	}

	/**
	 * Get the nightshift hours worked between 22h and 5h
	 * @return Carbon  A Carbon instance with the nightshift worked
	 */
	public function getNightHoursQT()
	{
		$dia_entrada = $this->eraseTime($this->hora_entrada);
		$dia_saida	 = $this->eraseTime($this->hora_saida);

		$entrada = $this->hora_entrada;
		$saida 	 = $this->getHoraSaidaQT();
		
		$noturno_inicio = $this->getNigthBegin();
		$noturno_fim    = $this->getNigthEnd();

		if($saida > $noturno_inicio && $entrada->hour > 5)
		{
			$noturno_fim->addDay();
		}
		
		if(($dia_entrada->diffInDays($dia_saida) == 0) && ($entrada < $noturno_fim))
		{			
			if($saida > $noturno_fim)
			{
				$saida = $noturno_fim;
			}

			if($entrada < $noturno_inicio)
			{
				$entrada = $noturno_inicio;
			}
			
			$diffInMinutes =  $entrada->diffInMinutes($saida);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}
		
		else if($dia_entrada->diffInDays($dia_saida) > 0 )
		{						
			if($saida > $noturno_fim)
			{	
				$saida = $noturno_fim;
			}

			if($entrada > $noturno_inicio)
			{
				$noturno_inicio = $entrada;
			}
			
			$diffInMinutes =  $noturno_inicio->diffInMinutes($saida);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		elseif($entrada > $noturno_fim && $saida->hour > '22')
		{
			$noturno_inicio->addDay();
			$diffInMinutes =  $saida->diffInMinutes($noturno_inicio);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		return $this->convertMinutesToCarbon(0);
	}	

	/**
	 * Get the nightshift hours worked between 22h and 5h
	 * @return Carbon  A Carbon instance with the nightshift worked
	 */
	public function getNightHoursA5()
	{
		$dia_entrada = $this->eraseTime($this->hora_entrada);
		$dia_saida	  = $this->eraseTime($this->hora_saida);

		$entrada = $this->getHoraEntradaA5();
		$saida = $this->hora_saida;
		
		$noturno_inicio = $this->getNigthBegin();
		$noturno_fim    = $this->getNigthEnd();

		//variavel para teste abaixo
		$notunoErased = $this->getNigthEnd();

		//Se ao mudar de empresa o dia for diferente do horario de entrada, então deve-se adicionar
		//um dia ao inicio noturno
		if(($dia_entrada->diffInDays($this->eraseTime($notunoErased)) > 0))
		{
			$noturno_fim->addDay();
		}

		if(($dia_entrada->diffInDays($dia_saida) == 0) && ($entrada < $noturno_fim))
		{			

			if($saida > $noturno_fim)
			{
				$saida = $noturno_fim;
			}

			if($entrada < $noturno_inicio)
			{
				$entrada = $noturno_inicio;
			}

			$diffInMinutes =  $entrada->diffInMinutes($saida);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}
		
		else if($dia_entrada->diffInDays($dia_saida) > 0)
		{						

			if($saida > $noturno_fim)
			{	
				$saida = $noturno_fim;
			}

			if($entrada > $noturno_inicio)
			{
				$noturno_inicio = $entrada;
			}

			$diffInMinutes =  $noturno_inicio->diffInMinutes($saida);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		return $this->convertMinutesToCarbon(0);
	}	
	
	/**
	 * Retorna o Horario de Entrada QT, sendo este o horário padrão salvo no banco.
	 * @param  string $value date Y-m-d H:i:s
	 * @return Carbon
	 */
	public function getHoraEntradaQt($value){
		return Carbon::createFromFormat('Y-m-d H:i:s', $value);
	}

	/**
	 * set the exit from qualitec 
	 * @return Carbon
	 */
	public function getHoraSaidaQT()
	{		
		$halfTime = $this->getHalfWorkedTimeInMinutes();
		$hora_saida = $this->convertAttributeToCarbon('hora_entrada')->addMinutes($halfTime);

		if($this->atividade=='Atestado')
			$hora_saida = $this->convertAttributeToCarbon('hora_entrada');

		return Carbon::createFromFormat('Y-m-d H:i:s', $hora_saida);
	}

	/**
	 * set the entrance in assinco 
	 * @return Carbon
	 */
	public function getHoraEntradaA5()
	{		
		$hora_entrada = $this->getHoraSaidaQT()->addMinutes($this->getBreakTime());

		if($this->atividade=='Atestado')
			$hora_entrada = $this->getHoraSaidaQT();
		
		return Carbon::createFromFormat('Y-m-d H:i:s', $hora_entrada);
	}

	/**
	 * set the exit from assinco 
	 * @return Carbon
	 */
	public function getHoraSaidaA5($value)
	{		
		return Carbon::createFromFormat('Y-m-d H:i:s', $value);		
	}	

	/**
	 * get the break time  
	 * @return Carbon
	 */
	public function getBreakTime()
	{
		if(($this->atividade == 'Afastado') || ($this->atividade == 'Treinamento') || ($this->atividade == 'Falta') || ($this->atividade == 'Domingo')
		 || ($this->atividade == 'Atestado') || ($this->atividade == 'Folga') || ($this->atividade == 'Férias'))
			return 0;
		/*elseif($this->atividade == 'Feriado')
		 	return 0;*/
		else
			return $this->intervalo_inicio->diffInMinutes($this->intervalo_fim);
	}

}