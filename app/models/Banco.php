<?php

class Banco extends Eloquent 
{

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('cod');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table 		= 'tb_banco_horas';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey   = 'cod';
	

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	protected $fillable = array('cod_funcionario','credito','debito','data','motivo','created_by');

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		'cod_funcionario' => 'required',
	);

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function funcionario()
	{
		return $this->belongsTo('Funcionario', 'cod_funcionario');
	}

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function getSaldo()
	{

	}



}