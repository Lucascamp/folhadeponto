<?php 

use Carbon\Carbon;

class Horario extends BaseModel 
{

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('cod');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_horario';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'cod';
	

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;


	protected static $totals = array();

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(		
		'data' 			  => 'required|date_format:"d/m/Y"',
		'unidade' 		  => 'required',
		'cod_funcionario' => 'required|integer',
		'hora_entrada' 	  => 'required|date_format:"H:i"',
		'hora_saida' 	  => 'required|date_format:"H:i"',
		'intervalo_inicio'=> 'required|date_format:"H:i"',
		'intervalo_fim'   => 'required|date_format:"H:i"',
		'hora_faturada'   => 'date_format:"H:i"',
		'hora_contratada' => 'required',
		'cod_item_contab' => 'required|integer',
		'atividade' 	  => 'required',
		'tempo_exposicao' => 'date_format:"H:i:s"',
		'observacoes' 	  => 'min:0',
		);

	/**
	 * fillable attributes.
	 *
	 * @var array
	 */
	protected $fillable = array('data', 'unidade', 'cod_funcionario', 'hora_entrada', 'hora_saida', 'intervalo_inicio', 'intervalo_fim',
								'hora_faturada', 'hora_contratada', 'cod_item_contab', 'atividade', 'tempo_exposicao', 'observacoes',
								'validado', 'validado_por', 'motivo', 'validado2', 'validado_por2', 'motivo2', 'valido',
								'beneficios', 'created_by', 'updated_by', 'motivo');

	/**
	 * Define the grid columns
	 *
	 * @var array
	 */
	public static $columns = array(
		'cod' 			  	=> 'ID',
		'cod_funcionario' 	=> 'Funcionario',
		'unidade' 		 	=> 'Unidade',
		'data' 			    => 'Data',
		'cod_item_contab'   => 'Centro de Custo',		
		'hora_entrada' 	    => 'Hora Entrada',
		'intervalo_inicio'  => 'Intervalo Inicio',
		'intervalo_fim'     => 'Intervalo Fim',
		'hora_saida' 	    => 'Hora Saída',	
		'horas_trabalhadas' => 'Horas Totais',
		'hora_extra' 		=> 'Hora Extra',
		'adicional_noturno' => 'Adicional Noturno',
		'hora_faturada'   	=> 'Horas Trabalhadas',
		'hora_contratada' 	=> 'Hora Contratada',
		'tempo_exposicao'	=> 'Tempo de Exposição',		
		'atividade' 	  	=> 'Atividade',
		'observacoes' 	  	=> 'observações',
		'acoes' 	  	  	=> 'Ações',
		);	

	/**
     * Columns properties to datatable. 
     * TODO: NEED TO BE IMPROVED. The values need respect the column order.
     * @var array
     */
    public $aoColumns = array(
    	array('sClass'=>'readonly cod'),
		array('sClass'=>'readonly cod_funcionario'),
		array('sClass'=>'unidade'),
		array('sClass'=>'readonly data'),
		array('sClass'=>'cod_item_contab'),
		array('sClass'=>'hora_entrada'),
		array('sClass'=>'intervalo_inicio'),
		array('sClass'=>'intervalo_fim'),
		array('sClass'=>'hora_saida'),
		array('sClass'=>'readonly horas_trabalhadas'),		
		array('sClass'=>'readonly hora_extra'),
		array('sClass'=>'readonly adicional_noturno'),
		array('sClass'=>'hora_faturada'),
		array('sClass'=>'hora_contratada'),
		array('sClass'=>'tempo_exposicao'),
		array('sClass'=>'atividade'),
		array('sClass'=>'observacoes'),
		array('sClass'=>'readonly acoes', "sDefaultContent" => '<a href="#" class="btn btn-danger delete-row">Delete</a>'),
	);	

	const noturno_inicio = '22:00';
	const noturno_fim 	= '05:00';

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function funcionario()
	{
		return $this->belongsTo('Funcionario', 'cod_funcionario');
	}

	/**
	 * Define the relationship with item contabil	 
	 */
	public function item_contabil()
	{
		return $this->belongsTo('ItemContabil', 'cod_item_contab');
	}

	/**
	 * Define the relationship with Guia
	 */

	public function guia()
	{
		return $this->belongsTo('Guia', 'cod_go');
	}

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'hora_entrada', 
			'hora_saida', 
			'intervalo_inicio', 
			'intervalo_fim', 
			'data',
			'hora_faturada'
			);
	}

	/**
	 * get the break time in minutes
	 * @return int minutes between the begin and end of break time
	 */
	public function getBreakTime()
	{
		return $this->intervalo_inicio->diffInMinutes($this->intervalo_fim);
	}

	/**
	 * get holidays in the year
	 * @return int minutes between the begin and end of break time
	 */
	public static function getHolidays()
	{
		 return DB::table('tb_feriado')->lists('feriado', 'cod');
	}

	/**
	 * return a day worked time in minutes
	 * @return integer minutes worked
	 */
	public function getWorkedTimeInMinutes()
	{		
		$timeDiff 		= $this->hora_entrada->diffInMinutes($this->hora_saida);
		$breakTimeDiff 	= $this->getBreakTime();
		$timeDiff 	   -= $breakTimeDiff;

		return abs($timeDiff);
	}

	/**
	 * Convert minutes in a Carbon date
	 * @param  integer $minutes
	 * @return \Carbon\Carbon
	 */
	public function convertMinutesToCarbon($minutes)
	{
		
		$hours 	   = floor($minutes / 60);
		$minutes   = (int) $minutes % 60;
		
		return Carbon::createFromTime($hours, $minutes);
	}

	/**
	 * Get the worked hours
	 * @return Carbon  A Carbon instance with the time Worked
	 */
	public function getWorkedHours()
	{		
		if(($this->atividade=='Atestado')||($this->atividade=='Exame Médico'))
			$workedMinutes = ($this->hora_contratada * 60);

		elseif($this->atividade=='Afastado')
			$workedMinutes = 0;

		else	
			$workedMinutes = $this->getWorkedTimeInMinutes();

		return $this->convertMinutesToCarbon($workedMinutes);
	}

	/**
	 * Get the worked hours
	 * @return Carbon  A Carbon instance with the time Worked
	 */
	public function getClientHours()
	{		
		return Carbon::createFromTime($this->hora_faturada->hour, $this->hora_faturada->minute);
	}

	/**
	 * Get the worked hours
	 * @return Carbon  A Carbon instance with the time Worked
	 */
	public function getDisposicaoHours()
	{		
		$disposicao = $this->getWorkedHours()->diffInMinutes($this->getClientHours());
		return $this->convertMinutesToCarbon($disposicao);
	}

	/**
	 * Get the extra hours worked over the horas_contratadas
	 * @return Carbon  A Carbon instance with the extra time worked
	 */
	public function getExtraHours()
	{		
		if($this->atividade == 'Atestado')
			return $this->convertMinutesToCarbon(0);

		/*$feriados = $this->getHolidays();*/
		if((($this->data->dayOfWeek == Carbon::SUNDAY) 
			/*|| (in_array(substr($this->data->format('d/m/Y'), 0,5), $feriados)))*/
			||($this->atividade  == 'Feriado'))
			&& (($this->atividade != 'Embarcado')&&($this->atividade != 'Vigia')&&($this->atividade != 'Domingo horas normais')))
		{
			return $this->getWorkedHours();
		}

		$contractedMinutes = intval($this->hora_contratada * 60);
		$workedMinutes 	   = $this->getWorkedTimeInMinutes();
		
		if($workedMinutes > $contractedMinutes)
		{
			return $this->convertMinutesToCarbon($workedMinutes - $contractedMinutes);
		}
		return $this->convertMinutesToCarbon(0);
	}

	/**
	 * Change the time to 00:00:00 of the date
	 * @return Carbon  A Carbon instance the date wanted to be erased
	 */
	public function eraseTime(Carbon $date)
	{		
		$date->minute = 0;
		$date->hour = 0;
		$date->second = 0;

		return $date;
	}

	/**
	 * Get the beginning of the night shift
	 * @return Carbon  A Carbon instance with the begin of the nightshift
	 */
	public function getNigthBegin()
	{
		$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio);

		$dia_entrada = $this->eraseTime($this->hora_entrada);
		$dia_saida	  = $this->eraseTime($this->hora_saida);

		if($dia_entrada->diffInDays($dia_saida) == 0 && $this->hora_entrada->hour < 5)
		{
			$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio, $this->data->subDay());
		}

		return $noturno_inicio;
	}

	/**
	 * Get the end of the night shift
	 * @return Carbon  A Carbon instance with the end of the nightshift
	 */
	public function getNigthEnd()
	{
		$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim);

		$dia_entrada = $this->eraseTime($this->hora_entrada);
		$dia_saida	  = $this->eraseTime($this->hora_saida);
		
		if($dia_entrada->diffInDays($dia_saida) > 0)
		{
			$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $this->data->addDay());
		}

		elseif($dia_entrada->diffInDays($dia_saida) == 0 && $this->hora_saida->hour > 22)
		{
			$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $this->data->addDay());
		}
		
		return $noturno_fim;
	}

	/**
	 * Get the nightshift hours worked between 22h and 5h
	 * @return Carbon  A Carbon instance with the nightshift worked
	 */
	public function getNightHours()
	{
		$hora_entrada = $this->eraseTime($this->hora_entrada);
		$hora_saida	  = $this->eraseTime($this->hora_saida);

		$saida = $this->hora_saida;
		$entrada = $this->hora_entrada;

		$intervalo_inicio = $this->intervalo_inicio;
		$intervalo_fim = $this->intervalo_fim;

		$breakTimeDiff = 0;

		$noturno_inicio = $this->getNigthBegin();
		$noturno_fim    = $this->getNigthEnd();
		
		if(($hora_entrada->diffInDays($hora_saida) == 0)&&($entrada < $noturno_fim))
		{
			
			if($saida > $noturno_fim)
			{
				$saida = $noturno_fim;
			}
			
			if($entrada < $noturno_inicio)
			{
				$entrada = $noturno_inicio;
			}

			if($intervalo_fim > $noturno_fim)
			{
				$intervalo_fim = $noturno_fim;
			}

			if($intervalo_inicio < $noturno_inicio)
			{
				$intervalo_inicio = $noturno_inicio;
			}

			if(($intervalo_fim > $noturno_inicio) && ($intervalo_inicio < $noturno_fim))
			{
				$breakTimeDiff 	= $intervalo_inicio->diffInMinutes($intervalo_fim);  
			}	
			
			$diffInMinutes =  $entrada->diffInMinutes($saida);
			$diffInMinutes -= $breakTimeDiff;

			return $this->convertMinutesToCarbon($diffInMinutes);
		}
		
		else if($hora_entrada->diffInDays($hora_saida) > 0)
		{
			
			if($saida > $noturno_fim)
			{	
				$saida = $noturno_fim;
			}

			if($entrada < $noturno_inicio)
			{
				$entrada = $noturno_inicio;
			}

			if($intervalo_fim > $noturno_fim)
			{
				$intervalo_fim = $noturno_fim;
			}
			
			if($intervalo_inicio < $noturno_inicio)
			{
				$intervalo_inicio = $noturno_inicio;
			}
			
			if(($intervalo_fim > $noturno_inicio) && ($intervalo_inicio < $noturno_fim))
			{
				$breakTimeDiff 	= $intervalo_inicio->diffInMinutes($intervalo_fim); 
			}	
			
			$diffInMinutes =  $noturno_inicio->diffInMinutes($saida);
			$diffInMinutes -= $breakTimeDiff;
						
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		return $this->convertMinutesToCarbon(0);
	}	
	
	/**
	 * Get the hours when the employee stay exposed to radiation
	 * @return query in tb_programacao with the sum of the exposed hours per day
	 */
	public function exposedHours($showHour = true)
	{			
		$dataquery = clone $this->data;
		$dataquery = substr($dataquery, 0,10);
		$hour = '00:00:00';

		if($this->funcionario->matricula_qt)
		{

		$query ="SELECT tempo
				 FROM tb_tempoexposicao
				 WHERE cod_funcionario = '".$this->funcionario->cod."'
				 AND data = '".$dataquery."'";
	
		$connection = DB::connection();		
		$results = $connection->select($query);
		
		$result = reset($results);

		if($result!=null)
			$hour = $result->tempo;
		else
			$hour = '00:00:00';

		}
		echo Carbon::createFromFormat('H:i:s', $hour)->format('H:i:s');
		
		return Carbon::createFromFormat('H:i:s', $hour);		
	}

	/**
	 * Complete a Hour with the date givem
	 * @param  string $hour Hour in the format 'H:i'	 
	 * @param  Carbon $date Date to be inserted in the hour
	 * @param  string $hour_format Format of the Hour default 'H:i'
	 * @return Carbon $hour;
	 */
	public function completeHourWithDate($hour, Carbon $date = null, $hour_format = 'H:i:s')
	{

		if (!isset($date)) {
			$date = $this->data;
		}
		//if the hour has no seconds add :00 in the end
		$hour .= (count(explode(':',$hour)) < 3 ? ':00' : '' );

		//create a carbon date to be completed
		$hour = Carbon::createFromFormat($hour_format, $hour);		
		$teste = $hour;
		

		//complete the hour with the carbon date
		$hour->year  = $date->year;
		$hour->month = $date->month;
		$hour->day   = $date->day;

		$teste->month = $date->month;

		return $hour;
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataAttribute($value)
	{
		$this->attributes['data']  = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function setHoraEntradaAttribute($value)
	{	
		$this->attributes['hora_entrada'] = $this->completeHourWithDate($value);		
	}

	public function setHoraSaidaAttribute($value)
	{		
		$hora_saida = $this->completeHourWithDate($value);

		if ($hora_saida->hour < $this->hora_entrada->hour) {
			$hora_saida->addDay();
		}

		$this->attributes['hora_saida'] = $hora_saida;
	}

	public function setIntervaloInicioAttribute($value)
	{
		$intervalo_inicio = $this->completeHourWithDate($value);

		if ($intervalo_inicio->hour < $this->hora_entrada->hour) {
			$intervalo_inicio->addDay();
		}
		
		$this->attributes['intervalo_inicio'] = $intervalo_inicio;
	}

	public function setIntervaloFimAttribute($value)
	{
		$intervalo_fim = $this->completeHourWithDate($value);

		if ($intervalo_fim->hour < $this->intervalo_inicio->hour) {
			$intervalo_fim->addDay();
		}
		elseif ($intervalo_fim->hour < $this->hora_entrada->hour) {
			$intervalo_fim->addDay();
		}

		$this->attributes['intervalo_fim'] = $intervalo_fim;
	}	

	public function setHoraFaturadaAttribute($value)
	{	
		$this->attributes['hora_faturada'] = $this->completeHourWithDate($value);		
	}
	/*== End Convert  Date to Save on DB ==*/
	
	/**
	 * Sum Total Hours
	 * @param  Carbon $date Date to be compared to get the hours
	 * @param  name of the field that you want to be counted
	 * @return $date given
	 */
	public static function addTotalHour(Carbon $date, $key)
	{		
		if(! isset(self::$totals[$key]))
		{
			self::$totals[$key] = array('carbon' => new Carbon('00:00:00'));
		}

		if(!isset(self::$totals[$key]['minutes']))
			self::$totals[$key]['minutes'] = 0;

		if(isset(self::$totals[$key]['carbon']))
			self::$totals[$key]['minutes'] += self::$totals[$key]['carbon']->diffInMinutes($date);
		
		return $date;
	}

	

	/**
	*  Get the Sum of the Total Hours
	* @param  name of the field that you want to be counted
	* @return Time in minutes
	*/
	public static function getTotalHour($key)
	{
		if(!isset(self::$totals[$key]['minutes']))
			self::$totals[$key]['minutes'] = 0;

		if(isset(self::$totals['worked_hour_qt']['minutes']) && isset(self::$totals['extra_hour_qt']['minutes']))
		{
			self::$totals['worked_hour_qt']['minutes'] -= self::$totals['extra_hour_qt']['minutes'];
		}

		if(isset(self::$totals['worked_hour_a5']['minutes']) && isset(self::$totals['extra_hour_a5']['minutes']))
		{
			self::$totals['worked_hour_a5']['minutes'] -= self::$totals['extra_hour_a5']['minutes'];
		}

		if(isset(self::$totals['worked_hour']['minutes']) && isset(self::$totals['extra_hour']['minutes']))
		{
			self::$totals['worked_hour']['minutes'] -= self::$totals['extra_hour']['minutes'];
		}

		return number_format((self::$totals[$key]['minutes'] / 60), 2);
	}

	/**
	* Sum 1 day for each day that the employee is absent and the reason
	* Adapted to add benefits too
	* @param  Get the "atividade" 
	* @param  name of the activity that you want to be counted
	*/
	public static function addActivityDay($atividade, $key)
	{
		if(!isset(self::$totals[$key][$atividade]))
		{
			self::$totals[$key][$atividade] = 0;
		}

		if(!isset(self::$totals[$key]['va']))
		{
			self::$totals[$key]['va'] = 0;
		}
		
		if($atividade=='2xva')
			self::$totals[$key]['va'] += 2;
		else
			self::$totals[$key][$atividade] += 1;
	}
	
	/**
	*  Get the Sum of the Total Atividades days and sum benefits
	* @param  Atividade counted
	* @param  name of the field that you want to be counted
	* @return Time in minutes
	*/
	public static function getTotalActivity($atividade, $key)
	{	
		if(!isset(self::$totals[$key][$atividade]))
		{
			self::$totals[$key][$atividade] = 0;
		}

		return self::$totals[$key][$atividade];
	}

	/**
	* Query to get data to create the timesheet
	* @param  codfunc employee ID
	* @param  datainicio begin of the period
	* @param  datafinal end of the period
	* @return array of data from DB
	*/
	public function getHorariosFolha($codfunc, $datainicio, $datafinal)
	{        
		return $this->with(array('funcionario', 'item_contabil'))
		->where('tb_horario.cod_funcionario', $codfunc)
		->whereBetween('tb_horario.data', array($datainicio, $datafinal))
		->where('tb_horario.validado', 1)
		->where('tb_horario.validado2', 1)
		->where('tb_horario.motivo', '')
		->where('tb_horario.motivo2', '')
		->where('tb_horario.valido', 1)
		->where('tb_horario.fechado', 1)
		->orderBy('tb_horario.data')
		->get();
	}

	/**
	* Query to get data for the timesheet mirror
	* @param  codfunc employee ID
	* @param  datainicio begin of the period
	* @param  datafinal end of the period
	* @return array of data from DB
	*/
	public function getPrevisaoFolha($codfunc, $datainicio, $datafinal)
	{        
		return $this->with(array('funcionario', 'item_contabil'))
		->where('tb_horario.cod_funcionario', $codfunc)
		->whereBetween('tb_horario.data', array($datainicio, $datafinal))
		->orderBy('tb_horario.data')
		->get();
	}

	/**
	* Query to check if the timesheet have inconsistencys before allow to see
	* @param  codfunc employee ID
	* @param  datainicio begin of the period
	* @param  datafinal end of the period
	* @return array of data from DB
	*/
	public function checkInconsistenciasFolha($codfunc, $datainicio, $datafinal)
	{        
		$query="SELECT count(*) as Inconsistencias FROM(
				SELECT * FROM `tb_horario` 
				WHERE cod_funcionario = ".$codfunc." 
				AND data BETWEEN '".$datainicio."' AND '".$datafinal."' 
				AND deleted_at is null) as todos
				WHERE valido=0
				OR motivo!=''";

		$connection = DB::connection();		
		$results = $connection->select($query);
		$result = reset($results[0]);
		
		return $result;
	}

	/**
	* Check if have the benefit and check the box
	* @param  activity activity to be checked
	* @param  beneficios benefits that user have
	* @return boolean
	*/
	public static function checkBeneficiosEdit($activity, $beneficios)
	{        
		if(in_array($activity, $beneficios))
			return true;
		else
			return false;
	}

	/**
	* function to create the txt file of extra hours to export to protheus
	* @param  $results query from controller
	* @param  $type type of matricula qt/a5
	* @param  $file path to create the file
	* @return 
	*/
	public function exportExtraHours($results, $type, $file, $datainicio)
	{ 
		$cods  = array();
		$check = array();
		$contents='';
		$sigla = substr($type, 10);
		$demissao = 'data_demissao_'.$sigla;
		$matricula = 'matricula_'.$sigla;

		foreach($results as $horario)
		{
			$sigla = substr($type, 10);
			$demissao = 'data_demissao_'.$sigla;

			if($horario->funcionario->data_demissao_qt != null && ($datainicio > $horario->funcionario->data_demissao_qt))
            	$horario->funcionario->matricula_qt = null;

        	if($horario->funcionario->data_demissao_a5 != null && ($datainicio > $horario->funcionario->data_demissao_a5))
            	$horario->funcionario->matricula_a5 = null;

			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			if($horario->funcionario->$matricula!=null)
			{
				if($horario->data < $horario->funcionario->$demissao)
				{
					Horario::addTotalHour($horario->getExtraHours(), $horario->funcionario->$type);
				} 
			}

			//array com os codigos dos funcionarios
			if(!in_array($horario->funcionario->$type, $cods))
			{
				array_push($cods, $horario->funcionario->$type);
			}
			
			//teste para ver se o funcionario trabalha nas duas empresas
			if(($horario->funcionario->matricula_qt!=null)&&($horario->funcionario->matricula_a5!=null))
			{
				if(!in_array($horario->funcionario->$type, $check))
				{
					array_push($check, $horario->funcionario->$type);
				}
			} 
		}
		
		foreach($cods as $cod)
		{
			//se o usuario tiver duas empresas divide o total por 2
			if(in_array($cod, $check))
			{
				$valor=(Horario::getTotalHour($cod)/2);
				if($this->checkCod($cod))
					$contents = str_pad($cod, 6, "0", STR_PAD_LEFT).'  '.(substr(str_pad(number_format($valor, 2),5," "),0,5)).'   106'."\r\n";
			}
			else //se tiver uma empresa apenas imprime o total
			{	
				$valor=Horario::getTotalHour($cod);
				if($this->checkCod($cod))
					$contents = str_pad($cod, 6, "0", STR_PAD_LEFT).'  '.(substr(str_pad(number_format($valor, 2),5," "),0,5)).'   106'."\r\n";
			}
			
			$this->writeFile($cod, $file, $contents);
		}

	}

	public function getHalfWorked($hora_entrada, $hora_saida, $atividade, $intervalo_inicio, $intervalo_fim)
	{
		$hora_entrada   = Carbon::createFromFormat('Y-m-d H:i:s', $hora_entrada);
		$hora_saida   = Carbon::createFromFormat('Y-m-d H:i:s', $hora_saida);

		$timeDiff 		= $hora_saida->diffInMinutes($hora_entrada);

		if(($atividade == 'Afastado') || ($atividade == 'Treinamento') || ($atividade == 'Falta') || ($atividade == 'Domingo')
		 || ($atividade == 'Atestado') || ($atividade == 'Folga') || ($atividade == 'Férias'))
			$breakTimeDiff = 0;
		else
			$breakTimeDiff = $intervalo_inicio->diffInMinutes($intervalo_fim);
		 
		$timeDiff -= $breakTimeDiff;
		$minutes = abs($timeDiff);

		return $minutes / 2;
	}
	/**
	 * Get the nightshift hours worked between 22h and 5h
	 * @return Carbon  A Carbon instance with the nightshift worked
	 */
	public function ExportNightHoursQT($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim)
	{

		$cloneentrada = clone $entrada;
		$clonesaida   = clone $saida;
		
		$dia_entrada = $this->eraseTime($cloneentrada);
		$dia_saida	 = $this->eraseTime($clonesaida);

		$halftime = $this->getHalfWorked($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim);
		
		$hora_entrada = clone $entrada;

		$saida = $hora_entrada->addMinutes($halftime);
		
		$data = Carbon::createFromFormat('Y-m-d H:i:s', substr($hora_entrada, 0,10).'00:00:00');

		$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio, $dia_entrada);
		
		$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $dia_entrada);
		
		if($dia_entrada->diffInDays($dia_saida) == 0 && $hora_entrada->hour < 5)
		{
			$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio, $data->subDay());
		}
		
		if($dia_entrada->diffInDays($dia_saida) > 0)
		{
			$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $data->addDay());
		}

		elseif($dia_entrada->diffInDays($dia_saida) == 0 && $saida->hour > 22)
		{
			$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $data->addDay());
		}
		
		if($saida > $noturno_inicio && $entrada->hour > 5)
		{
			$noturno_fim->addDay();
		}
		
		if(($dia_entrada->diffInDays($dia_saida) == 0) && ($entrada < $noturno_fim))
		{	

			if($saida > $noturno_fim)
			{
				$saida = $noturno_fim;
			}

			if($entrada < $noturno_inicio)
			{
				$entrada = $noturno_inicio;
			}
			
			$diffInMinutes =  $entrada->diffInMinutes($saida);
			//var_dump($entrada, $saida, $diffInMinutes);
			return $this->convertMinutesToCarbon($diffInMinutes);
		}
		
		else if($dia_entrada->diffInDays($dia_saida) > 0 )
		{						
			if($saida > $noturno_fim)
			{	
				$saida = $noturno_fim;
			}

			if($entrada > $noturno_inicio)
			{
				$noturno_inicio = $entrada;
			}
			if($saida > $noturno_inicio)
				$diffInMinutes =  $noturno_inicio->diffInMinutes($saida);
			else
				$diffInMinutes = 0;

			//var_dump($noturno_inicio, $saida, $diffInMinutes);
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		elseif($entrada > $noturno_fim && $saida->hour > '22')
		{
			$noturno_inicio->addDay();
			$diffInMinutes =  $saida->diffInMinutes($noturno_inicio);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		return $this->convertMinutesToCarbon(0);
	}	

	/**
	 * Get the nightshift hours worked between 22h and 5h
	 * @return Carbon  A Carbon instance with the nightshift worked
	 */
	public function ExportNightHoursA5($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim)
	{
		$cloneentrada = clone $entrada;
		$clonesaida   = clone $saida;

		$data = Carbon::createFromFormat('Y-m-d H:i:s', substr($entrada, 0,10).'00:00:00');

		$dia_entrada = $this->eraseTime($cloneentrada);
		$dia_saida	 = $this->eraseTime($clonesaida);

		$halftime = $this->getHalfWorked($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim);

		$hora_saida = clone $saida;

		$entrada = $hora_saida->subMinutes($halftime);

		$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio, $data);
		
		$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $data);

		if($dia_entrada->diffInDays($dia_saida) == 0 && $entrada->hour < 5)
		{
			$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio, $data->subDay());
		}
		
		if($dia_entrada->diffInDays($dia_saida) > 0)
		{
			$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $data->addDay());
		}

		elseif($dia_entrada->diffInDays($dia_saida) == 0 && $saida->hour > 22)
		{
			$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim, $data->addDay());
		}

		if(($dia_entrada->diffInDays($dia_saida) == 0) && ($entrada < $noturno_fim))
		{			

			if($saida > $noturno_fim)
			{
				$saida = $noturno_fim;
			}

			if($entrada < $noturno_inicio)
			{
				$entrada = $noturno_inicio;
			}

			$diffInMinutes =  $entrada->diffInMinutes($saida);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}
		
		else if($dia_entrada->diffInDays($dia_saida) > 0)
		{						

			if($saida > $noturno_fim)
			{	
				$saida = $noturno_fim;
			}

			if($entrada > $noturno_inicio)
			{
				$noturno_inicio = $entrada;
			}

			$diffInMinutes =  $noturno_inicio->diffInMinutes($saida);
			
			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		return $this->convertMinutesToCarbon(0);
	}	
	
	/**
	* function to create the txt file of nighthours to export to protheus
	* @param  $results query from controller
	* @param  $type type of matricula qt/a5
	* @param  $file path to create the file
	* @return 
	*/
	public function exportNightHours($results, $type, $file)
	{ 
		$cods  = array();
		$check = array();
		$contents='';
		$verba='229';
		$sigla = substr($type, 10);
		$demissao = 'data_demissao_'.$sigla;
		$matricula = 'matricula_'.$sigla;
		foreach($results as $horario) //horario referente a query não ao variavel da classe
		{

			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			if($horario->funcionario->$matricula!=null)
			{

				if(($horario->funcionario->matricula_qt!=null)&&($horario->funcionario->matricula_a5!=null))
				{
					if($type == 'matricula_qt' && $horario->data < $horario->funcionario->$demissao)
					{
						$noturnoQT = $this->ExportNightHoursQT($horario->hora_entrada, $horario->hora_saida, $horario->atividade, $horario->intervalo_inicio, $horario->intervalo_fim);
												
						Horario::addTotalHour($noturnoQT, 'N'.$horario->funcionario->matricula_qt);
					}
					else
					{
						if($horario->data < $horario->funcionario->$demissao)
						{
							$noturnoA5 = $this->ExportNightHoursA5($horario->hora_entrada, $horario->hora_saida, $horario->atividade, $horario->intervalo_inicio, $horario->intervalo_fim);
								
							Horario::addTotalHour($noturnoA5, 'N'.$horario->funcionario->matricula_a5);
						}
					}
				}
				else
				{	
					if($horario->data < $horario->funcionario->$demissao)
					{	
						Horario::addTotalHour($horario->getNightHours(), 'N'.$horario->funcionario->$type);
					}
				}
			}	
			//array com os codigos dos funcionarios
			if(!in_array('N'.$horario->funcionario->$type, $cods ))
			{
				array_push($cods, 'N'.$horario->funcionario->$type);
			}
			//teste para ver se o funcionario trabalha nas duas empresas
			if(($horario->funcionario->matricula_qt!=null)&&($horario->funcionario->matricula_a5!=null))
			{
				if(!in_array('N'.$horario->funcionario->$type, $check))
				{
					array_push($check, 'N'.$horario->funcionario->$type);
				}
			}
			
		}   

		foreach($cods as $cod)
		{	
			//muda o numero da verba se o tipo for a5
			if($type == 'matricula_a5')
				$verba='229';
			//se o usuario tiver duas empresas divide o total por 2
			if(in_array($cod, $check))
			{
				$valor=((Horario::getTotalHour($cod))*1.14);

				if($this->checkCod($cod))
					$contents = str_pad((str_replace('N', '', $cod)), 6, "0", STR_PAD_LEFT).'  '.(substr(str_pad(number_format($valor, 2),5," "),0,5)).'   '.$verba."\r\n";
			}
			else //se tiver uma empresa apenas imprime o total
			{
				$valor=(Horario::getTotalHour($cod)*1.14);

				if($this->checkCod($cod))
					$contents = str_pad((str_replace('N', '', $cod)), 6, "0", STR_PAD_LEFT).'  '.(substr(str_pad(number_format($valor, 2),5," "),0,5)).'   '.$verba."\r\n";
			}

			$this->writeFile($cod, $file, $contents);
		}
	}

	/**
	* function to create the txt file of extra hours to export to protheus
	* @param  $results query from controller
	* @param  $type type of matricula qt/a5
	* @param  $file path to create the file
	* @return 
	*/
	public function exportActivity($results, $type, $file, $activity)
	{ 
		$cods  = array();
		$check = array();
		$contents='';
		$verba='409';
		$sigla = substr($type, 10);
		$demissao = 'data_demissao_'.$sigla;

		if($activity == 'Folga')
			$verba='N/A';

		if($activity == 'am')
				$verba='351';

		if($activity == 'va')
				$verba='424';

		if($activity == 'vt')
				$verba='451';
		
		if($activity == 'Atestado')
				$verba='118';

		foreach($results as $horario)
		{
			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			$beneficios = explode(',', $horario->beneficios);

			if(($horario->atividade==$activity) && ($horario->data < $horario->funcionario->$demissao))
				Horario::addActivityDay(($activity.$horario->funcionario->$type), 'Atividade');

		 	if((in_array($activity, $beneficios)) && ($horario->data < $horario->funcionario->$demissao))
		 		Horario::addActivityDay(($activity.$horario->funcionario->$type), 'Atividade');

			//array com os codigos dos funcionarios
			if(!in_array($activity.$horario->funcionario->$type, $cods ))
			{
				array_push($cods, $activity.$horario->funcionario->$type);
			}
		}

		foreach($cods as $cod)
		{
			if($this->checkCod($cod, $activity))
				{
					$valor=(Horario::getTotalActivity($cod, 'Atividade'));

					$contents =  str_pad((str_replace($activity, '', $cod)), 6, "0", STR_PAD_LEFT).'  '.str_pad($valor,8," ").$verba."\r\n";
				}
			
			$this->writeFile($cod, $file, $contents);
		}
	}

	/**
	* function to check cod condition
	* @param  string $cod cod of the employee
	* @return boolean 
	*/
	public function checkCod($cod, $activity=null)
	{
		if(((Horario::getTotalHour($cod)!='0.00')&&($cod!=null)&&($cod!='N'))||
			((Horario::getTotalActivity($cod, 'Atividade')!='0')&&($cod!=null)&&($cod!=$activity)))
			return true;
		else
			return false;
	}

	/**
	* function to create the txt file of extra hours to export to protheus
	* @param  string $cod cod of the employee
	* @param  string $file file path
	* @param  string $contents data to save in the file
	* @return 
	*/
	public function writeFile($cod, $file, $contents)
	{
		if(File::exists($file) && $this->checkCod($cod))
			File::append($file, $contents);	

		elseif($this->checkCod($cod))
			File::put($file, $contents);
	}

	

	/**
	 * Sum Total Hours
	 * @param  Carbon $date Date to be compared to get the hours
	 * @param  name of the field that you want to be counted
	 * @return $date given
	 */
	public static function addFieldTotal($key, $sum)
	{		
		
		if(!isset(self::$totals[$key]['sum']))
			self::$totals[$key]['sum'] = 0;
		
		self::$totals[$key]['sum'] += $sum;
		
		return $sum;
	}

	/**
	*  Get the Sum of the Total Atividades days and sum benefits
	* @param  Atividade counted
	* @param  name of the field that you want to be counted
	* @return Time in minutes
	*/
	public static function getFieldTotal($key)
	{	
		if(!isset(self::$totals[$key]['sum']))
		{
			self::$totals[$key]['sum'] = 0;
		}

		return self::$totals[$key]['sum'];
	}

	public static function allToExcel($nome, $horarios)
	{	
            Excel::create($nome , function ($excel) use ($horarios) {

            $excel->sheet('Marcações', function ($sheet) use ($horarios) {

                $sheet->row(1, array(

                            'Unidade',
                            'Nome',
                            // 'Função QT',
                            // 'Função A5',
                            'Data',
                            'Entrada',
                            'Intervalo',
                            'Intervalo',
                            'Saida',
                            'Horas Totais',
                            'Horas Trabalhadas',
                            'Horas Disposicao',
                            'Horas Extras',
                            'Hora Contratada',
                            'Tempo Exposicao',
                            'Atividade',
                            'Centro de Custo',
                            'Observacoes'
                        ));

                $sheet->row(1, function($row) {
                    $row->setBackground('#2A8005');
                    $row->setFontColor('#ffffff');
                    $row->setFontWeight('bold');
                });

                $i = 2;
                foreach ($horarios as $horario) {
                        
                        // if($horario->funcionario->funcao_qt != null)
                        //     $funcaoqt = $horario->funcionario->funcao_qt;
                        // else   
                        //     $funcaoqt = '-';

                        // if($horario->funcionario->funcao_a5 != null)
                        //     $funcaoa5 = $horario->funcionario->funcao_a5;
                        // else   
                        //     $funcaoa5 = '-';
        
                        $sheet->row($i, array(
                            $horario->unidade,
                            $horario->funcionario->nome,
                            // $funcaoqt,
                            // $funcaoa5,
                            $horario->data->format('d/m/Y'),
                            $horario->hora_entrada->format('H:i'),
                            $horario->intervalo_inicio->format('H:i'),
                            $horario->intervalo_fim->format('H:i'),
                            $horario->hora_saida->format('H:i'),
                            $horario->getWorkedHours()->format('H:i'),
                            $horario->getClientHours()->format('H:i'),
                            $horario->getDisposicaoHours()->format('H:i'),
                            $horario->getExtraHours()->format('H:i'),
                            $horario->hora_contratada,
                            $horario->tempo_exposicao ?: "-",
                            $horario->atividade,
                            $horario->item_contabil->CTD_DESC01,
                            $horario->observacoes
                        ));
                        if($i % 2 != 0)
                        {
                        $sheet->row($i, function($row) {
                            $row->setBackground('#D1D1D1');
                            });
                        }    
                
                        $i++;
                    }
            });

            })->download('xlsx');
		
	}


}//end of class
