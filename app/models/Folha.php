<?php 

use Carbon\Carbon;

class Folha extends Eloquent 
{
	 

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('cod');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_horario';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'cod';
	

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;


	protected static $totals = array();

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(		
		'data' 			  => 'required|date_format:"d/m/Y"',
		'cod_funcionario' => 'required|integer',
		'hora_entrada' 	  => 'required|date_format:"H:i"',
		'hora_saida' 	  => 'required|date_format:"H:i"',
		'intervalo_inicio'=> 'required|date_format:"H:i"',
		'intervalo_fim'   => 'required|date_format:"H:i"',
		'hora_faturada'   => 'required|integer',
		'hora_contratada' => 'required|integer',
		'cod_item_contab' => 'required|integer',
		'atividade' 	  => 'required',
		);


	const noturno_inicio = '22:00';
	const noturno_fim 	= '05:00';

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function funcionario()
	{
		return $this->belongsTo('Funcionario', 'cod_funcionario');
	}


	/**
	 * Define the relationship with item contabil	 
	 */
	public function item_contabil()
	{
		return $this->belongsTo('ItemContabil', 'cod_item_contab');
	}

	/**
	 * Define the relationship with Guia
	 */

	public function guia()
	{
		return $this->belongsTo('Guia', 'cod_go');
	}

	/**
	 * Transform the array fields in date instances of Carbon\Carbon
	 * @return array array of date fields
	 */
	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'hora_entrada', 
			'hora_saida', 
			'intervalo_inicio', 
			'intervalo_fim', 
			'data'
			);
	}

	/**
	 * Check if funcionario have matricula_a5 and _qt to divide the hours between both
	 * @return true or false
	 */
	public function checkMatriculas()
	{
		if( ($this->funcionario->matricula_qt && $this->funcionario->matricula_a5) != null)
			return true;
		else
			return false;
	}

	/**
	 * return a day worked time in minutes
	 * @return integer minutes worked
	 */
	public function getWorkedTimeInMinutes()
	{		
		$timeDiff 		= $this->hora_entrada->diffInMinutes($this->hora_saida); 
		$breakTimeDiff 	= $this->intervalo_inicio->diffInMinutes($this->intervalo_fim); 
		$timeDiff 	   -= $breakTimeDiff;		

		return abs($timeDiff);
	}

	/**
	 * Convert minutes in a Carbon date
	 * @param  integer $minutes
	 * @return \Carbon\Carbon
	 */
	public function convertMinutesToCarbon($minutes)
	{

		if($this->checkMatriculas() == true)
		{
			$minutes = $minutes/2;
		}

		$hours 	   = floor($minutes / 60);
		$minutes   = (int) $minutes % 60;
		
		return Carbon::createFromTime($hours, $minutes);
	}

	/**
	 * Get the extra hours worked over the horas_contratadas
	 * @return Carbon  A Carbon instance with the extra time worked
	 */
	public function getExtraHours()
	{		
		$contractedHours   = $this->hora_contratada;
		$contractedMinutes = $contractedHours*60;
		$workedMinutes 	   = $this->getWorkedTimeInMinutes();

		if($workedMinutes > $contractedMinutes)
		{
			return $this->convertMinutesToCarbon($workedMinutes - $contractedMinutes);
		}
		return $this->convertMinutesToCarbon(0);
	}

	/**
	 * Change the time to 00:00:00 of the date
	 * @return Carbon  A Carbon instance the date wanted to be erased
	 */
	public function eraseTime(Carbon $date)
	{		
		$date->minute = 0;
		$date->hour = 0;
		$date->second = 0;

		return $date;
	}

	/**
	 * Get the beginning of the night shift
	 * @return Carbon  A Carbon instance with the begin of the nightshift
	 */
	public function getNigthBegin()
	{
		$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio);

		$hora_entrada = $this->eraseTime($this->hora_entrada);
		$hora_saida	  = $this->eraseTime($this->hora_saida);

		if( $hora_entrada->diffInDays($hora_saida) == 0)
		{
			$noturno_inicio = $this->completeHourWithDate(Horario::noturno_inicio, $this->data->subDay());
		}

		return $noturno_inicio;
	}

	/**
	 * Get the end of the night shift
	 * @return Carbon  A Carbon instance with the end of the nightshift
	 */
	public function getNigthEnd()
	{
		$noturno_fim = $this->completeHourWithDate(Horario::noturno_fim);

		$hora_entrada = $this->eraseTime($this->hora_entrada);
		$hora_saida	  = $this->eraseTime($this->hora_saida);

		if( $hora_entrada->diffInDays($hora_saida) > 0)
		{
			$noturno_fim 	= $this->completeHourWithDate(Horario::noturno_fim, $this->data->addDay());
		}

		return $noturno_fim;
	}

	/**
	 * Get the nightshift hours worked between 22h and 5h
	 * @return Carbon  A Carbon instance with the nightshift worked
	 */
	public function getNightHours()
	{	
		$saida = $this->hora_saida;
		$entrada = $this->hora_entrada;
		$intervalo_inicio = $this->intervalo_inicio;
		$intervalo_fim = $this->intervalo_fim;

		$breakTimeDiff = 0;

		$noturno_inicio = $this->getNigthBegin();
		$noturno_fim    = $this->getNigthEnd();

				
		if($saida > $noturno_inicio)
		{
			if($saida > $noturno_fim)
			{
				$saida = $noturno_fim;
			}

			if($entrada > $noturno_inicio)
			{
				$noturno_inicio = $entrada;
			}

			if($intervalo_fim > $noturno_fim)
			{
				$intervalo_fim = $noturno_fim;
			}

			if($intervalo_inicio < $noturno_inicio)
			{
				$intervalo_inicio = $noturno_inicio;
			}

			if(($intervalo_fim > $noturno_inicio) && ($intervalo_inicio < $noturno_fim))
			{
				$breakTimeDiff 	= $intervalo_inicio->diffInMinutes($intervalo_fim); 
			}			
			
			$diffInMinutes =  $noturno_inicio->diffInMinutes($saida);
			$diffInMinutes -= $breakTimeDiff;

			return $this->convertMinutesToCarbon($diffInMinutes);
		}

		return $this->convertMinutesToCarbon(0);
	}	
		
	/**
	 * Complete a Hour with the date givem
	 * @param  string $hour Hour in the format 'H:i'	 
	 * @param  Carbon $date Date to be inserted in the hour
	 * @param  string $hour_format Format of the Hour default 'H:i'
	 * @return Carbon $hour;
	 */
	public function completeHourWithDate($hour, Carbon $date = null, $hour_format = 'H:i:s')
	{
		if (!isset($date)) {
			$date = $this->data;
		}

		//if the hour has no seconds add :00 in the end
		$hour .= (count(explode(':',$hour)) < 3 ? ':00' : '' );

		//create a carbon date to be completed
		$hour = Carbon::createFromFormat($hour_format, $hour);		

		//complete the hour with the carbon date
		$hour->year  = $date->year;
		$hour->month = $date->month;
		$hour->day   = $date->day;

		return $hour;
	}

	/*== Convert  Dates to Save on DB ==*/
	public function setDataAttribute($value)
	{
		$this->attributes['data']  = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function setHoraEntradaAttribute($value)
	{	
		$this->attributes['hora_entrada'] = $this->completeHourWithDate($value);		
	}

	public function setHoraSaidaAttribute($value)
	{		
		$hora_saida = $this->completeHourWithDate($value);

		if ($hora_saida->hour < $this->hora_entrada->hour) {
			$hora_saida->addDay();
		}


		$this->attributes['hora_saida'] = $hora_saida;
	}

	public function setIntervaloInicioAttribute($value)
	{
		$this->attributes['intervalo_inicio'] = $this->completeHourWithDate($value);
	}

	public function setIntervaloFimAttribute($value)
	{

		$intervalo_fim = $this->completeHourWithDate($value);

		if ($intervalo_fim->hour < $this->intervalo_inicio->hour) {
			$intervalo_fim->addDay();
		}

		$this->attributes['intervalo_fim'] = $intervalo_fim;
	}	
	/*== End Convert  Date to Save on DB ==*/

	/**
	 * Sum Total Hours
	 * @param  Carbon $date Date to be compared to get the hours
	 * @param  name of the field that you want to be counted
	 * @return $date given
	 */
	public static function addTotalHour(Carbon $date, $key)
	{		
		if(! isset(self::$totals[$key]))
		{
			self::$totals[$key] = array('carbon' => new Carbon('00:00:00'));
		}

		if(!isset(self::$totals[$key]['minutes']))
			self::$totals[$key]['minutes'] = 0;

		
		self::$totals[$key]['minutes'] = self::$totals[$key]['carbon']->diffInMinutes($date);
		

		self::$totals[$key]['carbon']->addHours($date->hour);
		self::$totals[$key]['carbon']->addMinutes($date->minute);

		return $date;
	}

	/**
	*  Get the Sum of the Total Hours
	* @param  name of the field that you want to be counted
	* @return Time in minutes
	*/
	public static function getTotalHour($key)
	{
		return self::$totals[$key]['minutes'] / 60;
	}
}
