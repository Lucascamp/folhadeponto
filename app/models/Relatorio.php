<?php

use Carbon\Carbon;

class Relatorio extends Horario
{
	public function relatorioHE($results, $type, $datainicio)
	{ 
		$mats  = array();
		$check = array();

		$demissao = 'data_demissao_'.$type;
		$matricula = 'matricula_'.$type;

		foreach($results as $horario)
		{
			
			if($horario->funcionario->$demissao != null && ($datainicio > $horario->funcionario->$demissao))
            	$horario->funcionario->$matricula = null;

			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			if($horario->data->format('Y-m-d') < $horario->funcionario->$demissao)
			{
				Horario::addTotalHour($horario->getExtraHours(), $horario->funcionario->$matricula);
			} 
			
			//array com os codigos dos funcionarios
			if(!in_array($horario->funcionario->$matricula, $mats) && $horario->funcionario->$matricula != '')
			{
				array_push($mats, $horario->funcionario->$matricula);
			}
			
			//teste para ver se o funcionario trabalha nas duas empresas
			if(($horario->funcionario->matricula_qt!=null)&&($horario->funcionario->matricula_a5!=null))
			{
				if(!in_array($horario->funcionario->$matricula, $check))
				{
					array_push($check, $horario->funcionario->$matricula);
				}
			} 
		}
		
		foreach($mats as $mat)
		{
			//se o usuario tiver duas empresas divide o total por 2
			if(in_array($mat, $check))
			{
				$hours[$mat] = floatval((Horario::getTotalHour($mat)/2));   
			}
			else //se tiver uma empresa apenas imprime o total
			{	
				$hours[$mat] = floatval(Horario::getTotalHour($mat)); 
			}

			if($hours[$mat] == 0 || $mat == '')
				unset($hours[$mat]);
		}
		
		return $hours;
	}

	
	public function relatorioADN($results, $type, $datainicio)
	{ 
		$mats  = array();
 			
		$demissao = 'data_demissao_'.$type;
		$matricula = 'matricula_'.$type;
		$funcao = 'ExportNightHours'.strtoupper($type);

		if($matricula == 'matricula_qt')
			$matricula_inv = 'matricula_a5';

		elseif($matricula == 'matricula_a5')
			$matricula_inv = 'matricula_qt';

		if($demissao == 'data_demissao_qt')
			$demissao_inv = 'data_demissao_a5';

		elseif($demissao == 'data_demissao_a5')
			$demissao_inv = 'data_demissao_qt';
		 
		foreach($results as $horario)
		{
			if($horario->funcionario->$demissao != null && ($datainicio > $horario->funcionario->$demissao))
            	$horario->funcionario->$matricula = null;

			if($horario->funcionario->$demissao_inv != null && ($datainicio > $horario->funcionario->$demissao_inv))
            	$horario->funcionario->$matricula_inv = null;

			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			if($horario->funcionario->$demissao_inv == null || $horario->funcionario->$demissao_inv == '')
				$horario->funcionario->$demissao_inv = $horario->data->addYear(1);

			if($horario->funcionario->matricula_qt != null && $horario->funcionario->matricula_a5 != null)
			{
				if($horario->data->format('Y-m-d') < $horario->funcionario->$demissao)
				{
					Horario::addTotalHour($horario->$funcao($horario->hora_entrada, $horario->hora_saida, $horario->atividade, $horario->intervalo_inicio,$horario->intervalo_fim), 'N'.$horario->funcionario->$matricula);
				} 
			}
			elseif($horario->funcionario->matricula_qt != null || $horario->funcionario->matricula_a5 != null)
			{
				if($horario->data->format('Y-m-d') < $horario->funcionario->$demissao)
				{
					Horario::addTotalHour($horario->getNightHours(), 'N'.$horario->funcionario->$matricula);
				} 
			}

			if(!in_array($horario->funcionario->$matricula, $mats ))
			{
				array_push($mats, $horario->funcionario->$matricula);
			}
		}
		
		foreach($mats as $mat)
		{
			$hours[$mat] = floatval((Horario::getTotalHour('N'.$mat)*1.14));
			if($hours[$mat] == 0 || $mat == '')
				unset($hours[$mat]);
		}
		
		return $hours ?: 0;
	}

	public function relatorioHN($results, $type, $datainicio)
	{ 
		
		$mats  = array();
 			
		$demissao = 'data_demissao_'.$type;
		$matricula = 'matricula_'.$type;
		$funcao = 'horaNormal'.strtoupper($type);

		if($demissao == 'data_demissao_qt')
			$demissao_inv = 'data_demissao_a5';

		elseif($demissao == 'data_demissao_a5')
			$demissao_inv = 'data_demissao_qt';

		if($matricula == 'matricula_qt')
			$matricula_inv = 'matricula_a5';

		elseif($matricula == 'matricula_a5')
			$matricula_inv = 'matricula_qt';

		foreach($results as $horario)
		{
			if($horario->funcionario->$demissao != null && ($datainicio > $horario->funcionario->$demissao))
            	$horario->funcionario->$matricula = null;

            if($horario->funcionario->$demissao_inv != null && ($datainicio > $horario->funcionario->$demissao_inv))
            	$horario->funcionario->$matricula_inv = null;

			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			if($horario->funcionario->$demissao_inv == null || $horario->funcionario->$demissao_inv == '')
				$horario->funcionario->$demissao_inv = $horario->data->addYear(1);

			if($horario->funcionario->matricula_qt != null && $horario->funcionario->matricula_a5 != null)
			{

				if($horario->data->format('Y-m-d') >= $horario->funcionario->$demissao_inv)
				{
					Horario::addTotalHour($this->horaNormalTotal($horario->hora_entrada, $horario->hora_saida, $horario->atividade, $horario->intervalo_inicio,$horario->intervalo_fim), 'HN'.$horario->funcionario->$matricula);
				} 

				elseif($horario->data->format('Y-m-d') < $horario->funcionario->$demissao)
				{
					Horario::addTotalHour($this->$funcao($horario->hora_entrada, $horario->hora_saida, $horario->atividade, $horario->intervalo_inicio,$horario->intervalo_fim), 'HN'.$horario->funcionario->$matricula);
				} 
			}
			elseif($horario->funcionario->matricula_qt != null || $horario->funcionario->matricula_a5 != null)
			{
				if($horario->data->format('Y-m-d') < $horario->funcionario->$demissao)
				{
					Horario::addTotalHour($this->horaNormalTotal($horario->hora_entrada, $horario->hora_saida, $horario->atividade, $horario->intervalo_inicio,$horario->intervalo_fim), 'HN'.$horario->funcionario->$matricula);
				} 
			}
			if(!in_array($horario->funcionario->$matricula, $mats ))
			{
				array_push($mats, $horario->funcionario->$matricula);
			}
		}
		
		foreach($mats as $mat)
		{
			$hours[$mat] = floatval(Horario::getTotalHour('HN'.$mat));

			if($hours[$mat] == 0 || $mat == '')
				unset($hours[$mat]);
		}
		
		return $hours ?: 0;
	}

	public function relatorioHF($results, $type, $datainicio)
	{ 
		$mats  = array();
 			
		$demissao = 'data_demissao_'.$type;
		$matricula = 'matricula_'.$type;
		$funcao = 'horaNormal'.strtoupper($type);
		
		$now = Carbon::now();

		$now->hour = 0;
		$now->minute = 0;
		$now->second = 0;
		
		if($demissao == 'data_demissao_qt')
			$demissao_inv = 'data_demissao_a5';

		elseif($demissao == 'data_demissao_a5')
			$demissao_inv = 'data_demissao_qt';

		foreach($results as $horario)
		{
			$faturada = Carbon::now();

			$faturada->hour = $horario->hora_faturada->hour;
			$faturada->minute = $horario->hora_faturada->minute;
			$faturada->second = $horario->hora_faturada->second;

			if($horario->funcionario->$demissao != null && ($datainicio > $horario->funcionario->$demissao))
            	$horario->funcionario->$matricula = null;

			if($horario->funcionario->$demissao == null || $horario->funcionario->$demissao == '')
				$horario->funcionario->$demissao = $horario->data->addYear(1);

			if($horario->data->format('Y-m-d') >= $horario->funcionario->$demissao_inv)
			{
				Horario::addTotalHour($horario->convertMinutesToCarbon($now->diffInMinutes($faturada)), 'HF'.$horario->funcionario->$matricula);
			} 

			elseif(($horario->data->format('Y-m-d') < $horario->funcionario->$demissao) && ($horario->funcionario->matricula_qt != null || $horario->funcionario->matricula_a5 != null))
			{
				Horario::addTotalHour($horario->convertMinutesToCarbon($now->diffInMinutes($faturada)/2), 'HF'.$horario->funcionario->$matricula);
			} 
			
			if(!in_array($horario->funcionario->$matricula, $mats ))
			{
				array_push($mats, $horario->funcionario->$matricula);
			}
		}
		
		foreach($mats as $mat)
		{
			$hours[$mat] = floatval(Horario::getTotalHour('HF'.$mat));

			if($hours[$mat] == 0 || $mat == '')
				unset($hours[$mat]);
		}
		return $hours ?: 0;
	}

	/**
	* function to create sum the activitys days for the general report
	* @param  $results query from controller
	* @param  $type type of matricula qt/a5
	* @param  $file path to create the file
	* @return 
	*/
	public function reportActivity($results, $activity)
	{ 
		$cods  = array();
			
		foreach($results as $horario)
		{
			$beneficios = explode(',', $horario->beneficios);
			
			if($horario->atividade==$activity)
		 		Horario::addActivityDay(($activity.$horario->funcionario->cod), 'Atividade');

		 	if(in_array($activity, $beneficios))
		 		Horario::addActivityDay(($activity.$horario->funcionario->cod), 'Atividade');

			//array com os codigos dos funcionarios
			if(!in_array($activity.$horario->funcionario->cod, $cods ))
			{
				array_push($cods, $activity.$horario->funcionario->cod);
			}
		}
		
		foreach($cods as $cod)
		{
			$day[$cod]=(Horario::getTotalActivity($cod, 'Atividade'));
		}

		return $day ?: 0;
	}
	

	public function horaNormalQT($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim)
	{
		$cloneentrada = clone $entrada;
		
		$halftime = $this->getHalfWorked($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim);
		
		$saida = $cloneentrada->addMinutes($halftime);

		$workedMinutes = $entrada->diffInMinutes($saida);
		
		return $this->convertMinutesToCarbon($workedMinutes);
	}

	public function horaNormalA5($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim)
	{
		$clonesaida = clone $saida;
		
		$halftime = $this->getHalfWorked($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim);
		
		$entrada = $clonesaida->subMinutes($halftime);

		$workedMinutes = $entrada->diffInMinutes($saida);

		

		return $this->convertMinutesToCarbon($workedMinutes);
	}

	public function horaNormalTotal($entrada, $saida, $atividade, $intervalo_inicio, $intervalo_fim)
	{
		$timeDiff= $entrada->diffInMinutes($saida);

		if(($atividade == 'Afastado') || ($atividade == 'Treinamento') || ($atividade == 'Falta') || ($atividade == 'Domingo')
		 || ($atividade == 'Atestado') || ($atividade == 'Folga') || ($atividade == 'Férias'))
			$breakTimeDiff = 0;
		else
			$breakTimeDiff = $intervalo_inicio->diffInMinutes($intervalo_fim);
		 
		$timeDiff -= $breakTimeDiff;
		$minutes = abs($timeDiff);

		return $this->convertMinutesToCarbon($minutes);
	}

}