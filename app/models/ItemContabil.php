<?php

class ItemContabil extends Eloquent {


	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('cod');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'tb_item_contab';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'cod';


	/**
	 * Define the relationship with Funcionario	 
	 */
	public function funcionario()
	{
		return $this->hasMany('Funcionario', 'cod_item_contab');
	}


	/**
	 * Define the relationship with Funcionario	 
	 */
	public function guias()
	{
		return $this->hasMany('Guia', 'item_contab', 'CTD_ITEM');
	}
	
}
