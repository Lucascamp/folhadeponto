<?php

class Ferias extends Eloquent 
{

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = array('cod');

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ferias';

	/**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey   = 'cod';
	

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the relationship with Funcionario	 
	 */
	public function funcionario()
	{
		return $this->belongsTo('Funcionario', 'cod_funcionario');
	}

}