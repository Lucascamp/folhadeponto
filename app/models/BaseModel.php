<?php

class BaseModel extends Eloquent {

	protected static $columnsNames;

	/**
     * Columns properties to datatable. TODO: move to model.
     * @var array
     */
    public $aoColumns;

	/**
	 * Return the table Collumn Names
	 * @return array
	 */
	public function getColumnsNames()
	{
		if($this->columnsNames)
			return $this->columnsNames;

		$connection = DB::connection();
		$connection->getSchemaBuilder();

		$table 	 = $connection->getTablePrefix() . $this->table;
		$grammar = $connection->getSchemaGrammar();
		$results = $connection->select($grammar->compileColumnExists(), array($connection->getDatabaseName(), $table));

		return $connection->getPostProcessor()->processColumnListing($results);
	}


	public function updateField($key, $value, $id, $label = null)
	{

		$attr  = array($key => $value);		
		$class = get_class($this);
		
		$rules = array($key => $class::$rules[$key]);

		$validator = Validator::make($attr, $rules);

		if ($validator->fails())
		{
			return $validator->messages()->first($key);
		}

		$model = $class::find($id);
		$model->$key = $value;

		if($model->save())
		{
			$model->where('cod', $id)->update(array('updated_by' => Auth::user()->cod));
			
			return $label ?: $value;
		}

	}

	public function addDTProperty($column, $property, $value)
	{
		if (isset($column))
			return $this->aoColumns[$column][$property] = $value;

		return $this->aoColumns[][$property] = $value;
	}


	public function addDTProperties($property, $values)
    {
        $result = array();

        foreach ($values as $column => $value) 
        {
			$result[] = $this->addDTProperty($column, $property, $value);
        }        

        return $this->aoColumns;
    }


	public function getColumnsKeys()
	{
		$class = get_class($this);
		return array_keys($class::$columns);
	}

	public function getColumnsLabels()
	{
		$class = get_class($this);
		return array_values($class::$columns);
	}
}