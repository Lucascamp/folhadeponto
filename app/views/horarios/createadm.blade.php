@extends('layouts.scaffold2')

@section('main')

@if(User::checkPermission('visualizar_administrativo'))

{{ Form::open(array('route' => 'horarios.storeadm', 'class'=>'form-inline')) }}

{{ Form::hidden('created_by',  Auth::user()->cod); }}
{{ Form::hidden('unidade',  Auth::user()->unidade); }}
{{ Form::hidden('cod_item_contab',  '107'); }}
{{ Form::hidden('atividade',  'Hora Direta'); }}
{{ Form::hidden('cod_funcionario', Auth::user()->cod_funcionario) }}
{{ Form::hidden('data', $hoje) }}

@if(isset($marcacao->cod))
{{ Form::hidden('cod', $marcacao->cod) }}
@endif

<div class="col-md-offset-3 col-md-6">
    <div class="fieldsetadm">

        @if($errors->any())
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ implode('', $errors->all('<p>:message</p>')) }}
            </div>
        </div>

        @elseif (Session::has('message1'))
        <div class="alert-group">
            <div class="alert alert-success">
                <p><center>{{ Session::get('message1') }}<div id='mensagem'></div></center></p>
            </div>
        </div>

        @else
        <h3><center>Ponto do dia: {{ $hoje }}</center></h3>
        <div class="clear"><br></div>
        @endif

        <div class="form-group col-md-12">
            {{ Form::label('funcionario', 'Funcionário: '.Auth::user()->nome) }}
        </div>

        <div class="clear"><br></div>     

        <div class="form-group col-md-3">
            {{ Form::label('hr_entrada', 'Entrada:') }}

            {{ Form::text('hora_entrada', isset($marcacao) ? $marcacao->hora_entrada->format('H:i') :'00:00', array('class'=>'mask-time form-control', 'id'=>'input_hora_entrada', 'size' => '3')) }}

            <input type="button" value="Entrada" name="timer" class="time btn btn-default" id="hora_entrada">
        </div>

        <div class="form-group col-md-3">
            {{ Form::label('int_inicio', 'Intervalo:') }}
            {{ Form::text('intervalo_inicio', isset($marcacao) ? $marcacao->intervalo_inicio->format('H:i') :'00:00', array('class'=>'mask-time form-control', 'id'=>'input_intervalo_inicio', 'size' => '3')) }}

            <input type="button" value="Intervalo" name="timer" class="time btn btn-default" id="intervalo_inicio">
        </div>

        <div class="form-group col-md-3">
            {{ Form::label('int_fim', 'Intervalo:') }}
            {{ Form::text('intervalo_fim', isset($marcacao) ? $marcacao->intervalo_fim->format('H:i') :'00:00', array('class'=>'mask-time form-control', 'id'=>'input_intervalo_fim', 'size' => '3')) }}

            <input type="button" value="Intervalo" name="timer" class="time btn btn-default" id="intervalo_fim">
        </div>

        <div class="form-group col-md-3">
            {{ Form::label('hr_saida', 'Saída:') }}
            {{ Form::text('hora_saida', isset($marcacao) ? $marcacao->hora_saida->format('H:i') :'00:00', array('class'=>'mask-time form-control', 'id'=>'input_hora_saida', 'size' => '3')) }}

            <input type="button" value="Saída" name="timer" class="time btn btn-default" id="hora_saida">
        </div>


        {{ Form::hidden('hora_faturada', '00:00') }}
        {{ Form::hidden('hora_contratada', '8')  }}

        <div class="clear"><br></div>

        <div class="form-group col-md-12">
            {{ Form::textarea('observacoes', isset($marcacao) ? $marcacao->observacoes : null, array('class'=>'form-control', 'style' => 'width:100%', 'placeholder' => 'Observações', 'rows' => '2' )) }}
        </div>

        <div class="form-group col-md-12">

            <center>
                {{ Form::submit('Salvar marcação', array('class' => 'btn btn-success', 'id' => 'salvar')) }}
               {{--  <a href="{{ URL::previous() }}" class='btn btn-info'>Retornar a tela inicial</a> --}}
            </center>
            {{ Form::close() }}

            <div class="clear"><br></div>

        </div>

    </div>
</div>

@else
    @include('accessdenied')
@endif

@stop
