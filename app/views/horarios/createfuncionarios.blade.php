@extends('layouts.scaffold')

<?php use Carbon\Carbon; ?>

@section('main')

@if(User::checkPermission('visualizar_cadastrar'))

<h1>Marcação de funcionários por unidade</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'horarios.salvarpontofuncionarios', 'class'=>'form-inline')) }}

{{ Form::hidden('created_by',  Auth::user()->cod); }}

    <div class="form-group col-md-1">
        {{ Form::label('labelunidade', 'Unidade:') }}
        {{ Form::select('unidade', $unidades, null, array('class' => 'unidadeFuncionarios chosen-select')) }}
    </div>

    <?php 
        $funcionarios = Session::get('funcionarios');
        $funcionarios_selected = Session::get('funcionarios_selected');
    ?>

    @if(isset($funcionarios))
        <?php dd($funcionarios); ?> 
        <div class="form-group col-md-4">
            {{ Form::label('cod_funcionario', 'Funcionários da unidade:') }}
            {{ Form::select('cod_funcionarios[]', $funcionarios, $funcionarios_selected , array('class' => 'chosen-select funcionariosUnidade','multiple'=> 'multiple', 'data-placeholder'=>"Escolha os funcionários", 'id' => 'funcionariosUnidade', 'style' =>'height: 30px;')) }}
        </div>
    @else    
        <div class="form-group col-md-4">
            {{ Form::label('cod_funcionario', 'Funcionários da unidade:') }}
            {{ Form::select('cod_funcionarios[]', [], null, array('class' => 'chosen-select funcionariosUnidade','multiple'=> 'multiple', 'data-placeholder'=>"Escolha os funcionários", 'id' => 'funcionariosUnidade', 'style' =>'height: 30px;')) }}
        </div>
    @endif
    <div class="form-group col-md-1">
    </div>

    <div class="form-group col-md-6" id="beneficios-inputs"> 
        {{ Form::label('beneficios', 'Selecione os Benefícios:') }}
        {{ Form::checkbox('beneficio_tipo_va', 'va', false, array('class'=>'beneficio','id'=>'beneficio_tipo_va'))}}<label for="beneficio_tipo_va"> Alimentação</label>
        {{ Form::checkbox('beneficio_tipo_vt', 'vt', false, array('class'=>'beneficio','id'=>'beneficio_tipo_vt'))}}<label for="beneficio_tipo_vt">Vale Transporte</label>
        {{ Form::checkbox('beneficio_tipo_am', 'am', false, array('class'=>'beneficio','id'=>'beneficio_tipo_am'))}}<label for="beneficio_tipo_am">Auxílio Moradia</label>  
        {{ Form::hidden('beneficios', null) }}
    </div>

<div class="clear"><br><br></div>  

<fieldset>

    <div class="form-group col-md-2">
        {{ Form::label('data', 'Data:') }}
        {{ Form::text('data', Carbon::now()->format('d/m/Y'), array('class'=>'form-control datepicker', 'size' => '9', 'id'=>'data')) }}
    </div>

    <div class="form-group col-md-3">
        {{ Form::label('lblatividade', 'Atividade:') }}
        {{ Form::select('atividade', $atividades, 
            'Hora Direta', array('style' => 'width:250px', 'class' => 'atividade chosen-select', 'id' => 'atividade')) }}
    </div>

    <div class="form-group col-md-3">
        {{ Form::label('cod_item_contab', 'Centro de Custo:') }}
        {{ Form::select('cod_item_contab', $itens_contabil, null ,array('style' => 'width:300px', 'class' => 'contab chosen-select', 'id' => 'contab')) }}      
    </div>
    
<div class="clear"><br></div>

<div id='divhorario' class='divhorario'>    

        <div class="form-group col-md-1">
            {{ Form::label('hr_entrada', 'Entrada:') }}
            {{ Form::text('hora_entrada', null, array('class'=>'mask-time form-control', 'id'=>'hora_entrada', 'size' => '2')) }}
        </div>
  
        <div class="form-group col-md-1">
            {{ Form::label('int_inicio', 'Intervalo:') }}
            {{ Form::text('intervalo_inicio', null, array('class'=>'mask-time form-control', 'id'=>'intervalo_inicio', 'size' => '2')) }}
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('int_fim', 'Intervalo:') }}
            {{ Form::text('intervalo_fim', null, array('class'=>'mask-time form-control', 'id'=>'intervalo_fim', 'size' => '2')) }}
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('hr_saida', 'Saída:') }}
            {{ Form::text('hora_saida', null, array('class'=>'mask-time form-control', 'id'=>'hora_saida', 'size' => '2')) }}
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('total', 'Total:') }}
            <div id='somahoras' class='somahoras'>00:00</div>
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('faturada', 'Trabalhadas:') }}
            {{ Form::text('hora_faturada', null, array('class'=>'mask-time form-control', 'id'=>'hora_faturada', 'size' => '2')) }}
        </div>
</div>   

<div id='divcontratada' class='divcontratada'>   
        <div class="form-group col-md-1">
            {{ Form::label('hora_contratada', 'Contratadas:') }}
            {{ Form::select('hora_contratada', $contratadas, '8', array('style' => 'width: 50%'))  }}
        </div>
</div> 
        
        <div class="form-group col-md-2" id='tempo_exposicao'>
            {{ Form::label('exposicao', 'Tempo de fonte exposta:') }}
            {{ Form::text('tempo_exposicao', null, array('class'=>'mask-time2 form-control', 'size' => '6')) }}
        </div>

<div class="clear"><br></div>

<div class="form-group col-md-12">
    {{ Form::text('observacoes', null, array('class'=>'form-control', 'style' => 'width:100%', 'placeholder' => 'Observações')) }}
</div>

</fieldset>
</div>

        <div class="form-group col-md-5">
        {{ Form::submit('Salvar marcação', array('class' => 'btn btn-success')) }}
        {{ Form::close() }}
        </div>
        <div class="clear"><br></div>

@if ($errors->any())
    <div class="alert-group">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ implode('', $errors->all('<p>:message</p>')) }}
        </div>
    </div>
@endif

@else
    @include('accessdenied')
@endif
    
@stop
