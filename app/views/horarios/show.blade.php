@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('pendencias_menu'))

<h1>Exibir marcações</h1>
<br>

{{ link_to_route('horarios.inconsistencia', 'Voltar para Inconsistências',
				  array('filter_funcionario='.$horario->funcionario->cod), 
				  array('class' => 'btn btn-info') ) }} 

<div class="clear"><br></div>

<div class="table-responsive panel panel-default">
    <table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Funcionário</th>			
			<th>Data</th>
			<th>Item Contabil</th>
			<th>Hora entrada</th>				
			<th>Intervalo inicio</th>
			<th>Intervalo fim</th>
			<th>Hora saida</th>
			<th>Horas Totais</th>
			<th>Horas Extra</th>
			<th>Horas Noturnas</th>
			<th>Horas Faturadas</th>
			<th>Horas Contratadas</th>
			<th>Tempo Fonte Exposta</th>
			<th>Atividade</th>
			<th colspan ="2">Ações</th>
		</tr>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $horario->funcionario->nome }}}</td>					
			<td>{{{ $horario->data->format('d/m/Y') }}}</td>
			<td>{{{ $horario->item_contabil->CTD_DESC01 }}}</td>
			<td>{{{ $horario->hora_entrada->format('H:i') }}}</td>			
			<td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
			<td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>
			<td>{{{ $horario->hora_saida->format('H:i') }}}</td>
			<td>{{{ $horario->getWorkedHours()->format('H:i') }}}</td>
			<td>{{{ $horario->getExtraHours()->format('H:i') }}}</td>
			<td>{{{ $horario->getNightHours()->format('H:i') }}}</td>
			<td>{{{ $horario->hora_faturada->format('H:i') }}}</td>
			<td>{{{ $horario->hora_contratada.'hs' }}}</td>
			<td>{{{ $horario->tempo_exposicao }}}</td>
			<td>{{{ $horario->atividade }}}</td>
			<td>{{ link_to_route('horarios.edit', 'Editar', array($horario->cod), array('class' => 'btn btn-info')) }}</td>
			<td>
				{{ Form::open(array('method' => 'DELETE', 'route' => array('horarios.destroy', $horario->cod))) }}
				{{ Form::submit('Deletar', array('class' => 'btn btn-danger align justify')) }}
				{{ Form::close() }}
			</td>
		</tr>
	</tbody>
</table>
</div>

	@else
        @include('accessdenied')
    @endif
    
@stop
