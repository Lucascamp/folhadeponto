@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('visualizar_menu'))

<h1>Todas as Marcações</h1>

<br>

<p>	
	@if(User::checkPermission('visualizar_cadastrar'))
		{{ link_to_route('horarios.create', 'Adicionar Novo', array(), array('class'=> 'btn btn-info')) }}
	@endif
</p>

<br>

<div class="clear"></div>


{{ $table->render('datatable.template') }}



@stop

@section('footer_scripts')


{{ $table->script('datatable.javascript') }}

	@else
        @include('accessdenied')
    @endif
@stop