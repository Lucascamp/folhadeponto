@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('pendencias_menu'))

<h1>Inconsistências de marcações</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'horarios.inconsistencia', 'method' => 'get')) }}

<div class="row">

	<div class="col-md-3">
		<div class="form-group">
			{{ Form::select('filter_funcionario', $filter_funcionarios, $funcionario) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::submit('Filtrar', array('class' => 'btn btn-success')) }}
		</div>
	</div>
</div>
{{ Form::close()}}

{{ Form::open(array('route' => array('horarios.inconsistencia'), 'method' => 'post')) }}

<div class="clear"></div>

{{ $horarios->appends(array('filter_funcionario' => $funcionario))->links() }}

<div class="table-responsive panel panel-default">
    <table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Funcionário</th>
			<th>Unidade</th>			
			<th>Data</th>
			<th>Hora entrada</th>				
			<th>Intervalo inicio</th>
			<th>Intervalo fim</th>
			<th>Hora saida</th>
			<th>Atividade</th>
			<th>Motivo 1</th>
			<th>Motivo 2</th>
			<th>Ação Corretiva</th>
		</tr>
	</thead>
	<tbody>

		@if ($horarios->count())

		@foreach ($horarios as $horario)

		<tr>
			<td>{{{ $horario->funcionario->nome }}}</td>
			<td>{{{ $horario->unidade }}}</td>			
			<td>{{{ $horario->data->format('d/m/Y') }}}</td>
			<td>{{{ $horario->hora_entrada->format('H:i') }}}</td>				
			<td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
			<td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>
			<td>{{{ $horario->hora_saida->format('H:i') }}}</td>
			<td>{{{ $horario->atividade }}}</td>
			<td>{{{ $horario->motivo }}}</td>
			<td>{{{ $horario->motivo2 }}}</td>

			@if($horario->motivo != '' or $horario->motivo2 != '')
				<td>
					{{ link_to_route('horarios.edit', 'Editar Marcação', array($horario->cod), array('class' => 'btn btn-info')) }}
				</td>
			@elseif($horario->valido == 0)
				<td> 
					@if(User::checkPermission('validar_menu'))
						{{ link_to_route('horarios.validar', 'Ir para Validação', array('filter_funcionario='.$horario->funcionario->cod.'&filter_item_contab=0&filter_dia=0'), array('class' => 'btn btn-info')) }}
					@else
						Ponto necessita ser validado pelo superior.
					@endif
				</td>
			@endif		
		</tr>

			@endforeach
		
			@else
			<tr> 
				<td colspan="14" class="center">
					Nenhum horario a ser validado.
				</td>
			</tr>
			@endif

   
		</tbody>
	</table>
</div>	
	<div class="clear"><br></div>
	
	{{ $horarios->appends(array('filter_funcionario' => $funcionario))->links() }}
	
	{{ Form::close() }}

	@else
        @include('accessdenied')
    @endif

	@stop
