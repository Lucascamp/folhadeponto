@extends('layouts.scaffold')

<?php use Carbon\Carbon; ?>

@section('main')

@if(User::checkPermission('visualizar_cadastrar'))

<h1>Marcação de funcionários por unidade</h1>

<div class="clear"><br></div>

{{ Form::open(array('', 'class'=>'form-inline')) }}

{{ Form::hidden('created_by',  Auth::user()->cod, array('id'=>'created_by')); }}

    <div class="form-group col-md-1">
        {{ Form::label('labelunidade', 'Unidade:') }}
        {{ Form::select('unidade', $unidades, null, array('class' => 'unidadeFuncionarios chosen-select','id'=>'unidade')) }}
    </div>

    <div class="form-group col-md-2">
        {{ Form::label('data', 'Data:') }}
        {{ Form::text('data', Carbon::now()->format('d/m/Y'), array('class'=>'form-control datepicker', 'size' => '9', 'id'=>'data')) }}
    </div>

    <div class="form-group col-md-1">
    </div>

    <div class="form-group col-md-6" id="beneficios-inputs"> 
        Legenda Benefícios:</br>
        <label>Alimentação - VA</label>
        <label>Vale Transporte - VT</label>
        <label>Auxílio Moradia - AM</label>  
        
    </div>

<div class="clear"><br><br></div>  

    <table class="table table-bordered table-hover table-striped">
        <thead>
            <tr class="thmultifun">
                <th>Funcionario</th>
                <th>Entrada</th>
                <th>Intervalo</th>
                <th>Intervalo</th>
                <th>Saida</th>
                <th>Total</th>
                <th>Trabalhada</th>
                <th>Contratada</th>
                <th>Fonte Exposta</th>
                <th>Atividade</th>
                <th>Centro de Custo</th>
                <th>Obs.</th>
                <th width="150px">Benefícios</th>
            </tr>
        </thead>
        <tbody id="tbodyfuncionarios">

        </tbody>
    </table>
    
</div>

        <div class="form-group col-md-2">
            <div id="bt_salvar" class="btn btn-success">Salvar Marcações</div>
        </div>

        <div class="form-group col-md-5">
            <div id="flashSuccess" class="alert alert-success flashSuccess"></div>
        </div>

        <div class="form-group col-md-5">
            <div id="flashError" class="alert alert-danger flashError"></div>
        </div>

        <div class="clear"><br></div>

        <div class="espaco" id='espaco'></div>

@if ($errors->any())
    <div class="alert-group">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close"  data-dismiss="alert" aria-hidden="true">×</button>
            {{ implode('', $errors->all('<p>:message</p>')) }}
        </div>
    </div>
@endif

@else
    @include('accessdenied')
@endif
    
@stop
