@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('pendencias_menu'))

<h1>Editar Marcação</h1>

<div class="clear"><br></div> 

{{ Form::model($horario, array('method' => 'PATCH', 'route' => array('horarios.update', $horario->cod), 'class'=>'form-inline')) }}

{{ Form::hidden('cod_funcionario', $horario->cod_funcionario)  }}
{{ Form::hidden('cod_item_contab', $horario->cod_item_contab)  }}
{{ Form::hidden('data', $horario->data->format('d/m/Y'))  }}

{{ Form::hidden('updated_by', Auth::user()->cod)  }}
    
    <div class="form-group col-md-4">
        {{ Form::label('funcionario', 'Funcionario:') }}
        {{ Form::text('funcionario', $horario->funcionario->nome, array('disabled'=>'disabled')) }}            
    </div>

    <?php $beneficios = explode(',', $horario->beneficios); ?>

    <div class="form-group" id="beneficios-inputs col-md-2"> 
        {{ Form::label('beneficios', 'Benefícios') }}
        {{ Form::checkbox('beneficio_tipo_va', 'va', Horario::checkBeneficiosEdit('va', $beneficios), array('class'=>'beneficio','id'=>'beneficio_tipo_va'))}} Alimentação <br> 

        {{ Form::checkbox('beneficio_tipo_vt', 'vt', Horario::checkBeneficiosEdit('vt', $beneficios), array('class'=>'beneficio','id'=>'beneficio_tipo_vt'))}} Vale Transporte <br>
        {{ Form::checkbox('beneficio_tipo_am', 'am', Horario::checkBeneficiosEdit('am', $beneficios), array('class'=>'beneficio','id'=>'beneficio_tipo_am'))}} Auxilio Moradia <br>
        {{ Form::hidden('beneficios', null, array('class'=>'form-control')) }}
    </div>


    @if($horario->motivo != '')
    
        <div class="form-group col-md-2">
            {{ Form::label('motivolabel', 'Motivo 1:') }}
            {{ Form::label('',$horario->motivo) }}
        </div>

        <div class="form-group col-md-2">
            {{ Form::label('motivolabel', 'Motivo 2:') }}
            {{ Form::label('',$horario->motivo2) }}
        </div>

    @endif

<div class="clear"><br></div>  

<fieldset>
<legend><b>Cliente:</b></legend> 

    <div class="form-group col-md-1">
        {{ Form::label('labelunidade', 'Unidade:') }}
        {{ Form::select('unidade', $unidades, $horario->unidade, array('class' => 'unidade chosen-select', 'id' => 'unidade')) }}
    </div>

    <div class="form-group col-md-2">
        {{ Form::label('datalbl', 'Data:') }}
        {{ Form::text('datashow',$horario->data->format('d/m/Y'), array('disabled'=>'disabled', 'class'=>'form-control datepicker mask-date', 'size' => '9')) }}
    </div>
    
    <div class="form-group col-md-3">
        {{ Form::label('projeto', 'Centro de custo:') }}
        {{ Form::text('projeto', $horario->item_contabil->CTD_DESC01, array('disabled'=>'disabled', 'style' => 'width:300px')) }}       
    </div>

     <div class="form-group col-md-2">
        {{ Form::label('lblatividade', 'Atividade:') }}
        {{ Form::select('atividade', $atividades, 
            $horario->atividade, array('style' => 'width:250px', 'class' => 'atividade chosen-select', 'id' => 'atividade')) }}
    </div>

    <div class="clear"><br></div>
    <div id='divhorario' class='divhorario'>
        <div class="form-group col-md-1">
            {{ Form::label('hora_entrada', 'Entrada:') }}
            {{ Form::text('hora_entrada', $horario->hora_entrada->format('H:i'), array('class'=>'mask-time form-control', 'size' => '2')) }}
        </div>        

        <div class="form-group col-md-1">
            {{ Form::label('intervalo_inicio', 'Intervalo:') }}
            {{ Form::text('intervalo_inicio',$horario->intervalo_inicio->format('H:i'), array('class'=>'mask-time form-control', 'size' => '2')) }}
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('intervalo_fim', 'Intervalo:') }}
            {{ Form::text('intervalo_fim',$horario->intervalo_fim->format('H:i'), array('class'=>'mask-time form-control', 'size' => '2')) }}
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('hora_saida', 'Saída:') }}
            {{ Form::text('hora_saida',$horario->hora_saida->format('H:i'), array('class'=>'mask-time form-control', 'size' => '2')) }}
        </div>

        <div class="form-group col-md-1">
            {{ Form::label('total', 'Total:') }}
            <div id='somahoras' class='somahoras'>{{{ $horario->getWorkedHours()->format('H:i') }}}</div>
        </div>

        <div class="form-group col-md-2">
        {{ Form::label('faturada', 'Horas Faturadas:') }}
        {{ Form::text('hora_faturada', $horario->hora_faturada->format('H:i'), array('class'=>'mask-time form-control', 'id'=>'hora_faturada', 'size'=>'2')) }}
        </div>

        <div class="form-group col-md-2">
            {{ Form::label('hora_contratada', 'Horas contratadas:') }}
            {{ Form::select('hora_contratada', $contratadas, $horario->hora_contratada)  }}
        </div>

        <div class="form-group col-md-2">
            {{ Form::label('exposicao', 'Tempo de fonte exposta:') }}
            {{ Form::text('tempo_exposicao', null, array('class'=>'mask-time2 form-control', 'size' => '6')) }}
        </div>

        <div class="clear"><br></div>

        <div class="form-group col-md-12">
            {{ Form::text('observacoes', $horario->observacoes, array('class'=>'form-control', 'size' => '200', 'placeholder' => 'Observações')) }}
        </div>
</div>
</fieldset>
    <div class="clear"><br></div>
        
    {{ Form::submit('Atualizar', array('class' => 'btn btn-success')) }}
    {{ link_to_route('horarios.show', 'Cancelar', $horario->cod, array('class' => 'btn btn-danger')) }}

    {{ Form::close() }}

    <div class="clear"><br></div>
    
@if ($errors->any())
    <div class="alert-group">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ implode('', $errors->all('<p>:message</p>')) }}
        </div>
    </div>
@endif

    @else
        @include('accessdenied')
    @endif

  @stop
