@extends('layouts.scaffold')

@section('main')

<h1>Marcações</h1>

<br>

<p>
	{{ link_to_route('horarios.create', 'Adicionar Marcação', array(), array('class'=> 'btn btn-info')) }}
</p>
<div class="clear"></div>
    
<div class="row top-buffer">
{{ Form::open(array('route' => 'horarios.index', 'method' => 'get')) }}
    <div class="col-md-3">
        <div class="form-group">
            {{ Form::select('filter_funcionario', $filter_funcionarios, $funcionario) }}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            {{ Form::select('filter_item_contab', $filter_itens_contabil , $item_contabil) }}
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            {{ Form::select('filter_dia', $filter_dia , $dia) }}
        </div>
    </div>
        <div class="form-group">
            {{ Form::submit('Filtrar', array('class' => 'btn btn-success')) }}
        </div>
</div>

{{ Form::close()}}

<div class="row">
    {{ $horarios->links() }}
</div>

<div class="table-responsive panel panel-default">
    <table class="table table-striped table-bordered">
       <thead>
          <tr>
            <th>Funcionário</th>
            <th>Data</th>
            <th>Item Contabil</th>
            <th>Horas Entrada</th>               
            <th>Intervalo inicio</th>
            <th>Intervalo fim</th>
            <th>Horas Saida</th>
            <th>Horas Totais</th>
            <th>Horas Extras</th>
            <th>Horas Noturnas</th>
            <th>Exposição</th>
            <th>Horas Faturadas</th>
            <th>Horas Contratadas</th>
            <th>Atividade</th>
            <th colspan ="2">Ações</th>
         </tr>
     </thead>
     <tbody>

    @if ($horarios->count())

        @foreach ($horarios as $horario)
            <tr>
            <td>{{{ $horario->funcionario->nome }}}</td>           
            <td>{{{ $horario->data->format('d/m/Y') }}}</td>
            <td>{{{ $horario->item_contabil->CTD_DESC01 }}}</td>
            <td>{{{ $horario->hora_entrada->format('H:i') }}}</td>              
            <td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
            <td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>
            <td>{{{ $horario->hora_saida->format('H:i') }}}</td>
            <td>{{{ $horario->getWorkedHours()->format('H:i') }}}</td>
            <td>{{{ $horario->getExtraHours()->format('H:i') }}}</td>
            <td>{{{ $horario->getNightHours()->format('H:i') }}}</td>
            <td>{{{ $horario->exposedHours() }}}</td>
            <td>{{{ $horario->hora_faturada.'hs' }}}</td>
            <td>{{{ $horario->hora_contratada.'hs' }}}</td>
            <td>{{{ $horario->atividade }}}</td>
            <td>{{ link_to_route('horarios.edit', 'Editar', array($horario->cod), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('horarios.destroy', $horario->cod))) }}
                {{ Form::submit('Deletar', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
                
    @else
        <tr> 
            <td colspan="14" class="center">
                Nenhum horario encontrado.
            </td>
        </tr>
    @endif
        </tbody>
    </table>
</div>

{{ Form::close()}}

<div class="clear"></div>

{{ $horarios->links() }}


@stop
