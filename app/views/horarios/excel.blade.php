@if ($horarios->count())
	<table class="table table-striped table-bordered">		
			<tr>
				<th>Funcionario</th>
				<th>Localizacao</th>
				<th>Data</th>
				<th>Item Contabil</th>
				<th>Hora entrada</th>
				<th>Hora saida</th>
				<th>Intervalo inicio</th>
				<th>Intervalo fim</th>
				<th>Hora Total</th>
				<th>Hora Extra</th>
				<th>Horas Noturnas</th>
				<th>Hora faturada</th>
				<th>Hora contratada</th>
				<th>Atividade</th>
			</tr>				
		<tbody>
			@foreach ($horarios as $horario)
				<tr>
					<td>{{{ $horario->funcionario->nome }}}</td>
					<td>{{{ "MG" }}}</td>
					<td>{{{ $horario->data->format('d/m/Y') }}}</td>
					<td>{{{ $horario->item_contabil->CTD_DESC01 }}}</td>
					<td>{{{ $horario->hora_entrada->format('H:i') }}}</td>
					<td>{{{ $horario->hora_saida->format('H:i') }}}</td>
					<td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
					<td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>
					<td>{{{ $horario->getWorkedHours()->format('H:i') }}}</td>
					<td>{{{ $horario->getExtraHours()->format('H:i') }}}</td>
					<td>{{{ $horario->getNightHours()->format('H:i') }}}</td>
					<td>{{{ $horario->hora_faturada.'hs'}}}</td>
					<td>{{{ $horario->hora_contratada.'hs' }}}</td>
					<td>{{{ $horario->atividade }}}</td>                    
				</tr>
				
			@endforeach
		</tbody>
	</table>
@else
	There are no horarios
@endif
