@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('visualizar_criarperiodo'))

<h1>Criar várias marcações</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'horarios.salvarvarios', 'class'=>'form-inline')) }}

{{ Form::hidden('created_by',  Auth::user()->cod); }}

    <div class="form-group col-md-4">
        {{ Form::label('cod_funcionario', 'Funcionário:') }}
        {{ Form::select('cod_funcionario', $funcionarios) }}
    </div>

    <div class="form-group" id="beneficios-inputs col-md-2"> 
        {{ Form::label('beneficios', 'Selecione os Benefícios:') }}
        {{ Form::checkbox('beneficio_tipo_va', 'va', false, array('class'=>'beneficio','id'=>'beneficio_tipo_va'))}}<label for="beneficio_tipo_va"> Alimentação</label>
        {{ Form::checkbox('beneficio_tipo_vt', 'vt', false, array('class'=>'beneficio','id'=>'beneficio_tipo_vt'))}}<label for="beneficio_tipo_vt">Vale Transporte</label>
        {{ Form::checkbox('beneficio_tipo_am', 'am', false, array('class'=>'beneficio','id'=>'beneficio_tipo_am'))}}<label for="beneficio_tipo_am">Auxílio Moradia</label>  
        {{ Form::hidden('beneficios', null, array('class'=>'form-control')) }}
    </div>
       
<div class="clear"><br></div>  

<fieldset>
<legend><b>Informações da marcações</b></legend> 
    
    <div class="clear"><br></div>

    <div class="form-group col-md-1">
        {{ Form::label('labelunidade', 'Unidade:') }}
        {{ Form::select('unidade', $unidades, Auth::user()->unidade, array('class' => 'unidade chosen-select', 'id' => 'unidade')) }}
    </div>
    
    <div class="form-group col-md-2">
        {{ Form::label('datainicio', 'Data Inicial:') }}
        {{ Form::text('data', null, array('class'=>'form-control datepicker', 'size' => '9')) }}
    </div>

    <div class="form-group col-md-2">
        {{ Form::label('datafinal', 'Data Final:') }}
        {{ Form::text('datafinal', null, array('class'=>'form-control datepicker', 'size' => '9')) }}
    </div>

    <div class="form-group col-md-2">
        {{ Form::label('lblatividade', 'Atividade:') }}
        {{ Form::select('atividade', array(
            'Férias' => 'Férias',
            'Folga' => 'Folga',
            'Afastado' => 'Afastado',
            'Atestado' => 'Atestado',
            'Demitido' => 'Demitido'), 
            'Férias', array('class' => 'atividade chosen-select', 'id' => 'atividade')) }}
    </div>

        <div class="form-group col-md-3">
            {{ Form::label('cod_item_contab', 'Centro de Custo:') }}
            {{ Form::select('cod_item_contab', $itens_contabil, null ,array('style' => 'width:300px', 'class' => 'contab chosen-select', 'id' => 'contab')) }}      
        </div>

<div class="clear"><br></div>
    
        {{ Form::hidden('hora_entrada', '00:00') }}
        {{ Form::hidden('intervalo_inicio', '00:00') }}
        {{ Form::hidden('intervalo_fim', '00:00') }}
        {{ Form::hidden('hora_saida', '00:00') }}
        {{ Form::hidden('hora_faturada', '00:00') }}
        {{ Form::hidden('hora_contratada', '8') }}

</fieldset>
</div>
    
        <div class="clear"><br></div>
        <div class="form-group col-md-5">
        {{ Form::submit('Salvar marcações', array('class' => 'btn btn-success')) }}
        {{ link_to_route('horarios.index', 'Tabela de marcações', array(), array('class'=> 'btn btn-info')) }}
        {{ Form::close() }}
        </div>
        <div class="clear"><br></div>

@if ($errors->any())
    <div class="alert-group">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ implode('', $errors->all('<p>:message</p>')) }}
        </div>
    </div>
@endif

@else
    @include('accessdenied')
@endif
    
@stop
