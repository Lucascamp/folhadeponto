@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<div class="area_impressao" name="area_impressao" id="area_impressao">

<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='50%' align='right'><center><b><font size="5" face="Arial">Marcações por dia no Período</font></b></center>
		<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right></td>
	</tr>
</table>

	<div class="clear"><br></div>

	<div width='100%'>	
		<div width='50%' align="left">
			<table width='20%' class="zebra3" align="left">
			    <thead>
			        <tr>
			        	<th width="10%">Data</th>
			            <th width='10%'>Somatório</th>
			        </tr>
			    </thead>
			    <?php 
			    	$i = 0; 
			    	$count= count($horarios)-1;
			   	?>
			    <tbody>
			  		@foreach (range(1, $periodo) as $index)
				  		@if($datainicio->format('d/m/Y') == $horarios[$i]->data)
				  			<tr>
								<td>{{ $horarios[$i]->data }}</td>
								<td>{{ $horarios[$i]->somatorio }}</td>
							</tr>
							@if($i<$count)
								<?php $i++; ?>
							@endif
						@else
							<tr>
								<td>{{ $datainicio->format('d/m/Y') }}</td>
								<td>-</td>
							</tr>
						@endif
						<?php $datainicio->addDay(); ?>
					@endforeach
				</tbody>
			</table>
		</div>
		<div width='50%' align="center">		
			<canvas id="daily-reports" width="900" height="500"></canvas>		
		</div>
	</div>

</div>	

{{ HTML::script('js/chart/chart.min.js'); }}

<script>
	(function(){
		var ctx = document.getElementById('daily-reports').getContext('2d');
		var chart = {
			labels: {{ $datasJson }},
			datasets: [{
				data: {{ $somatorioJson }},
				fillColor : '#0DA31F',
				strokeColor : '#000000',
				pointColor : '#150EE3',
			}]	
		};
		new Chart(ctx).Line(chart, { bezierCurve:false });
	})();
</script>

<div class="clear"><br></div>
<div class="clear"><br></div>

	@else
        @include('accessdenied')
    @endif
    
@stop