@extends('layouts.scaffold2')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<h1>Marcações/dia no Período</h1>

<div class="clear"><br></div>

<!-- // {{ Form::open(array('route' => 'relatorios.marcacoes2')) }} -->

<div class="row">
<div class="col-md-12">
<div class="clear"><br></div>
    <div class="col-md-3">
        <div class="form-group">        
            {{ Form::label('UN', 'Unidades:') }}
            {{ Form::select('UN', $uns,$attributes = array('id' => 'UN')) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">    
            {{ Form::label('Periodos', 'Periodo:') }}
            {{ Form::select('Periodos', $periodos, $attributes = array('id' => 'Periodos')) }}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">    
            {{ Form::label('labeldataini', 'Data inicial:') }}
            <input type="text" class="datepicker" id="dataini" name="dataini"/>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">    
            {{ Form::label('labeldatafim', 'Data final:') }}
            <input type="text" class="datepicker" id="datafim" name="datafim"/>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group"> 
        <div class="clear"><br></div>   
            <input type="button" name="datafim" class="btn btn-info" id="bt-gerarrelatorio" value="Gerar Relatório"></button>
        </div>
    </div>
    
        <div class="clear"><br></div>

         <!-- // {{ Form::submit('Gerar Relatório', array('class' => 'btn btn-info mensagem_aguarde', 'id' => 'mensagem_aguarde')) }} -->
            
    </div>
</div>   
<!-- {{ Form::close() }} -->
<div id="relatorio"></div>

<div id="screen"></div>
<div id="dvLoading"></div>
<div id="dvMessage"><center><font size='6' color='white'>Gerando Relatório</font></center></div>

</div>  

    @if ($errors->any())
    <ul>
      {{ implode('', $errors->all('<li class="error">:message</li>')) }}
  </ul>
  @endif

    @else
        @include('accessdenied')
    @endif
    
  @stop


