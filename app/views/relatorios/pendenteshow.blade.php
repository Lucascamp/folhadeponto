@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<div class="area_impressao" name="area_impressao" id="area_impressao">

<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='50%' align='right'><center><b><font size="5" face="Arial">Pendências do Período</font></b></center>
		<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right></td>
	</tr>
</table>
	<div class="clear"><br></div>
	
		<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
        	<th width="10%">codigo</th>
            <th width='45%'>Funcionários</th>
            <th width='45%'>Pendências</th>
        </tr>
    </thead>
    
    <?php $i=0; ?>

    <tbody>
  		@foreach ($funcionarios as $funcionario)
  		<tr>
  			 
			@if($funcionario->cod == $horarios[$i]->cod_funcionario)
				<td>{{ $funcionario->cod }}</td>
				<td>{{ $funcionario->nome }}</td>
				<td>{{ ($daysdiff-intval($horarios[$i]->soma)) }}</td>

				<?php 
				if($i < (count($horarios)-1))
						 $i++;	
				?>

			@else
				<td>{{ $funcionario->cod }}</td>
				<td>{{ $funcionario->nome }}</td>
				<td>{{ $daysdiff }}</td>
			@endif

		

		</tr>
		@endforeach

	</tbody>
</table>

</div>			

	@else
        @include('accessdenied')
    @endif
    
@stop