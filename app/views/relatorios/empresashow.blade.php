@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<div class="area_impressao" name="area_impressao" id="area_impressao">

<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='30%'>
			<left><img border="0" src="../images/img_logo_ponto_qt.png" width="150" height="45"></left>
		</td>
		<td width='40%'>	
			<center><b><font size="5" face="Arial">Somatório geral do Período</font></b></center>
		</td>
		<td width='30%' align='right'>
	 		<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right>
	 	</td>
	</tr>
</table>
	<div class="clear"><br></div>
	
		<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
            <th width='15%'>Funcionário</th>
            <th width='5%'>Matrícula Qualitec</th>
            <th width='5%'>Hora Normal</th>             
            <th width='5%'>Hora Extra</th>
            <th width='8%'>Adicional Noturno</th>
            <th width='5%'>Horas Faturadas</th>
            <th width='5%'>Faltas</th>
            <th width='5%'>Folgas</th>
            <th width='5%'>Atestado</th>
            <th width='5%'>A disposição do cliente</th>
            <th width='5%'>A disposição</th>
            <th width='5%'>VA/VR</th>
            <th width='5%'>VT</th>
            <th width='5%'>AM</th>
        </tr>
    </thead>
    
    <tbody>
  	@foreach ($funcionarios as $funcionario)
  	<tr>
  	
  	@if($funcionario->matricula_qt!=null)

		<td>{{ $funcionario->nome }}</td>
		
		<td>{{ $funcionario->matricula_qt }}</td>
		
		<?php $fields = array('HNqt', 'HEqt', 'ADNqt', 'HFqt', 'Falta', 'Folga', 'Atestado', 'ADC', 'AD', 'va', 'vt', 'am'); ?>
		@foreach ($fields as $field)

			@if($field=='ADC')
				<?php $field='A Disposição do cliente'; ?>
				@if(array_key_exists($field.$funcionario->cod, $ADC))
						<td>{{{ Horario::addFieldTotal('total_qt'.$field, $ADC[$field.$funcionario->cod]) }}}</td>
				@else
						<td>-</td>
				@endif

			@elseif($field=='AD')
				<?php $field='A Disposição'; ?>
				@if(array_key_exists($field.$funcionario->cod, $AD))
					<td>{{{ Horario::addFieldTotal('total_qt'.$field, $AD[$field.$funcionario->cod]) }}}</td>
				@else
					<td>-</td>
				@endif

			@elseif(isset(${$field}[$funcionario->matricula_qt]))

				@if($field=='ADNqt')
					<td>{{{ number_format(Horario::addFieldTotal('total_qt'.$field, ${$field}[$funcionario->matricula_qt]), 2) }}}</td>

				@elseif($field=='HNqt' || $field=='HEqt' || $field=='HFqt')
					<td>{{{ number_format(Horario::addFieldTotal('total_qt'.$field, ${$field}[$funcionario->matricula_qt]), 2) }}}</td>
				@endif

			@elseif(array_key_exists($field.$funcionario->cod, ${$field}))
				<td>{{{  Horario::addFieldTotal('total_'.$field, ${$field}[$field.$funcionario->cod]) }}}</td>
			
			@else
				<td>-</td>
			@endif
           

		@endforeach
		
		</tr>
	@endif
	@endforeach

	</tbody>
</table>
	
	<table border="2" width="100%" bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td rowspan="2" align=center width="4%">Totais</td>
		<td width="8%" align=center>Hora Direta</td>
		<td width="8%" align=center>Hora Extra</td>
		<td width="8%" align=center>Adicional Noturno</td>
		<td width="8%" align=center>Horas Faturadas</td>
		<td width="8%" align=center>Falta</td>
		<td width="8%" align=center>Folga</td>
		<td width="8%" align=center>Atestado</td>
		<td width="8%" align=center>A Disposição do cliente</td>
        <td width="8%" align=center>A Disposição</td>
		<td width="8%" align=center>VA/VR</td>
		<td width="8%" align=center>VT</td>
		<td width="8%" align=center>Moradia</td>
	</tr>
	<tr>
		@foreach ($fields as $field)
			
			@if($field=='ADC')
				<?php $field='A Disposição do cliente'; ?>
			@elseif($field=='AD')
				<?php $field='A Disposição'; ?>
			@endif

			@if($field=='HNqt' || $field=='HEqt' || $field=='ADNqt' || $field=='HFqt' )
				<td width="8%" align=center><br>{{ number_format(Horario::getFieldTotal('total_qt'.$field), 2,',', '.') }}</td>
			@else
				<td width="8%" align=center><br>{{ Horario::getFieldTotal('total_qt'.$field) }}</td>
			@endif
		@endforeach
		
	</tr>
</table>

<div class="page-break"></div>

<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='30%'>
			<left><img border="0" src="../images/img_logo_ponto_a5.png" width="150" height="45"></left>
		</td>
		<td width='40%'>	
			<center><b><font size="5" face="Arial">Somatório geral do Período</font></b></center>
		</td>
		<td width='30%' align='right'>
	 		<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right>
	 	</td>
	</tr>
</table>

	<div class="clear"><br></div>
	
		<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
            <th width='15%'>Funcionário</th>
            <th width='5%'>Matrícula Assinco</th>
            <th width='5%'>Hora Normal</th>             
            <th width='5%'>Hora Extra</th>
            <th width='8%'>Adicional Noturno</th>
            <th width='5%'>Horas Faturadas</th>
            <th width='5%'>Faltas</th>
            <th width='5%'>Folgas</th>
            <th width='5%'>Atestado</th>
            <th width='5%'>A disposição do cliente</th>
            <th width='5%'>A disposição</th>
            <th width='5%'>VA/VR</th>
            <th width='5%'>VT</th>
            <th width='5%'>AM</th>
        </tr>
    </thead>
    
    <tbody>
  	@foreach ($funcionarios as $funcionario)
  	<tr>
  	@if($funcionario->matricula_a5!=null)
  	
		<td>{{ $funcionario->nome }}</td>
		
		<td>{{ $funcionario->matricula_a5 }}</td>

		<?php $fields = array('HNa5', 'HEa5', 'ADNa5', 'HFa5', 'Falta', 'Folga', 'Atestado', 'ADC', 'AD', 'va', 'vt', 'am'); ?>
		@foreach ($fields as $field)
			
			@if($field=='ADC')
				<?php $field='A Disposição do cliente'; ?>
				@if(array_key_exists($field.$funcionario->cod, $ADC))
						<td>{{{ Horario::addFieldTotal('total_a5'.$field, $ADC[$field.$funcionario->cod]) }}}</td>
				@else
						<td>-</td>
				@endif

			@elseif($field=='AD')
				<?php $field='A Disposição'; ?>
				@if(array_key_exists($field.$funcionario->cod, $AD))
					<td>{{{ Horario::addFieldTotal('total_a5'.$field, $AD[$field.$funcionario->cod]) }}}</td>
				@else
					<td>-</td>
				@endif

			@elseif(isset(${$field}[$funcionario->matricula_a5]))

				@if($field=='ADNa5')
					<td>{{{ number_format(Horario::addFieldTotal('total_a5'.$field, ${$field}[$funcionario->matricula_a5]), 2) }}}</td>

				@elseif($field=='HNa5' || $field=='HEa5' || $field=='HFa5')
					<td>{{{ number_format(Horario::addFieldTotal('total_a5'.$field, ${$field}[$funcionario->matricula_a5]), 2) }}}</td>
				@endif

			@elseif(array_key_exists($field.$funcionario->cod, ${$field}))
				<td>{{{  Horario::addFieldTotal('total_a5'.$field, ${$field}[$field.$funcionario->cod]) }}}</td>
			
			@else
				<td>-</td>
			@endif

		@endforeach
	
		</tr>
	@endif	
	@endforeach

	</tbody>
</table>
	
	<table border="2" width="100%" bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td rowspan="2" align=center width="4%">Totais</td>
		<td width="8%" align=center>Hora Direta</td>
		<td width="8%" align=center>Hora Extra</td>
		<td width="8%" align=center>Adicional Noturno</td>
		<td width="8%" align=center>Horas Faturadas</td>
		<td width="8%" align=center>Falta</td>
		<td width="8%" align=center>Folga</td>
		<td width="8%" align=center>Atestado</td>
		<td width="8%" align=center>A Disposição do cliente</td>
        <td width="8%" align=center>A Disposição</td>
		<td width="8%" align=center>VA/VR</td>
		<td width="8%" align=center>VT</td>
		<td width="8%" align=center>Moradia</td>
	</tr>
	<tr>
		@foreach ($fields as $field)
			
			@if($field=='ADC')
				<?php $field='A Disposição do cliente'; ?>
			@elseif($field=='AD')
				<?php $field='A Disposição'; ?>
			@endif

			@if($field=='HNa5' || $field=='HEa5' || $field=='ADNa5' || $field=='HFa5' )
				<td width="8%" align=center><br>{{ number_format(Horario::getFieldTotal('total_a5'.$field), 2,',', '.') }}</td>
			@else  
				<td width="8%" align=center><br>{{ Horario::getFieldTotal('total_a5'.$field) }}</td>
			@endif
		@endforeach
		
	</tr>
</table>	

</div>			

	@else
        @include('accessdenied')
    @endif
    
@stop