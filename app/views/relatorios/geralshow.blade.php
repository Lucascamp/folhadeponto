@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

	<div class="area_impressao" name="area_impressao" id="area_impressao">
		<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='50%' align='right'><center><b><font size="5" face="Arial">Somatório geral do Período</font></b></center>
		<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right></td>
	</tr>
</table>
	<div class="clear"><br></div>
	
<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
            <th width='15%'>Funcionário</th>
            <th width='5%'>Mat. Qualitec</th>
            <th width='5%'>Mat. Assinco</th>
            <th width='5%'>Hora Normal</th>             
            <th width='5%'>Hora Extra</th>
            <th width='8%'>Adicional Noturno</th>
            <th width='5%'>Horas Faturadas</th>
            <th width='5%'>Faltas</th>
            <th width='5%'>Folgas</th>
            <th width='5%'>Atestado</th>
            <th width='5%'>A disposição do cliente</th>
            <th width='5%'>A disposição</th>
            <th width='5%'>VA/VR</th>
            <th width='5%'>VT</th>
            <th width='5%'>AM</th>
        </tr>
    </thead>

    <tbody>
  	@foreach ($funcionarios as $funcionario)
  	<tr>
		<td>{{ $funcionario->nome }}</td>
		
		@if($funcionario->matricula_qt!=null)
			<td>{{ $funcionario->matricula_qt }}</td>
		@else
			<td>-</td>
		@endif

		@if($funcionario->matricula_a5!=null)
			<td>{{ $funcionario->matricula_a5 }}</td>
		@else
			<td>-</td>
		@endif
		
		@if(($funcionario->matricula_qt!=null && $funcionario->matricula_a5!=null) && (isset($HNqt[$funcionario->matricula_qt])&&isset($HNa5[$funcionario->matricula_a5]) ))
			<td>{{ number_format(Horario::addFieldTotal('total_HN', ($HNqt[$funcionario->matricula_qt] + $HNa5[$funcionario->matricula_a5])), 2) }}</td>
		
		@elseif($funcionario->matricula_qt!=null && isset($HNqt[$funcionario->matricula_qt]))
			<td>{{ number_format(Horario::addFieldTotal('total_HN', $HNqt[$funcionario->matricula_qt]), 2) }}</td>

		@elseif($funcionario->matricula_a5!=null && isset($HNa5[$funcionario->matricula_a5]))
			<td>{{ number_format(Horario::addFieldTotal('total_HN', $HNa5[$funcionario->matricula_a5]), 2) }}</td>

		@else		
			<td>-</td>
		@endif

		@if(($funcionario->matricula_qt!=null && $funcionario->matricula_a5!=null) && (isset($HEqt[$funcionario->matricula_qt])&&isset($HEa5[$funcionario->matricula_a5]) ))
			<td>{{ number_format(Horario::addFieldTotal('total_HE', ($HEqt[$funcionario->matricula_qt] + $HEa5[$funcionario->matricula_a5])), 2) }}</td>
		
		@elseif($funcionario->matricula_qt!=null && isset($HEqt[$funcionario->matricula_qt]))
			<td>{{ number_format(Horario::addFieldTotal('total_HE', $HEqt[$funcionario->matricula_qt]), 2) }}</td>

		@elseif($funcionario->matricula_a5!=null && isset($HEa5[$funcionario->matricula_a5]))
			<td>{{ number_format(Horario::addFieldTotal('total_HE', $HEa5[$funcionario->matricula_a5]), 2) }}</td>

		@else		
			<td>-</td>
		@endif

		@if(($funcionario->matricula_qt!=null && $funcionario->matricula_a5!=null) && (isset($ADNqt[$funcionario->matricula_qt])&&isset($ADNa5[$funcionario->matricula_a5]) ))
			<td>{{ number_format(Horario::addFieldTotal('total_ADN', ($ADNqt[$funcionario->matricula_qt] + $ADNa5[$funcionario->matricula_a5])), 2) }}</td>
		
		@elseif($funcionario->matricula_qt!=null && isset($ADNqt[$funcionario->matricula_qt]))
			<td>{{ number_format(Horario::addFieldTotal('total_ADN', $ADNqt[$funcionario->matricula_qt]), 2) }}</td>

		@elseif($funcionario->matricula_a5!=null && isset($ADNa5[$funcionario->matricula_a5]))
			<td>{{ number_format(Horario::addFieldTotal('total_ADN', $ADNa5[$funcionario->matricula_a5]), 2) }}</td>

		@else		
			<td>-</td>
		@endif

		@if(($funcionario->matricula_qt!=null && $funcionario->matricula_a5!=null) && (isset($HFqt[$funcionario->matricula_qt])&&isset($HFa5[$funcionario->matricula_a5]) ))
			<td>{{ number_format(Horario::addFieldTotal('total_HF', ($HFqt[$funcionario->matricula_qt] + $HFa5[$funcionario->matricula_a5])), 2) }}</td>
		
		@elseif($funcionario->matricula_qt!=null && isset($HFqt[$funcionario->matricula_qt]))
			<td>{{ number_format(Horario::addFieldTotal('total_HF', $HFqt[$funcionario->matricula_qt]), 2) }}</td>

		@elseif($funcionario->matricula_a5!=null && isset($HFa5[$funcionario->matricula_a5]))
			<td>{{ number_format(Horario::addFieldTotal('total_HF',  $HFa5[$funcionario->matricula_a5]), 2) }}</td>

		@else		
			<td>-</td>
		@endif

		<?php $fields = array('Falta', 'Folga', 'Atestado', 'ADC', 'AD', 'va', 'vt', 'am'); ?>

			@foreach ($fields as $field)
				@if($field=='ADC')
					<?php $field='A Disposição do cliente'; ?>
					@if(array_key_exists($field.$funcionario->cod, $ADC))
						<td>{{{ Horario::addFieldTotal('total_'.$field, $ADC[$field.$funcionario->cod]) }}}</td>
					@else
						<td>-</td>
					@endif

				@elseif($field=='AD')
					<?php $field='A Disposição'; ?>
					@if(array_key_exists($field.$funcionario->cod, $AD))
						<td>{{{ Horario::addFieldTotal('total_'.$field, $AD[$field.$funcionario->cod]) }}}</td>
					@else
						<td>-</td>
					@endif

				@elseif(array_key_exists($field.$funcionario->cod, ${$field}))
					<td>{{{  Horario::addFieldTotal('total_'.$field, ${$field}[$field.$funcionario->cod]) }}}</td>
				@else
					<td>-</td>
				@endif

			@endforeach
		</tr>
	@endforeach
	</tbody>
</table>
	<table border="2" width="100%" bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td rowspan="2" align=center width="4%">Totais</td>
		<td width="8%" align=center>Hora Direta</td>
		<td width="8%" align=center>Hora Extra</td>
		<td width="8%" align=center>Adicional Noturno</td>
		<td width="8%" align=center>Horas Faturadas</td>
		<td width="8%" align=center>Falta</td>
		<td width="8%" align=center>Folga</td>
		<td width="8%" align=center>Atestado</td>
		<td width="8%" align=center>A Disposição do cliente</td>
        <td width="8%" align=center>A Disposição</td>
		<td width="8%" align=center>VA/VR</td>
		<td width="8%" align=center>VT</td>
		<td width="8%" align=center>Moradia</td>
	</tr>
	<tr>
	<?php $fields2 = array('HN','HE','ADN', 'HF', 'Falta', 'Folga', 'Atestado', 'ADC', 'AD', 'va', 'vt', 'am'); ?>
	@foreach ($fields2 as $field)
			
		@if($field=='ADC')
			<?php $field='A Disposição do cliente'; ?>
		@elseif($field=='AD')
			<?php $field='A Disposição'; ?>
		@endif

		@if($field=='HN' || $field=='HE' || $field=='ADN' || $field=='HF' )
			<td width="8%" align=center><br>{{ number_format(Horario::getFieldTotal('total_'.$field), 2,',', '.') }}</td>
		@else
			<td width="8%" align=center><br>{{ Horario::getFieldTotal('total_'.$field) }}</td>
		@endif

	@endforeach
		
	</tr>
</table>

</div>			

	@else
        @include('accessdenied')
    @endif
    
@stop
