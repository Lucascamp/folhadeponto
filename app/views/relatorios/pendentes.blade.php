@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<h1>Pendências do Período</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'relatorios.pendentes')) }}

<div class="row">
<div class="col-md-3">
<h2>Escolha o período do relatório</h2>
<div class="clear"><br></div>
        <div class="form-group">
            {{ Form::label('labelTodos', 'Periodo:') }}
            {{ Form::select('todos', $todos) }} 

            <div class="clear"><br></div>

             {{ Form::submit('Gerar Relatório', array('class' => 'btn btn-info mensagem_aguarde', 'id' => 'mensagem_aguarde')) }}
                
        </div>
    </div>   
{{ Form::close() }}
 
<div id="screen"></div>
<div id="dvLoading"></div>
<div id="dvMessage"><center><font size='6' color='white'>Gerando Relatório</font></center></div>

</div>  

    @if ($errors->any())
    <ul>
      {{ implode('', $errors->all('<li class="error">:message</li>')) }}
  </ul>
  @endif

    @else
        @include('accessdenied')
    @endif

  @stop


