@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

    <div class="area_impressao" name="area_impressao" id="area_impressao">
        <table width="100%" height="73" class='tablefolha'>
    <tr>
        <td width='50%' align='right'><center><b><font size="5" face="Arial">Relatório administrativo</font></b></center>
        
        <right>Período: de {{{ $periodoInicio->format('d/m/Y') }}} a {{{ $periodoFinal->format('d/m/Y') }}}</right>

        </td>
    </tr>
</table>
    <div class="clear"><br></div>
    
        <table class="table table-responsive table-bordered" align="center">
    <thead>
        <tr>
            <th>Data</th>
            @foreach (range(1, $dias) as $index)
            <?php $diainicio->addDay(); ?>
                <th>{{ $diainicio->format('d/m/Y') }}</th>
            @endforeach
        </tr>
    </thead>
    
    <tbody>

    @foreach ($funcionarios as $key => $funcionario)
        <tr>
            <td>{{ $funcionario }}</td>

            <?php  $currentData = clone $periodoInicio; ?>

            @foreach (range(1, $dias) as $index)
            <?php $currentData->addDay(); ?>

                @if(isset($checkHorario[$key][$currentData->format('Y-m-d')]))
                    <td style="background:#91FFB2;">OK!</td>
                @else
                    <td style="background:#FC8B8B">-</td>    
                @endif

            @endforeach

        </tr>
    @endforeach

    </tbody>
</table>

</div>          

    @else
        @include('accessdenied')
    @endif
    
@stop