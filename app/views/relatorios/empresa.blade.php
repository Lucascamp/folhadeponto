@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<h1>Somatório do Período dividido pelas empresas</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'relatorios.empresa')) }}

<h3>Escolha o período e funcionários para o relatório</h3>
<div class="clear"><br></div>
<div class='row'>    
  <div class='col-md-3'>    
          Período:
          <div class="clear"><br></div>
          {{ Form::select('todos', $todos) }}
  </div> 
         <div class="clear"><br></div>
        {{ Form::submit('Gerar Relatório', array('class' => 'btn btn-info mensagem_aguarde', 'id' => 'mensagem_aguarde')) }}
</div>  

{{ Form::close() }}

<div id="screen"></div>
<div id="dvLoading"></div>
<div id="dvMessage"><center><font size='6' color='white'>Gerando Relatório</font></center></div>

@if ($errors->any())
  <ul>
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
  </ul>
@endif

@else
  @include('accessdenied')
@endif

@stop


