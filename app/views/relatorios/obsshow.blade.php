@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

	<div class="area_impressao" name="area_impressao" id="area_impressao">
		<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='50%' align='right'><center><b><font size="5" face="Arial">Relatório de observações</font></b></center>
		
		@if(isset($datainicio))
			<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right>
		@endif

		</td>
	</tr>
</table>
	<div class="clear"><br></div>
	
		<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
        	<th width='5%'>Data</th>
            <th width='20%'>Funcionário</th>
            <th width='5%'>Entrada</th>  
            <th width='5%'>Intervalo</th> 
            <th width='5%'>Intervalo</th> 
            <th width='5%'>Saída</th>            
            <th width='5%'>Total</th>
            <th width='50%'>Observações</th>
        </tr>
    </thead>
    
    <tbody>

  	@foreach ($horarios as $horario)
  		<tr>
  			<td>{{{ $horario->data->format('d/m/Y') }}}</td>
			<td>{{ $horario->funcionario->nome }}</td>
			<td>{{ $horario->hora_entrada->format('H:i') }}</td>
			<td>{{ $horario->intervalo_inicio->format('H:i') }}</td>
			<td>{{ $horario->intervalo_fim->format('H:i') }}</td>
			<td>{{ $horario->hora_saida->format('H:i') }}</td>
			<td>{{ $horario->getWorkedHours()->format('H:i') }}</td>	
			<td>{{ $horario->observacoes }}</td>
		</tr>
	@endforeach

	</tbody>
</table>

</div>			

	@else
        @include('accessdenied')
    @endif
    
@stop