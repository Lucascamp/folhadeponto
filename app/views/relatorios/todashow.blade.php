@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<div class="area_impressao" name="area_impressao" id="area_impressao">

<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width='30%'>	
			<left>     
        {{ link_to_route('relatorios.exportar', 'Exportar para excel', array($cod), array('class' => 'btn btn-success', 'id' => 'exportar')) }}
        <img src="../images/loadexcel.gif" id='imgexportar'>
      </left>
		</td>
		<td width='40%'>	
			<center><b><font size="5" face="Arial">Total de {{ $count }} marcações</font></b></center>
		</td>
		<td width='30%' align='right'>
	 		<right>Período: de {{{ $datainicio->format('d/m/Y') }}} a {{{ $datafinal->format('d/m/Y') }}}</right>
	 	</td>
	</tr>
</table>
	<div class="clear"><br></div>
	
		<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
            <th rowspan = '2' width='2%'>Unidade</th>
            <th rowspan = '2' width='12%'>Funcionário</th>
            <!-- <th rowspan = '2' width='5%'>Função QT</th>
            <th rowspan = '2' width='5%'>Função A5</th> -->
            <th rowspan = '2' width='3%'>Data</th>
            <th colspan = '9' width='15%'>Horas</th>
            <th rowspan = '2' width='3%'>Tempo Exposição</th>	
            <th rowspan = '2' width='8%'>Atividade</th>	
            <th rowspan = '2' width='15%'>Item Contábil</th>
            <th rowspan = '2' width='15%'>Observações</th>
        </tr>
        <tr>
          <th width='3%'>Entrada</th>
          <th width='3%'>Intervalo</th>
          <th width='3%'>Intervalo</th>
          <th width='3%'>Saida</th>  
          <th width='3%'>Totais</th>
          <th width='3%'>Trabalhadas</th> 
          <th width='3%'>Disposicao</th>   
          <th width='3%'>Extras</th>
          <th width='3%'>Contratadas</th>
        </tr>
    </thead>
    
    <tbody>
  	@foreach ($horarios as $horario)
  	<tr>
    
  		<td>{{ $horario->unidade }}</td>
  		<td>{{ $horario->funcionario->nome }}</td>
  		
     <!--  @if($horario->funcionario->funcao_qt != null)
        <td>{{ $horario->funcionario->funcao_qt }}</td>
      @else 
        <td>-</td>
      @endif  

      @if($horario->funcionario->funcao_a5 != null)
        <td>{{ $horario->funcionario->funcao_a5 }}</td>
      @else 
        <td>-</td>
      @endif   -->
      
  		<td>{{ $horario->data->format('d/m/Y') }}</td>
      <td>{{ $horario->hora_entrada->format('H:i:s') }}</td>
      <td>{{ $horario->intervalo_inicio->format('H:i:s') }}</td>
      <td>{{ $horario->intervalo_fim->format('H:i:s') }}</td>
      <td>{{ $horario->hora_saida->format('H:i:s') }}</td>
      <td>{{{ $horario->getWorkedHours()->format('H:i') }}}</td>
      <td>{{{ $horario->getClientHours()->format('H:i') }}}</td>
      <td>{{{ $horario->getDisposicaoHours()->format('H:i') }}}</td>
      <td>{{{ $horario->getExtraHours()->format('H:i') }}}</td>

  		<td>{{ $horario->hora_contratada }}</td>
  		@if($horario->tempo_exposicao != null)
  			<td>{{ $horario->tempo_exposicao }} </td>
  		@else	
  			<td>-</td>
  		@endif
  		<td>{{ $horario->atividade }}</td>
  		<td>{{ $horario->item_contabil->CTD_DESC01 }} </td>
  		<td>{{ $horario->observacoes }}</td>
	</tr>
	@endforeach

	</tbody>
</table>
	
</div>			

@else
  @include('accessdenied')
@endif
    
@stop