@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('relatorios_menu'))

<h1>Relatórios</h1>

<div class="clear"><br></div>

<div class="menu">
            <ul>
                @if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/empresa"><img src="../images/ico_relatorio_simples.png" border="0"/>Somatório por empresa</a></li>
                @endif

                @if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/geral"><img src="../images/ico_relatorio_simples.png" border="0"/>Somatório do Período</a></li>
                @endif

                <!--@if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/observacoes"><img src="../images/ico_relatorio_simples.png" border="0"/>Observações<br>&nbsp;</a></li>
                @endif

                @if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/pendentes"><img src="../images/ico_relatorio_simples.png" border="0"/>Pendências
                    <br>&nbsp;</a></li>
                @endif
                -->

                <!-- // @if(User::checkPermission('visualizar_menu'))  -->
                    <!-- <li><a href="relatorios/marcacoes"><img src="../images/ico_relatorio_simples.png" border="0"/>Marcações Diárias</a></li> -->
                <!-- // @endif -->

                @if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/marcacoes2"><img src="../images/ico_relatorio_simples.png" border="0"/>Marcações Diárias</a></li>
                @endif

                @if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/todas"><img src="../images/ico_relatorio_simples.png" border="0"/>Todas Marcações</a></li>
                @endif

                @if(User::checkPermission('visualizar_menu')) 
                    <li><a href="relatorios/administrativo"><img src="../images/ico_relatorio_simples.png" border="0"/>Marcações Administrativo</a></li>
                @endif
            </ul>

        </div>
    
    <div class="clear"><br></div>

    @else
        @include('accessdenied')
    @endif

@stop
