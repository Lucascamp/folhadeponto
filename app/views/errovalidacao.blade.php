@extends('layouts.scaffold')

@section('main')


<h3 align='center'>
Atualmente, seu usuário não possui nenhum funcionário atribuido para ser validado, favor contatar pelo e-mail lucas.campos@applus.com <br><br> com a lista de funcionários os quais é responsável.

Obrigado
<br>
<br>
<br>

	 <center>
	 	{{ link_to('/', 'Retornar ao Início', array('class' => 'btn btn-success')) }}
	 </center>
@stop