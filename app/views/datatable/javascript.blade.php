<script type="text/javascript">
    jQuery(document).ready(function(){
        // dynamic table
        var oTable = jQuery('.{{ $class }}').dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": false,
            @foreach ($options as $k => $o)
            {{ json_encode($k) }}: {{ json_encode($o) }},
            @endforeach
            @foreach ($callbacks as $k => $o)
            {{ json_encode($k) }}: {{ $o }},
            @endforeach
            
            "fnDrawCallback": function ( nRow, aData, iDisplayIndex ) {

                $('.unidade').editable('/sistemarx/modulo_ponto/horarios/edit', { 
                    data   : Qualitec.unidades,
                    type   : 'select',
                    submit : 'OK',
                    "submitdata": function ( value, settings ) 
                    {
                        var aPos = oTable.fnGetPosition( this );
                        var aData = oTable.fnSettings().aoData[ aPos[0] ]._aData;
                        var field = $(this).attr('class').split(' ').pop();

                        return {id: aData[0], column: field}                        
                    },
                });

                $('.cod_item_contab').editable('/sistemarx/modulo_ponto/horarios/edit', { 
                    data   : Qualitec.itens_contabil,
                    type   : 'select',
                    submit : 'OK',
                    "submitdata": function ( value, settings ) 
                    {
                        var aPos  = oTable.fnGetPosition( this );
                        var aData = oTable.fnSettings().aoData[ aPos[0] ]._aData;                        
                        var field = $(this).attr('class').split(' ').pop();

                        var new_value = $('option:selected', $(this)).val();
                        

                        return {id: aData[0], column: field, label: settings.data[new_value]}
                    }
                });

                 $('.hora_contratada').editable('/sistemarx/modulo_ponto/horarios/edit', { 
                    data   : Qualitec.contratadas,
                    type   : 'select',
                    submit : 'OK',
                    "submitdata": function ( value, settings ) 
                    {
                        var aPos = oTable.fnGetPosition( this );
                        var aData = oTable.fnSettings().aoData[ aPos[0] ]._aData;
                        var field = $(this).attr('class').split(' ').pop();

                        return {id: aData[0], column: field}                        
                    },
                });

                $('.atividade').editable('/sistemarx/modulo_ponto/horarios/edit', { 
                    data   : Qualitec.atividades,
                    type   : 'select',
                    submit : 'OK',
                    "submitdata": function ( value, settings ) 
                    {
                        var aPos = oTable.fnGetPosition( this );
                        var aData = oTable.fnSettings().aoData[ aPos[0] ]._aData;
                        var field = $(this).attr('class').split(' ').pop();

                        return {id: aData[0], column: field}                        
                    },
                });

                $('.delete-row').on('click', function(e){
                     e.preventDefault();

                    var $tr = $(this).parents('tr');

                    var idValue = $tr.find('td').first().text();                        
                    excluir(idValue, $tr);                    

                 });

                // custom values are available via $values array
                /* Apply the jEditable handlers to the table */      
                $('.{{ $class }} tbody td:not(.readonly)').editable( '/sistemarx/modulo_ponto/horarios/edit', { 

                    "callback": function( sValue, y ) {
                        var aPos = oTable.fnGetPosition( this );
                        //oTable.fnUpdate( sValue, aPos[0], aPos[1] );

                        // Redraw the table from the new data on the server
                        oTable.fnDraw();
                    },
                    "submitdata": function ( value, settings ) {

                        var aPos = oTable.fnGetPosition( this );
                        var aData = oTable.fnSettings().aoData[ aPos[0] ]._aData;
                        var field = $(this).attr('class').split(' ').pop();
                        

                        return {id: aData[0], column: field}
                    },

                    type:'text',            
                    tooltip   : 'Clique para editar...',
                    indicator : 'Salvando...'
                });
                
            }
        });
});
</script>