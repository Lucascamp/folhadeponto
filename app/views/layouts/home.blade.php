<!doctype html>
<html>
<head>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sistema de Programação de RX" />
       
    {{ HTML::style('components/vendor/bootstrap/dist/css/bootstrap.min.css') }}
    {{ HTML::style('css/style.css')}}
    {{ HTML::style('css/chosen.min.css') }}
    {{ HTML::style('components/vendor/datatables/media/css/jquery.dataTables.css') }}    
    {{ HTML::style('css/main.css') }}

</head>

<body>

    @include('layouts.divhorarios')
    @include('layouts.divopcoes')
    @include('layouts.header')
    
     <div class="container-fluid">

                @yield('main')
                       
        </div>     </div>
   
{{ HTML::script('components/vendor/jquery/jquery.js') }}
{{ HTML::script('components/vendor/datatables/media/js/jquery.dataTables.js') }}
{{ HTML::script('components/vendor/jquery_jeditable/jquery.jeditable.js'); }}
{{ HTML::script('components/vendor/jquery-maskedinputs/dist/jquery.maskedinput.min.js') }}
{{ HTML::script('components/vendor/chosen/chosen.jquery.js') }}
{{ HTML::script('components/vendor/bootstrap/dist/js/bootstrap.js'); }}
{{ HTML::script('components/vendor/printArea/demo/jquery.PrintArea.js') }}
{{ HTML::script('js/datatables/datatables-bootstrap-pagination.js'); }}
{{ HTML::script('js/bootstrap-dialog.js'); }}
{{ HTML::script('js/main.js'); }}

@yield('footer_scripts')
    
@include('layouts.footer')

</body>

</html>