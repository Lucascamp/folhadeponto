<!doctype html>
<html>
<head>
    <meta charset="utf-8">    

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sistema de Programação de RX" />

    <meta name="description" content="Sistema de Programação de RX" />    
    {{ HTML::style('components/vendor/bootstrap/dist/css/bootstrap.min.css') }}
    {{ HTML::style('css/style.css')}}
    {{ HTML::style('css/chosen.min.css') }}
    {{ HTML::style('components/vendor/datatables/media/css/jquery.dataTables.css') }}
    {{ HTML::style('js/datepicker/css/datepicker.css') }}   
    {{ HTML::style('css/main.css') }}

</head>

<body>

    @include('layouts.divopcoes')
    @include('layouts.header')
    

    <div class="corner_wrap">
        @if (Session::has('message'))
        <div class="flash alert">
            <p>{{ Session::get('message') }}</p>
        </div>
        @endif

        @yield('main')

    </div>


    @include('layouts.partials._scripts')

    @yield('footer_scripts')
    
    @include('layouts.footer')

</body>

</html>