<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<title>Sistema RX | Módulo Ponto</title>

<?php
	$urls=['marcacao/administrativo'];
	$urlatual=Request::url();
?>

<div id="header">
	
		@if(strpos($urlatual, $urls[0]))
  			<a href="/sistemarx/" class="left_bt">INÍCIO</a>
		@else
  			<a href="{{ ponto_url() }}" class="left_bt">INÍCIO</a>  
		@endif
		
		<span class="system-title">Folha de Ponto {{$version}}</span>
		<a href="../ajuda" class="right_bt" id="ajuda"><img src="{{ ponto_url() }}/images/ajuda.png" alt="CLIQUE AQUI OU PRESSIONE F1" title="PRECISA DE AJUDA? CLIQUE AQUI OU PRESSIONE F1" border="0" /></a>
    </div>
	<div id="main_container">
		<div class="content">	