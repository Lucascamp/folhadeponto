<div class="box" id="menuhorarios">
	<div class="box_content">
		<div class="box_content_tab">HORÁRIOS</div>
		<div class="box_content_center">

			<div class="menu">
				<ul>
					@if(User::checkPermission('visualizar_cadastrar')) 
                   		<li><a class="sub" href="horarios/create" id="horarios_criar"><img src="images/ico_adicionar.png" border="0" alt="" title=""/>Criar <br> Marcações</a></li>
               		@endif
					
					@if(User::checkPermission('visualizar_menu')) 
                    	<li><a class="sub" href="horarios" id="horarios_visualizar"><img src="images/ico_datatable.png" border="0" alt="" title=""/>Tabela de Marcações</a></li>
                	@endif

                	@if(User::checkPermission('visualizar_criarperiodo')) 
                   		<li><a class="sub" href="horarios/criarvarios" id="horarios_varios"><img src="images/ico_varias_marcacoes.png" border="0" alt="" title=""/>Criar Periodo de Marcações</a></li>
               		@endif

               		@if(User::checkPermission('visualizar_cadastrar')) 
                   		<!-- <li><a class="sub" href="horarios/variosfuncionarios" id="horarios_criar"><img src="images/ico_multiplos.png" border="0" alt="" title=""/>Marcação Equipe</a></li> -->
               		@endif

               		@if(User::checkPermission('visualizar_cadastrar')) 
                   		<li><a class="sub" href="horarios/variosfuncionarios2" id="horarios_criar"><img src="images/ico_multiplos.png" border="0" alt="" title=""/>Marcações Multiplas</a></li>
               		@endif
				</ul>
			</div>                       

			<a class="boxclose_right" id="horarios_fechar">Fechar</a>
			<div class="clear"></div>

		</div>
	</div>
</div>