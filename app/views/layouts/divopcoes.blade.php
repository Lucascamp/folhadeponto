 <div class="box" id="menuopcoes" >
 	<div class="box_content">
 	<div class="box_content_tab">MENU OPCÕES</div>
 		<div class="box_content_center">
 			<div class="menu">
 				<ul>
  					@if((User::checkPermission('relatorio_imprimir'))||(User::checkPermission('folha_imprimir'))||(User::checkPermission('espelho_imprimir')))
						<li><a href="#" class="sub" id="imprimir" name="imprimir"><img src="../images/ico_imprimir.png">IMPRIMIR PÁGINA</a></li>
					@endif

					@if(User::checkPermission('relatorios_menu'))
						<li><a href="#" class="sub" id="exporta_excel_xls"><img src="../images/ico_excel_xls.png">EXPORTAR </br>XLS</a></li>
					@endif

 				</ul>
 			</div>	  
		
 			<a class="boxclose_right" id="menuopcoes_fechar">Fechar</a>
 			
 			<div class="clear"></div>

 		</div>
	</div>
</div>