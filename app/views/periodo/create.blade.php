@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('gerenciamento_menu'))

<h1>Gerenciar períodos</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'periodos')) }}

<h2>Criar periodo impressão da folha</h2>

<div class="clear"><br></div>
<div class="row">
    <div class="form-group col-md-3">
    
        {{ Form::label('mesref', 'Mês de referência:') }}
        {{ Form::select('mes', array(
            'Janeiro' => 'Janeiro',
            'Fevereiro' => 'Fevereiro',
            'Março' => 'Março',
            'Abril' => 'Abril',
            'Maio' => 'Maio',
            'Junho' => 'Junho',
            'Julho' => 'Julho',
            'Agosto' => 'Agosto',
            'Setembro' => 'Setembro',
            'Outubro' => 'Outubro',
            'Novembro' => 'Novembro',
            'Dezembro' => 'Dezembro')) }}
    </div>
    
    <div class="form-group col-md-3">
        {{ Form::label('anoref', 'Ano:') }}
        {{ Form::select('ano', $ano, $anoatual) }}
    </div>
    
    <div class="form-group col-md-2">
        {{ Form::label('inicio', 'Início do período:') }}
        {{ Form::text('datainicio', null, array('class'=>'datepicker')) }}
    </div>
    
    <div class="form-group col-md-3">
        {{ Form::label('final', 'final do período:') }}
        {{ Form::text('datafinal', null, array('class'=>'datepicker')) }}
    </div>
    
    <div class="clear"><br></div>

    <div class="form-group col-md-3">
        {{ Form::submit('Criar período', array('class' => 'btn btn-success')) }}
    </div>

    <div class="clear"></div>

    {{ Form::close() }}

    <hr>
</div> 

{{ Form::open(array('route' => 'editperiodo')) }}

<div class="row">
<div class="col-md-3">

    <h2>Escolha o período a ser editado</h2>

<div class="clear"><br></div>
        
    <div class="form-group">
        {{ Form::label('labelTodos', 'Periodo:') }}
        {{ Form::select('todos', $todos) }} 

        <div class="clear"><br></div>

        {{ Form::submit('Editar período', array('class' => 'btn btn-info')) }}
        {{ Form::close() }}     
    </div>
</div>   


<div class="col-md-1"></div>

{{ Form::open(array('route' => 'statusperiodo')) }}

<div class="row">
<div class="col-md-3">

    <h2>Fechar período em aberto</h2>

<div class="clear"><br></div>
    <div class="form-group">
        {{ Form::label('labelAberto', 'Periodo:') }}
        {{ Form::select('aberto', $aberto) }} 

        <div class="clear"><br></div>

        {{ Form::submit('Fechar período', array('class' => 'btn btn-warning fecharperiodo')) }}
                
    </div>

</div>

<div class="col-md-1"></div>

<div class="col-md-3">

    <h2>Reabrir períodos fechados</h2>

<div class="clear"><br></div>
    
    <div class="form-group">
        {{ Form::label('labelFechado', 'Periodo:') }}
        {{ Form::select('fechado', $fechado) }}        

        <div class="clear"><br></div>

        {{ Form::submit('Abrir período', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}     
    </div>
       
</div>
  
</div>  

<hr>

@if ($errors->any())
    <div class="alert-group">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ implode('', $errors->all('<p>:message</p>')) }}
        </div>
    </div>
@endif

    @else
        @include('accessdenied')
    @endif

  @stop


