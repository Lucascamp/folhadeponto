@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('gerenciamento_menu'))

<h1>Editar período</h1>
{{ Form::open(array('route' => 'updateperiodo')) }}

{{ Form::hidden('cod', $periodo->cod)  }}

<div class="col-md-2">
  <div class="form-group">

    {{ Form::label('mes', 'Mês:') }}
    {{ Form::text('mes_referencia', $periodo->mes_referencia, array('disabled'=>'disabled')) }}           

  </div>
</div>

<div class="col-md-1">
  <div class="form-group">

    {{ Form::label('anoref', 'Ano:') }}
    {{ Form::text('ano', $periodo->ano, array('disabled'=>'disabled')) }}           

  </div>
</div>

<div class="col-md-2">
  <div class="form-group">

    {{ Form::label('data1', 'Período início:') }}
    {{ Form::text('datainicio', $datainicio->format('d/m/Y'), array('class'=>'datepicker form-control')) }}

  </div>
</div>

<div class="col-md-2">
  <div class="form-group">

    {{ Form::label('data2', 'Período final:') }}
    {{ Form::text('datafinal', $datafinal->format('d/m/Y'), array('class'=>'datepicker form-control')) }}

  </div>
</div>     

<div class="clear"><br></div>

{{ Form::submit('Salvar Alterações', array('class' => 'btn btn-success')) }}

{{ Form::close() }}
 
@if ($errors->any())
    <div class="alert-group">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ implode('', $errors->all('<p>:message</p>')) }}
        </div>
    </div>
@endif
    
    @else
        @include('accessdenied')
    @endif

@stop
