@extends('layouts.home')

@section('main')

    <div class="logo">
        <img src="images/logo.png" alt="" title="" border="0" />
    </div>
                    
        <div class="menu">
            <ul>
                    <li><a href="#" id="horarios_abrir"><img src="images/ico_visualizar_marcacoes.png" border="0" alt="" title=""/>Marcações</a></li>
                
                <!-- @if(User::checkPermission('buscar_menu')) -->
                    <!-- <li><a href="atualizacoes_automaticas" id="pontos_go_abrir"><img src="images/ico_atualizar.png" border="0" alt="" title=""/>Atualizações Automáticas</a></li> -->
                <!-- @endif -->

                @if(User::checkPermission('validar_menu'))
                    <li><a href="horarios/validar" id="usuario_abrir"><img src="images/ico_validar_marcacao.png" border="0" alt="" title=""/>Validação</a></li>
                @endif
                
                @if(User::checkPermission('pendencias_menu'))
                    <li><a href="horarios/inconsistencia" id="inconsistencia_abrir"><img src="images/ico_relatorio_inconsistencias.png" border="0" alt="" title=""/>Pendências</a></li>
                @endif

                @if(User::checkPermission('gerenciamento_menu'))
                    <li><a href="periodos" id="periodos_abrir"><img src="images/ico_periodo.png" border="0" alt="" title=""/>Períodos</a></li>
                @endif

                @if(User::checkPermission('validar_menu'))
                     <li><a href="ferias"><img src="images/ico_ferias.png" border="0" alt="" title=""/>Férias</a></li>
                @endif

                <br>

                @if(User::checkPermission('espelho_menu'))
                    <li><a href="espelho" id="espelho_abrir"><img src="images/ico_espelho.png" border="0" alt="" title=""/>Espelho</a></li>
                @endif

                @if(User::checkPermission('folha_menu'))
                    <li><a href="folha" id="folha_abrir"><img src="images/ico_imprime_folha_ponto.png" border="0" alt="" title=""/>Folha</a></li>
                @endif

                @if(User::checkPermission('relatorios_menu'))
                    <li><a href="relatorios" id="relatorios_abrir"><img src="images/ico_report.png" border="0" alt="" title=""/>Relatórios</a></li>
                @endif

                @if(User::checkPermission('exportar_menu'))
                    <li><a href="exportar" id="exportar_abrir"><img src="images/ico_exportar.png" border="0" alt="" title=""/>Exportação</a></li>
                @endif
                
            </ul>
        </div>

    @stop

    