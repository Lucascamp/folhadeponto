@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('espelho_menu'))

<h1>Espelhamento da folha de ponto</h1>
	
<br>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'espelhofolha', 'method' => 'post')) }}
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('func', 'Selecione o funcionário') }}
			{{ Form::select('funcionario', $funcionarios) }}
		</div>
	</div>
	<?php $user=''; ?>

	@if($user == 'rh')
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('datai', 'Inicio do período:') }}
			{{ Form::text('datainicio', null, array('class'=>'mask-date form-control')) }}
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('dataf', 'Final do período:') }}
			{{ Form::text('datafinal', null, array('class'=>'mask-date form-control')) }}
		</div>
	</div>

	@else
	{{ Form::hidden('datainicio', null) }}
	{{ Form::hidden('datafinal', null) }}
	@endif
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::label('labelPeriodo', 'Periodos em aberto:') }}
			{{ Form::select('periodo', $periodos) }}        
		</div>
	</div>
</div>

<div class="clear"><br></div>
<div class="clear"><br></div>

<div class="form-group col-md-3">
	{{ Form::submit('Criar espelho de ponto', array('class' => 'btn btn-success')) }}
</div>

{{ Form::close()}}
<div class="clear"></div>
@if ($errors->any())    
<ul>
	{{ implode('', $errors->all('<li class="error">:message</li>')) }}
</ul>
@endif
	
	@else
        @include('accessdenied')
    @endif

@stop
