@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('espelho_menu'))

	<div class="area_impressao" name="area_impressao" id="area_impressao">
		@include('espelhofolha.partials.table', $data)		
	</div>			
 
	@else
        @include('accessdenied')
    @endif
    
@stop