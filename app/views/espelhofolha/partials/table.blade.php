<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td align='center' colspan="2"><b><font size="5" face="Arial">Espelhamento da Folha de Ponto</font></b></td>
	</tr>
	<tr>
		<td width="50%">Nome: {{ $funcionario->nome }}</td>
		<td width="50%" align="right">Período: de {{{$datainicio->format('d/m/Y')}}} a {{{$datafinal->format('d/m/Y')}}}</td>
	</tr>
	</table>

	<div class="clear"><br></div>
	@if ($horarios)
		@include('espelhofolha.partials.table_content', array('horarios'=>$horarios, 'periodo'=>$periodo))
	@else
	<h2>Nenhuma marcação encontrada.</h2>
	@endif

<table border="2" width="100%" bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td colspan="6" align=center>Total de horas</td>
		<td colspan="3" align=center>Atividades</td>
		<td colspan="3" align=center>Beneficios</td>
		<td rowspan="3" valign="top" width="10%">Observações:</td>
	</tr>
	<tr>
		<td width="5%" align=center>Diretas</td>
		<td width="5%" align=center>Extras</td>
		<td width="5%" align=center>Noturnas</td>
		<td width="5%" align=center>Trabalhadas</td>
		<td width="5%" align=center>Disposição</td>
		<td width="5%" align=center>TFE</td>
		<td width="3%" align=center>Falta</td>
		<td width="3%" align=center>Atestado</td>
		<td width="3%" align=center>Folga</td>
		<td width="3%" align=center>VA/VR</td>
		<td width="3%" align=center>VT</td>
		<td width="3%" align=center>AM</td>
	</tr>
	<tr>
		<td align=center><br>{{Horario::getTotalHour('worked_hour')}}</td>
		<td align=center><br>{{Horario::getTotalHour('extra_hour')}}</td>
		<td align=center><br>{{ number_format((Horario::getTotalHour('night_hour')*1.14), 2) }}</td>
		<td align=center><br>{{Horario::getTotalHour('client_hour')}}</td>
		<td align=center><br>{{Horario::getTotalHour('disposicao_hour')}}</td>
		<td align=center><br>{{Horario::getTotalHour('exposed_hour')}}</td>
		<td align=center><br>{{Horario::getTotalActivity('Falta', 'Atividade')}}</td>
		<td align=center><br>{{Horario::getTotalActivity('Atestado', 'Atividade')}}</td>
		<td align=center><br>{{Horario::getTotalActivity('Folga', 'Atividade')}}</td>
		<td align=center><br>{{Horario::getTotalActivity('va', 'Atividade')}}</td>
		<td align=center><br>{{Horario::getTotalActivity('vt', 'Atividade')}}</td>
		<td align=center><br>{{Horario::getTotalActivity('am', 'Atividade')}}</td>
	</tr>
</table>

<table width="100%" border='1' bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td align='center'><b><font size="3" face="Arial" color='red'>Este espelho é apenas uma previsão não é valido como folha de ponto definitiva</font></b></td>
	</tr>
</table>
