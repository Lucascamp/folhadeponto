<?php use Carbon\Carbon; ?>
<tr>
	<td>{{{ $horario->unidade }}}</td>
	<td>{{{ $horario->data->format('d/m/Y') }}}</td>
	@if(($horario->atividade == 'Falta') || ($horario->atividade == 'Folga') ||($horario->atividade == 'Férias'))
	<td>-</td>	
	<td>-</td>
	<td>-</td>	
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	@else
		<td>{{{ $horario->hora_entrada->format('H:i') }}}</td>	
		<td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
		<td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>	
		<td>{{{ $horario->hora_saida->format('H:i') }}}</td>
		<td>{{{ Horario::addTotalHour($horario->getExtraHours(), 'extra_hour')->format('H:i') }}}</td>
	 	<td>{{{ Horario::addTotalHour($horario->getNightHours(), 'night_hour')->format('H:i') }}}</td>
		<td>{{{ Horario::addTotalHour($horario->getWorkedHours(), 'worked_hour')->format('H:i') }}}</td>
		<td>{{{ Horario::addTotalHour($horario->getClientHours(), 'client_hour')->format('H:i') }}}</td>
		<td>{{{ Horario::addTotalHour($horario->getDisposicaoHours(), 'disposicao_hour')->format('H:i') }}}</td>
			
		<td>
			@if($horario->tempo_exposicao != null)
				{{{ Horario::addTotalHour(Carbon::createFromFormat('H:i:s', $horario->tempo_exposicao), 'exposed_hour')->format('H:i:s') }}}
			@else
			-	
			@endif
		</td>
		<?php $beneficio = explode(',', $horario->beneficios); ?>
		
		@if((in_array('va', $beneficio))&&($horario->funcionario->unidade != $horario->unidade))
				<td>2 {{{ Horario::addActivityDay('2xva', 'Atividade') }}}</td>
			@elseif(in_array('va', $beneficio))
				<td>1 {{{ Horario::addActivityDay('va', 'Atividade') }}}</td>
			@else
				<td>-</td>
		@endif

		@if(in_array('vt', $beneficio))
				<td>1 {{{ Horario::addActivityDay('vt', 'Atividade') }}}</td>
			@else
				<td>-</td>
		@endif

		@if(in_array('am', $beneficio))
				<td>1 {{{ Horario::addActivityDay('am', 'Atividade') }}}</td>
			@else
				<td>-</td>
		@endif

	@endif
	<td>{{{ $horario->atividade, Horario::addActivityDay($horario->atividade, 'Atividade') }}}</td>
	<td>{{{ substr($horario->item_contabil->CTD_DESC01, 0, 29) }}}</td>
</tr>