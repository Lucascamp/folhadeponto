<table width='100%' class="zebra3" align="center">
     <thead>
        <tr>
            <th rowspan = '2' width='3%'>Unidade</th>
            <th rowspan = '2' width='5%'>Data</th>
            <th colspan = '4' width='25%'>Horas da jornada</th>
            <th colspan = '5' width='25%'>Horas Totais da diária</th>
            <th rowspan = '2' width='4%'>TFE</th>
            <th rowspan = '2' width='3%'>VA/VR</th>
            <th rowspan = '2' width='3%'>VT</th>
            <th rowspan = '2' width='3%'>AM</th>
            <th rowspan = '2' width='12%'>Atividade</th>
            <th rowspan = '2' width='25%'>Centro de Custo</th>
        </tr>
        <tr>
            <th width='5%'>Entrada</th>             
            <th width='5%'>Intervalo</th>
            <th width='5%'>Intervalo</th>
            <th width='5%'>Saída</th>
            <th width='5%'>Extras</th>
            <th width='5%'>Noturnas</th>
            <th width='5%'>Total</th>
            <th width='5%'>Trabalhadas</th>
            <th width='5%'>Disposição</th>
        </tr>
    </thead>

    <tbody>

    <?php         
        $horarios = $horarios->getIterator();
        $currentData = clone $datainicio;    
    ?>


        @foreach (range(1, $periodo) as $index)

            @if($horarios->count())

                <?php $horario = $horarios->current(); ?>

                @if($horario instanceof Horario && $currentData->format('Y-m-d') == $horario->data->format('Y-m-d'))
                    @include('espelhofolha.partials.row', array('horario'=> $horario))
                    <?php $horarios->next(); ?>
                @else
                    @include('espelhofolha.partials.row_fake', array('current'=> $currentData, 'horario'=> $horario))
                @endif

                <?php $currentData->addDay(); ?>
            @else
               @include('espelhofolha.partials.row_fake', array('current'=> $currentData))
               <?php $currentData->addDay(); ?>
            @endif
        @endforeach        
    </tbody>
</table>