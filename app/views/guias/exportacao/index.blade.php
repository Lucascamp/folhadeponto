@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('exportar_menu'))

<h1>Exportar para o Protheus</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'exportar')) }}

<div class="row">
  <div class="col-md-3">
    <h2>Escolha o período a ser exportado</h2>
    <div class="clear"><br></div>
    <div class="form-group">
      {{ Form::label('labelFechado', 'Periodos:') }}
      {{ Form::select('fechado', $fechado) }}        

      <div class="clear"><br></div>

      {{ Form::submit('Criar arquivo de exportação', array('class' => 'btn btn-info')) }}

    </div>
  </div>
  {{ Form::close() }}     
  
</div>  

@if ($errors->any())
<ul>
  {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</ul>
@endif

@else
  @include('accessdenied')
@endif

@stop


