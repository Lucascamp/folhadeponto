@extends('layouts.scaffold')

@section('main')

<h1>Guias Operacionais</h1>

<p>{{ link_to_route('guias.create', 'Adicionar Nova', array(), array('class'=> 'btn btn-info')) }}</p>

<br>

@if ($guias->count())
<table class="table table-striped table-bordered">
	<thead>
		<tr>			
			<th>Projeto</th>
			<th>Localização</th>
			<th>Equipe</th>
			<th>Inspetor</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($guias as $guia)
		<tr>
			<td>{{{ $guia->projeto }}}</td>
			<td>{{{ $guia->proj_localizacao }}}</td>
			<td>
				@foreach($guia->funcionarios as $funcionario)
				{{{ $funcionario->nome }}} <br>
				@endforeach
			</td>
			<td>{{{ $guia->inspetor->nome }}}</td>

			<td>{{ link_to_route('guias.edit', 'Edit', array($guia->cod), array('class' => 'btn btn-info')) }}</td>
			<td>
				{{ Form::open(array('method' => 'DELETE', 'route' => array('guias.destroy', $guia->cod))) }}
				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@else
Não existem Guias Operacionais Cadastradas.
@endif

@stop
