@extends('layouts.scaffold')

@section('main')

<h1>Editar Guia Operacional</h1>
<br>
{{ Form::model($guia, array('method' => 'PATCH', 'route' => array('guias.update', $guia->cod))) }}
<ul>
    <li class="form-control">
        {{ Form::label('projeto', 'Projeto:') }}
        {{ Form::text('projeto') }}
    </li>

    <li class="form-control">
        {{ Form::label('proj_localizacao', 'Localização:') }}
        {{ Form::text('proj_localizacao') }}
    </li>

    <div class="clear"></div>

    <li class="form-control">
        {{ Form::label('equipe', 'Selecione a equipe:') }}
        {{ Form::select('equipe[]', $funcionarios, $funcionarios_selected,  array('class' => 'chosen-select', 'multiple')) }}
    </li>    
    
    <li class="form-control">
        {{ Form::label('equi_inspn2_mat', 'Selecione o Inspetor do projeto:') }}
        {{ Form::select('equi_inspn2_mat', array_merge(array(''), $funcionarios), $guia->inspetor->cod, array('class' => 'chosen-select')) }}
    </li>

</ul>

<div class="clear"><br></div>

<div class="form-control">
    {{ Form::submit('Update', array('class' => 'btn btn-info')) }}

    {{ link_to_route('guias.show', 'Cancel', $guia->cod, array('class' => 'btn')) }}
</div>

{{ Form::close() }}

@if ($errors->any())
<ul>{{ implode('', $errors->all('<li class="error">:message</li>')) }}</ul>
@endif

@stop