@extends('layouts.scaffold')

@section('main')

<h1>Exibir Guia Operacional</h1>
<br>
<p>{{ link_to_route('guias.index', 'Guias Operacionais', array(), array('class'=> 'btn btn-info')) }}</p>
<div class="clear"><br></div>

<table class="table table-striped table-bordered">
	<thead>
		<tr>			
			<th>Projeto</th>
			<th>Localização</th>
			<th>Equipe</th>
			<th>Inspetor</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $guia->projeto }}}</td>
			<td>{{{ $guia->proj_localizacao }}}</td>
			<td>
				@foreach($guia->funcionarios as $funcionario)
				{{{ $funcionario->nome }}} <br>
				@endforeach
			</td>						
			<td>{{{ $guia->inspetor->nome }}}</td>

			<td>{{ link_to_route('guias.edit', 'Edit', array($guia->cod), array('class' => 'btn btn-info')) }}</td>
			<td>
				{{ Form::open(array('method' => 'DELETE', 'route' => array('guias.destroy', $guia->cod))) }}
				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
				{{ Form::close() }}
			</td>
		</tr>
	</tbody>
</table>

@stop
