@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('validar_menu'))

<h1>Banco de Horas</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => array('horarios.validar'), 'method' => 'post')) }}

<div class="clear"><br></div>

<div class="col-md-6 col-md-offset-3">
	<div class="table-responsive panel panel-default">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="60%">Funcionário</th>
					<th width="20%">Saldo Atual</th>
					<th width="20%" colspan="3">Ações</th>
				</tr>
			</thead>
			<tbody>

				@if ($funcionarios)

				@foreach ($funcionarios as $key => $funcionario)

				<tr>
					<td>{{{ $funcionario }}}</td>
					<td>saldo</td>
					<td>
						<button type="button" data-target="#addBanco" data-toggle="modal" data-tooltip="tooltip" class="btn btn-success glyphicon glyphicon-plus addBancoBtn" style="width: 41px; height:34px;" title="Crédito"
		       				data_value="{{ $key }}" data_value2="{{ $funcionario }}"></button>
		       		</td>
					<td>
						<button type="button" data-target="#subBanco" data-toggle="modal" data-tooltip="tooltip" class="btn btn-danger glyphicon glyphicon-minus subBancoBtn" style="width: 41px; height:34px;" title="Débito"
		       				data_value="{{ $key }}" data_value2="{{ $funcionario }}"></button>
					</td>
				</tr>

				@endforeach

				@else
				<tr> 
					<td colspan="14" class="center">
						Nenhum funcionario a ser validado.
					</td>
				</tr>
				@endif


			</tbody>
		</table>
	</div>	
</div>
<div class="clear"><br></div>

{{ Form::close() }}

@include('banco._partials.addBanco', $funcionarios) 
@include('banco._partials.subBanco', $funcionarios) 

@else
	@include('accessdenied')
@endif

@stop
