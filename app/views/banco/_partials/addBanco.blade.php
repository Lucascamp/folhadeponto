<div class="modal fade" id="addBanco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-success">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Crédito</font></h4>
      </div>
      <div class="modal-body" align="center">
        
        {{ Form::open(array('route' => 'banco.store', 'class'=>'form-inline')) }}
            
        {{ Form::hidden('created_by',  Auth::user()->cod); }} 
        {{ Form::hidden('add_to_funcionario', '', array('id' => 'add_to_funcionario')); }}

        {{ Form::label('Tempo', 'Tempo:') }}
        {{ Form::text('credito', null, array('class'=>'form-control mask-time', 'style' => 'width:100%')) }}

      </div>
      <br>
      <div class="modal-footer" align="center">
        {{ Form::submit('Salvar', array('class' => 'btn btn-success')) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

