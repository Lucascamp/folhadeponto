<div class="modal fade" id="subBanco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
        <h4 class="modal-title" id="myModalLabel"><font color='#ffffff'>Débito</font></h4>
      </div>
      <div class="modal-body" align="center">
        
        {{ Form::open(array('route' => 'banco.store', 'class'=>'form-inline')) }}
            
        {{ Form::hidden('created_by',  Auth::user()->cod); }} 
        {{ Form::hidden('sub_from_funcionario', '', array('id' => 'sub_from_funcionario')); }}

        {{ Form::label('Tempo', 'Tempo:') }} 
        {{ Form::text('debito', null, array('class'=>'form-control mask-time border', 'style' => 'width:100%;')) }}

      </div>
      <br>
      <div class="modal-footer" align="center">
        {{ Form::submit('Salvar', array('class' => 'btn btn-danger')) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

