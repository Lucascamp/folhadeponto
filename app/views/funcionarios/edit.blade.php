@extends('layouts.scaffold')

@section('main')

<h1>Edit Funcionario</h1>
{{ Form::model($funcionario, array('method' => 'PATCH', 'route' => array('funcionarios.update', $funcionario->cod))) }}
	<ul>
        <li>
            {{ Form::label('matricula', 'Matricula:') }}
            {{ Form::text('matricula') }}
        </li>

        <li>
            {{ Form::label('nome', 'Nome:') }}
            {{ Form::text('nome') }}
        </li>

        <li>
            {{ Form::label('funcao', 'Funcao:') }}
            {{ Form::text('funcao') }}
        </li>

        <li>
            {{ Form::label('mope', 'Mope:') }}
            {{ Form::text('mope') }}
        </li>

        <li>
            {{ Form::label('asnt', 'Asnt:') }}
            {{ Form::text('asnt') }}
        </li>

        <li>
            {{ Form::label('cnen', 'Cnen:') }}
            {{ Form::text('cnen') }}
        </li>

        <li>
            {{ Form::label('ftbs', 'Ftbs:') }}
            {{ Form::text('ftbs') }}
        </li>

        <li>
            {{ Form::label('snqc', 'Snqc:') }}
            {{ Form::text('snqc') }}
        </li>

        <li>
            {{ Form::label('unidade', 'Unidade:') }}
            {{ Form::text('unidade') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('funcionarios.show', 'Cancel', $funcionario->cod, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
