@extends('layouts.scaffold')

@section('main')

<h1>Create Funcionario</h1>

{{ Form::open(array('route' => 'funcionarios.store')) }}
<ul>
    <li>
        {{ Form::label('matricula', '* Matricula:') }}
        {{ Form::text('matricula') }}
    </li>

    <li>
        {{ Form::label('nome', '* Nome:') }}
        {{ Form::text('nome') }}
    </li>

    <li>
        {{ Form::label('funcao', '* Funcao:') }}
        {{ Form::text('funcao') }}
    </li>

    <li>
        {{ Form::label('mope', '* MOPE:') }}
        {{ Form::text('mope') }}
    </li>

    <li>
        {{ Form::label('asnt', '* ASNT:') }}
        {{ Form::text('asnt') }}
    </li>

    <li>
        {{ Form::label('cnen', '* CNEN:') }}
        {{ Form::text('cnen') }}
    </li>

    <li>
        {{ Form::label('ftbs', '* FTBS:') }}
        {{ Form::text('ftbs') }}
    </li>

    <li>
        {{ Form::label('snqc', '* SNQC:') }}
        {{ Form::text('snqc') }}
    </li>

    <li>
        {{ Form::label('unidade', '* Unidade:') }}
        {{ Form::text('unidade') }}
    </li>    
</ul>


<div class="clear"><br></div>
{{ Form::submit('Submit', array('class' => 'btn btn-info f-right')) }}

{{ Form::close() }}

@if ($errors->any())
<ul>
  {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</ul>
@endif

@stop


