@extends('layouts.scaffold')

@section('main')

<h1>All Funcionarios</h1>

<p>{{ link_to_route('funcionarios.create', 'Add new funcionario') }}</p>

@if ($funcionarios->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Matricula</th>
				<th>Nome</th>
				<th>Funcao</th>
				<th>Mope</th>
				<th>Asnt</th>
				<th>Cnen</th>
				<th>Ftbs</th>
				<th>Snqc</th>
				<th>Unidade</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($funcionarios as $funcionario)
				<tr>
					<td>{{{ $funcionario->matricula }}}</td>
					<td>{{{ $funcionario->nome }}}</td>
					<td>{{{ $funcionario->funcao }}}</td>
					<td>{{{ $funcionario->mope }}}</td>
					<td>{{{ $funcionario->asnt }}}</td>
					<td>{{{ $funcionario->cnen }}}</td>
					<td>{{{ $funcionario->ftbs }}}</td>
					<td>{{{ $funcionario->snqc }}}</td>
					<td>{{{ $funcionario->unidade }}}</td>
                    <td>{{ link_to_route('funcionarios.edit', 'Edit', array($funcionario->cod), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('funcionarios.destroy', $funcionario->cod))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no funcionarios
@endif

@stop
