@extends('layouts.scaffold')

<?php use Carbon\Carbon; ?>

@section('main')

@if(User::checkPermission('validar_menu'))

<h1>Férias</h1>

<h2><font color="red">Dados providos pelo protheus após calculo da folha</font></h2>

{{ Form::open(array('route' => 'ferias', 'method' => 'get')) }}

<div class="row">
	
	<div class="col-md-5">
		<div class="form-group">
			{{ Form::select('filter_funcionario', $funcionario_drop, $funcionario_filter) }}
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			{{ Form::select('filter_cc', $cc_drop , $cc_filter) }}
		</div>
	</div>

		<div class="form-group">
			{{ Form::submit('Filtrar', array('class' => 'btn btn-success')) }}
		</div>
</div>

{{ Form::close() }}

<div class="clear"><br></div>

<div class="table-responsive panel panel-default">
    <table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Funcionário</th>
			<th>Centro de custo</th>
			<th>Férias atuais</th>			
			<th>Férias vencidas</th>
			<th colspan="2">Período Aquisitivo</th>
			<th>Data Limite Maxima</th>
		</tr>
	</thead>
	<tbody>

		@foreach ($ferias_atual as $ferias)
		<tr>
			<td>{{{ $ferias->funcionario->nome }}}</td>
			<td>{{{ $cc[$ferias->funcionario->centro_custo] }}}</td>
			<td>{{{ $ferias->ferias }}}</td>
			<td>{{{ $ferias->ferias_vencidas }}}</td>

			<td>{{{ Carbon::createFromFormat('Y-m-d', $ferias->data_inicial)->format('d/m/Y') }}}</td>
			<td>{{{ Carbon::createFromFormat('Y-m-d', $ferias->data_final)->format('d/m/Y') }}}</td>
			<td>{{{ Carbon::createFromFormat('Y-m-d', $ferias->data_ideal)->format('d/m/Y') }}}</td>
		</tr>

		@endforeach
		
		</tbody>
	</table>
</div>	
	<div class="clear"><br></div>
	
	{{ Form::close() }}

	@else
        @include('accessdenied')
    @endif

	@stop
