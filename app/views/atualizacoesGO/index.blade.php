@extends('layouts.scaffold')

<?php use Carbon\Carbon; ?>

@section('main')

@if(User::checkPermission('buscar_menu'))

<?php
	 //seleciona periodo em aberto
		$periodoInicio = DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_inicio');
		$periodoFinal  = DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_fim');
		$periodoFinal  = Carbon::createFromFormat('Y-m-d H:i:s', $periodoFinal);
		$periodoFinal->addDay();
		//dia adicionado pra query abaixo pegar todo periodo pois o periodo é a data+'00:00:00'
		//os dados do ultimo dia tb devem ser criados

	    //Busca dados
		$rdo = DB::table('tb_rdo')
		//->join('tb_go', 'tb_rdo.cod_go', '=', 'tb_go.cod_go') join apenas aumenta quantidade de RDOS
		->whereNull('tb_rdo.deleted_at')
		->whereNotNull('tb_rdo.clienteentrada')
		->whereBetween('tb_rdo.clienteentrada', array($periodoInicio, $periodoFinal))
		->count();
?>
<h1>Atualizações Automáticas</h1>

<div class="clear"><br>
<table align="center">

	<tbody>
		<tr>
			<td>
			<div class="menu">
            	<ul>
            		<li>
            			<a class='mensagem_aguarde' href="atualizapontos">
           				<img src="images/ico_marcacoes_da_go.png" border="0"/>Criar marcações das RDO's</a>
           			</li>

           			<li>
           				<a class='mensagem_aguarde' href="atualizatempoexposicao">
       					<img src="images/ico_exposicao_da_go.png" border="0"/>Atualizar tempos de exposição</a>
       				</li>
	           	</ul>
			</div>
			</td>	
			<td>
			
			</td>		
		</tr>
		<tr>
		<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<b> Quantidade de RDO's para criação de marcações: {{{ $rdo }}} </b>
			</td>	
		</tr>
		
	</tbody>
</table>

<div id="screen"></div>
<div id="dvLoading"></div>
<div id="dvMessage"><center><font size='6' color='white'>Atualizando o sistema</font></center></div>


	@else
        @include('accessdenied')
    @endif

@stop
