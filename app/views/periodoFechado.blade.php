@extends('layouts.scaffold')

@section('main')

<h1 align='center' color='red'>Período Fechado</h1>
<br>
<h3 align='center'>Não existem períodos em aberto. Entre em contato com RH para verificar o motivo do período estar fechado</h3>
<br>
	 <center>
	 	{{ link_to('/', 'Retornar ao Início', array('class' => 'btn btn-success')) }}
	 </center>

@stop