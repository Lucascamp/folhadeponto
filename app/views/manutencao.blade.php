@extends('layouts.scaffold')

@section('main')

<h1 align='center' color='red'>Em manutenção</h1>
<br>
<h3 align='center'>
Estamos terminando de atribuir aos funcionários a pessoa que irá validar
<br>
Data de retorno: 06/05/2015 às 11 Horas</h3>
<br>
	 <center>
	 	{{ link_to('/', 'Retornar ao Início', array('class' => 'btn btn-success')) }}
	 </center>
@stop