@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('folha_menu'))

<h1 align='center'>existem inconsistências para este funcionário</h1>

<br>

<div class="clear"><br>
<div class="clear"><br>
<table class="zebra2" align="center">
	<thead>
		<tr>
			<th>Nome</th>		
			<th>Verificar Inconsistências</th>	
		</tr>
	</thead>
	<tbody>

		<tr>
		<td>
		{{ $data['funcionario']->nome }}
		</td>	
		<td>
		{{ link_to_route('horarios.inconsistencia', 'Ir para Inconsistências', array('filter_funcionario='.$data['funcionario']->cod.'&filter_item_contab=0&filter_dia=0'), array('class' => 'btn btn-info') ) }}
		</td>		
		</tr>
		
	</tbody>
</table>
<div class="clear"><br>
<div class="clear"><br></div>
	
	@else
        @include('accessdenied')
    @endif

@stop