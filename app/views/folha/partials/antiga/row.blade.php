<?php use Carbon\Carbon; ?>
<tr>
	<td>{{{ $horario->unidade }}}</td>
	<td>{{{ $horario->data->format('d/m/Y') }}}</td>
	
	@if((strstr($horario->atividade, 'Falta')) || (strstr($horario->atividade, 'Folga')) ||($horario->atividade == 'Férias') || (strstr($horario->atividade, 'A disposição do cliente')) || (strstr($horario->atividade, 'A disposição')) || (strstr($horario->atividade, 'Atestado')) || (strstr($horario->atividade, 'Exame Médico')) || (strstr($horario->atividade, 'Afastado')) || (strstr($horario->atividade, 'A Disposição do cliente')) || (strstr($horario->atividade, 'Treinamento'))) 
	
		<td>-</td>	
		<td>-</td>
		<td>-</td>
		<td>-</td>	

	@if(($horario->atividade == 'A disposição do cliente') || ($horario->atividade == 'A disposição') || ($horario->atividade == 'Atestado') || ($horario->atividade == 'Exame Médico') || ($horario->atividade == 'A Disposição do cliente')||($horario->atividade == 'Treinamento'))

		<td>{{{ Horario::addTotalHour($horario->getWorkedHours(), 'worked_hour_'.$type)->format('H:i') }}}</td>
		<td>{{{ Horario::addTotalHour($horario->getExtraHours(), 'extra_hour_'.$type)->format('H:i') }}}</td>
		<td>{{{ Horario::addTotalHour($horario->getNightHours(), 'night_hour_'.$type)->format('H:i') }}}</td>

		@if(($data['funcionario']->matricula_qt == null)&&($data['funcionario']->matricula_a5 != null))
			<td>-</td>
		@endif
		
		@if($data['type'] == 'qt')
				@if($horario->tempo_exposicao != null)
					<td><?php //Horario::addTotalHour($horario->exposedHours(), 'exposed_hour_'.$type); ?>
					{{{ Horario::addTotalHour(Carbon::createFromFormat('H:i:s', $horario->tempo_exposicao), 'exposed_hour_'.$type)->format('H:i:s') }}}</td>
					@else
					<td>-</td>	
				@endif
		@endif	

	@else
		<td>-</td>
		<td>-</td>
		<td>-</td>

		@if(($data['funcionario']->matricula_qt == null)&&($data['funcionario']->matricula_a5 != null))
			<td>-</td>
		@endif

		@if($data['type'] == 'qt')
			<td>-</td>
		@endif	
	@endif

	@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
			
	@else
		<td>-</td>
		<td>-</td>
		@if(($data['funcionario']->matricula_qt == null)&&($data['funcionario']->matricula_a5 != null))
			
		@else
			<td>-</td>
		@endif
	@endif
	
	@elseif(($type == 'qt') && (strpos($horario->atividade, '/feriasQT')))

	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>

	@elseif(($type == 'a5') && (strpos($horario->atividade, '/feriasA5')))

	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
		@if(($data['funcionario']->matricula_qt == null)&&($data['funcionario']->matricula_a5 != null))	
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif

	@else

	<td>{{{ $horario->hora_entrada->format('H:i') }}}</td>
	
	@if(($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null)&&(($horario->data > $horario->funcionario->data_demissao_qt || $horario->data < $horario->funcionario->data_demissao_a5)))	
		<td>00:00</td>
	@else
		<td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
	@endif

	@if(($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null)&&(($horario->data > $horario->funcionario->data_demissao_qt || $horario->data < $horario->funcionario->data_demissao_a5)))	
		<td>00:00</td>
	@else
		<td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>
	@endif

	<td>{{{ $horario->hora_saida->format('H:i') }}}</td>
	<td>{{{ Horario::addTotalHour($horario->getWorkedHours(), 'worked_hour_'.$type)->format('H:i') }}}</td>
	<td>{{{ Horario::addTotalHour($horario->getExtraHours(), 'extra_hour_'.$type)->format('H:i') }}}</td>
	<td>{{{ Horario::addTotalHour($horario->getNightHours(), 'night_hour_'.$type)->format('H:i') }}}</td>
		@if(($data['funcionario']->matricula_qt == null)&&($data['funcionario']->matricula_a5 != null))
			
		@else
			@if($data['type'] == 'qt')
				@if($horario->tempo_exposicao != null)
				<td><?php //Horario::addTotalHour($horario->exposedHours(), 'exposed_hour_'.$type); ?>
				{{{ Horario::addTotalHour(Carbon::createFromFormat('H:i:s', $horario->tempo_exposicao), 'exposed_hour_'.$type)->format('H:i:s') }}}</td>
				@else
				<td>-</td>	
				@endif
			@endif
		@endif
	
	<?php $beneficio = explode(',', $horario->beneficios); ?>
		
		@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
			<!--  nao imprimi beneficios se tiver as duas folhas -->
	 	@else
			@include('folha.partials.antiga.content_benefits', array('horarios'=>$horarios))
		@endif

	@endif

	@if($type == 'qt')
		
		@if(strpos($horario->atividade, '/feriasQT'))
			<td>Férias</td>

		@elseif(strpos($horario->atividade, '/feriasQT'))	
			<td>{{{ substr($horario->atividade, 0, strpos($horario->atividade, '/feriasQT'))}}}</td>

		@elseif(strpos($horario->atividade, '/feriasA5'))	
			<td>{{{ substr($horario->atividade, 0, strpos($horario->atividade, '/feriasA5'))}}}</td>	

		@else
			<td>{{{ $horario->atividade, Horario::addActivityDay($horario->atividade, 'Atividade_'.$type) }}}</td>
		@endif	

	@elseif($type == 'a5')

		@if(strpos($horario->atividade, '/feriasA5'))
			<td>Férias</td>

		@elseif(($type == 'a5') && (strpos($horario->atividade, '/feriasA5')))	
			<td>{{{ substr($horario->atividade, 0, strpos($horario->atividade, '/feriasA5'))}}}</td>

		@elseif(($type == 'a5') && (strpos($horario->atividade, '/feriasQT')))	
			<td>{{{ substr($horario->atividade, 0, strpos($horario->atividade, '/feriasQT'))}}}</td>	

		@else
			<td>{{{ $horario->atividade, Horario::addActivityDay($horario->atividade, 'Atividade_'.$type) }}}</td>
		@endif

	@endif
	
	<td>{{{ substr($horario->item_contabil->CTD_DESC01, 0, 22) }}}</td>
</tr>