<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
            <th width='4%'>Unidade</th>
            <th width='5%'>Data</th>
            <th width='5%'>Entrada</th>             
            <th width='5%'>Intervalo</th>
            <th width='5%'>Intervalo</th>
            <th width='5%'>Saída</th>
            <th width='9%'>Hora Trabalhada</th>
            <th width='8%'>Hora Extra</th>
            <th width='10%'>Adicional Noturno</th>
            @if($data['type'] == 'a5')
           
            <!--  nao imprimi horas de serviço na folha a5 -->
            
            @else

            <th width='6%'>TFE</th>
            
            @endif
            
            @if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
           <!--  nao imprimi beneficios se tiver as duas folhas -->
            @else

            <th width='3%'>VA/VR</th>
            <th width='3%'>VT</th>
            <th width='3%'>AM</th>

            @endif

            <th width='12%'>Atividade</th>
            <th width='25%'>Centro de Custo</th>
        </tr>
    </thead>

    <tbody>

    <?php         
        $horarios = $horarios->getIterator();
        $currentData = clone $datainicio;    
    ?>

        @foreach (range(1, $periodo) as $index)

            @if($horarios->count())

                <?php $horario = $horarios->current(); ?>

                @if(isset($horario->funcionario->data_demissao_qt) && $horario->data > $horario->funcionario->data_demissao_qt && $data['type'] == 'qt')
                    @include('folha.partials.row_fake', array('current'=> $currentData, 'horario'=> $horario))

                @elseif(isset($horario->funcionario->data_demissao_a5) != null && $horario->data > $horario->funcionario->data_demissao_a5 && $data['type'] == 'a5')
                    @include('folha.partials.row_fake', array('current'=> $currentData, 'horario'=> $horario))
    
                @elseif($horario instanceof Horario && $currentData->format('Y-m-d') == $horario->data->format('Y-m-d'))
                    @include('folha.partials.row', array('horario'=> $horario, 'type' => $type))
                    <?php $horarios->next(); ?>
                @else
                    @include('folha.partials.row_fake', array('current'=> $currentData, 'horario'=> $horario))
                @endif

                <?php $currentData->addDay(); ?>
            @else
               @include('folha.partials.row_fake', array('current'=> $currentData))
               <?php $currentData->addDay(); ?>
            @endif
        @endforeach        
    </tbody>
</table>