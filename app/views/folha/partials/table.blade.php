<?php 
	$funcao_attr = 'funcao_'.$type;
	$matricula_attr = 'matricula_'.$type;
?>


<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width="520">
		<img border="0" src="images/img_logo_ponto_{{ $type}}.png" width="130" height="40"></td>
			<td width="500"><b><font size="5" face="Arial">FOLHA DE PONTO</font></b></td>
			<td>Período: de {{{$datainicio->format('d/m/Y')}}} a {{{$datafinal->format('d/m/Y')}}}</td>
		</tr>
		<tr>
			<td width="441">Nome: {{ $funcionario->nome }}</td>
			<td width="441">Função: {{ $funcionario->$funcao_attr }}</td>
			<td width="365">Matrícula: {{ $funcionario->$matricula_attr }}</td>
		</tr>
	</table>

	<table width="100%" height="8"><tr><td></td></tr></table>

	@if ($horarios)
		@include('folha.partials.table_content', array('horarios'=>$horarios,'type'=>$type,'periodo'=>$periodo))
	@else
	<h2>Nenhuma marcação encontrada.</h2>
	@endif

	<table border="2" width="100%" bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td rowspan="2" align=center width="3%">Totais</td>
		<td width="5%" align=center>Hora Direta</td>
		<td width="5%" align=center>Hora Extra</td>
		<td width="8%" align=center>Adicional Noturno</td>
		@if($data['type'] == 'qt')
			<td width="5%" align=center>TFE</td>
		@endif
		
		<td width="3%" align=center>Falta</td>
		<td width="3%" align=center>Atestado</td>
		<td width="3%" align=center>Folga</td>

		@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
		<!--  nao imprimi beneficios se tiver as duas folhas -->
		@else
			<td width="3%" align=center>VA/VR</td>
			<td width="3%" align=center>VT</td>
			<td width="3%" align=center>AM</td>
		@endif
				
		<td rowspan="2" valign="top" width="18%">Observações:</td>
	</tr>
	<tr>
		<td width="84" align=center><br>{{Horario::getTotalHour('worked_hour_'.$type)}}</td>
		<td width="73" align=center><br>{{Horario::getTotalHour('extra_hour_'.$type)}}</td>
		<td width="86" align=center><br>{{ number_format((Horario::getTotalHour('night_hour_'.$type)*1.14), 2) }}</td>
		@if($data['type'] == 'a5')
			
		@else
		<td width="86" align=center><br>{{Horario::getTotalHour('exposed_hour_'.$type)}}</td>
		@endif
		
		<td width="80" align=center><br>{{Horario::getTotalActivity('Falta', 'Atividade_'.$type)}}</td>
		<td width="62" align=center><br>{{Horario::getTotalActivity('Atestado', 'Atividade_'.$type)}}</td>
		<td width="91" align=center><br>{{Horario::getTotalActivity('Folga', 'Atividade_'.$type)}}</td>

		@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
		<!--  nao imprimi beneficios se tiver as duas folhas -->
		@else

			<td width="60" align=center><br>{{Horario::getTotalActivity('va', 'Atividade')}}</td>
			<td width="60" align=center><br>{{Horario::getTotalActivity('vt', 'Atividade')}}</td>
			<td width="60" align=center><br>{{Horario::getTotalActivity('am', 'Atividade')}}</td>

		@endif
	</tr>
</table>

<table width="100%" border='1' bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td>Assinatura Funcionário</td>
		<td>Assinatura Supervisor</td>
		<td>Assinatura Gerente</td>
	</tr>
	<tr>
		<td width="25%"><br><br>
		</td>
		<td width="25%"><br><br>
		</td>
		<td width="25%"><br><br>
		</td>
	</tr>
	</table>

<p align='left' style='font-size: 8pt'>Legenda: VA/VR: Vale Alimentação ou Vale Refeição | VT: Vale Transporte | AM: Auxílio Moradia 
@if($data['type'] == 'qt')| TFE: Tempo de Fonte Exposta @endif </p>

