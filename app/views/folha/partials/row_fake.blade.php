<tr>
	<td>-</td>
	<td>{{{ $current->format('d/m/Y')}}}</td>
	<td>-</td>	
	<td>-</td>
	<td>-</td>	
	<td>-</td>
	<td>-</td>
	<td>-</td>
	<td>-</td>
	@if($data['type'] == 'a5')
     <!--  nao imprimi horas de serviço na folha a5 -->       
    @else
    <td>-</td>
	@endif
	
	@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
	<!--  nao imprimi beneficios se tiver as duas folhas -->
	@else

		<td>-</td>
		<td>-</td>
		<td>-</td>
		
	@endif
	
	@if(isset($horario->funcionario->data_demissao_qt) || isset($horario->funcionario->data_demissao_a5))
		@if($current->format('Y-m-d') == $horario->funcionario->data_demissao_qt && ($data['type'] == 'qt'))
			<td>Demitido</td>

		@elseif($current->format('Y-m-d') == $horario->funcionario->data_demissao_a5 && ($data['type'] == 'a5')) 
			<td>Demitido</td>

		@else
			<td>-</td>	

		@endif
		
		@else
			<td>-</td>
	
	@endif
	<td>-</td>
</tr>