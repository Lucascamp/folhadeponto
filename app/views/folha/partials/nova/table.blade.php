<?php 
	$funcao_attr = 'funcao_'.$type;
	$matricula_attr = 'matricula_'.$type;
?>

<table width="100%" height="73" class='tablefolha'>
	<tr>
		<td width="520">
		<img border="0" src="images/img_logo_ponto_{{ $type}}.png" width="130" height="40"></td>
			<td width="500"><b><font size="5" face="Arial">FOLHA DE PONTO</font></b></td>
			<td>Período: de {{{$datainicio->format('d/m/Y')}}} a {{{$datafinal->format('d/m/Y')}}}</td>
		</tr>
		<tr>
			<td width="441">Nome: {{ $funcionario->nome }}</td>
			<td width="441">Função: {{ $funcionario->$funcao_attr }}</td>
			<td width="365">Matrícula: {{ $funcionario->$matricula_attr }}</td>
		</tr>
	</table>

	<table width="100%" height="8"><tr><td></td></tr></table>

	@if ($horarios)
		@include('folha.partials.nova.table_content', array('horarios'=>$horarios,'type'=>$type,'periodo'=>$periodo))
	@else
	<h2>Nenhuma marcação encontrada.</h2>
	@endif

	<table border="2" width="100%" bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		

		@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
			<td colspan="6" align=center>Total de horas</td>
			<td colspan="8" align=center>Atividades</td>

		@elseif(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt == null))
			<td colspan="5" align=center>Total de horas</td>
			<td colspan="5" align=center>Atividades</td>
			<td colspan="3" align=center>Beneficios</td>	
			
		@else
			<td colspan="6" align=center>Total de horas</td>
			<td colspan="5" align=center>Atividades</td>
			<td colspan="3" align=center>Beneficios</td>

		@endif
				
	</tr>
	<tr>
		<td width="5%" align=center>Diretas</td>
		@if($data['funcionario']->administrativo == 1)
			<td width="5%" align=center>Banco de horas</td>
		@else	
			<td width="5%" align=center>Extras</td>
		@endif	
		<td width="5%" align=center>Noturnas</td>
		<td width="5%" align=center>Trabalhadas</td>
		<td width="5%" align=center>Disposição</td>
		@if($data['type'] == 'qt')
			<td width="5%" align=center>TFE</td>
		@endif
		
		<td width="3%" align=center>Falta</td>
		<td width="3%" align=center>Atestado</td>
		<td width="3%" align=center>Folga</td>
		<td width="3%" align=center>A Disposição</td>
		<td width="3%" align=center>Disp. Cliente</td>

		@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
		<!--  nao imprimi beneficios se tiver as duas folhas -->
		@else
			<td width="3%" align=center>VA/VR</td>
			<td width="3%" align=center>VT</td>
			<td width="3%" align=center>AM</td>
		@endif
				
		
	</tr>
	<tr>
		<td align=center><br>{{Horario::getTotalHour('worked_hour_'.$type)}}</td>
		<td align=center><br>{{Horario::getTotalHour('extra_hour_'.$type)}}</td>
		<td align=center><br>{{ number_format((Horario::getTotalHour('night_hour_'.$type)*1.14), 2) }}</td>
		<td align=center><br>{{Horario::getTotalHour('client_hour_'.$type)}}</td>
		<td align=center><br>{{Horario::getTotalHour('disposicao_hour_'.$type)}}</td>

		@if($data['type'] == 'a5')
			
		@else
		<td align=center><br>{{Horario::getTotalHour('exposed_hour_'.$type)}}</td>
		@endif
		
		<td align=center><br>{{Horario::getTotalActivity('Falta', 'Atividade_'.$type)}}</td>
		<td align=center><br>{{Horario::getTotalActivity('Atestado', 'Atividade_'.$type)}}</td>
		<td align=center><br>{{Horario::getTotalActivity('Folga', 'Atividade_'.$type)}}</td>
		<td align=center><br>{{Horario::getTotalActivity('A Disposição', 'Atividade_'.$type)}}</td>
		<td align=center><br>{{Horario::getTotalActivity('A Disposição do cliente', 'Atividade_'.$type)}}</td>

		@if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
		<!--  nao imprimi beneficios se tiver as duas folhas -->
		@else

			<td align=center><br>{{Horario::getTotalActivity('va', 'Atividade')}}</td>
			<td align=center><br>{{Horario::getTotalActivity('vt', 'Atividade')}}</td>
			<td align=center><br>{{Horario::getTotalActivity('am', 'Atividade')}}</td>

		@endif
	</tr>
</table>

<table width="100%" border='1' bordercolor="C8C8C8" class='tablefolha'>
	<tr>
		<td width="40%">Assinatura Funcionário</td>
		<td width="40%">Assinatura Supervisor</td>
		<td width="20%"rowspan="2" valign="top" width="10%">Observações:</td>
	</tr>
	<tr>
		<td><br><br></td>
		<td><br><br></td>
	</tr>
	</table>

<p align='left' style='font-size: 8pt'>Legenda: VA/VR: Vale Alimentação ou Vale Refeição | VT: Vale Transporte | AM: Auxílio Moradia | UN: Unidade
@if($data['type'] == 'qt')| TFE: Tempo de Fonte Exposta @endif </p>

