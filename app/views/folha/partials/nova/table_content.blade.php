<table width='100%' class="zebra3" align="center">
    <thead>
        <tr>
            <th rowspan = '2' width='2%'>UN</th>
            <th rowspan = '2' width='5%'>Data</th>
            <th colspan = '4' width='25%'>Horas da jornada</th>
            <th colspan = '5' width='25%'>Horas Totais da diária</th>
            
            @if($data['type'] == 'qt')
                <th rowspan = '2' width='4%'>TFE</th>
            
            @endif
            
            @if(($data['type'] == 'a5')&&($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
           <!--  nao imprimi beneficios se tiver as duas folhas -->
            @else

            <th rowspan = '2' width='3%'>VA/VR</th>
            <th rowspan = '2' width='3%'>VT</th>
            <th rowspan = '2' width='3%'>AM</th>

            @endif

            <th rowspan = '2' width='13%'>Atividade</th>
            <th rowspan = '2' width='25%'>Centro de Custo</th>
        </tr>
        <tr>
            <th width='5%'>Entrada</th>             
            <th width='5%'>Intervalo</th>
            <th width='5%'>Intervalo</th>
            <th width='5%'>Saída</th>
            @if($data['funcionario']->administrativo == 1)
                <th width='5%'>Banco</th>
            @else
                <th width='5%'>Extras</th>
            @endif    
            <th width='5%'>Noturnas</th>
            <th width='5%'>Total</th>
            <th width='5%'>Trabalhadas</th>
            <th width='5%'>Disposição</th>
        </tr>
    </thead>

    <tbody>

    <?php         
        $horarios = $horarios->getIterator();
        $currentData = clone $datainicio;    
    ?>

        @foreach (range(1, $periodo) as $index)

            @if($horarios->count())

                <?php $horario = $horarios->current(); ?>

                @if(isset($horario->funcionario->data_demissao_qt) && $horario->data > $horario->funcionario->data_demissao_qt && $data['type'] == 'qt')
                    @include('folha.partials.nova.row_fake', array('current'=> $currentData, 'horario'=> $horario))

                @elseif(isset($horario->funcionario->data_demissao_a5) != null && $horario->data > $horario->funcionario->data_demissao_a5 && $data['type'] == 'a5')
                    @include('folha.partials.nova.row_fake', array('current'=> $currentData, 'horario'=> $horario))
    
                @elseif(($horario instanceof Horario) && ($currentData->format('Y-m-d') == $horario->data->format('Y-m-d')))
                    @include('folha.partials.nova.row', array('horario'=> $horario, 'type' => $type))
                    <?php $horarios->next(); ?>
                @else
                    @include('folha.partials.nova.row_fake', array('current'=> $currentData, 'horario'=> $horario))
                @endif

                <?php $currentData->addDay(); ?>
            @else
               @include('folha.partials.nova.row_fake', array('current'=> $currentData))
               <?php $currentData->addDay(); ?>
            @endif
        @endforeach        
    </tbody>
</table>