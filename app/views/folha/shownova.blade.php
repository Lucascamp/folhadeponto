@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('folha_menu'))
		
	<div class="area_impressao" name="area_impressao" id="area_impressao">
		@if($data['funcionario']->matricula_qt != null)

			<?php  $data['type'] = 'qt'; ?>

		@include('folha.partials.nova.table', $data)		

		@endif

		@if(($data['funcionario']->matricula_qt != null)&&($data['funcionario']->matricula_a5 != null))
			<div class="clear"><br></div>
			<div class="page-break"></div>
		@endif
		
		@if($data['funcionario']->matricula_a5 != null)

			<?php  $data['type'] = 'a5'; ?>

			{{ HorarioFolha::setType('a5') }} 

			@include('folha.partials.nova.table', $data) 

		@endif
		
	</div> 

	@else
        @include('accessdenied')
    @endif

@stop