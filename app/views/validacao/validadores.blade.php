@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('validar_menu'))

<h1>Validadores do ponto</h1>

<div class="clear"><br></div>

{{ Form::open(array('route' => array('horarios.validar'), 'method' => 'post')) }}

<div class="table-responsive panel panel-default">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Validador</th>
				<th>Tipo</th>
				<th>Funcionários</th>
				<th>Salvar</th>			
			</tr>
		</thead>
		<tbody>

		@foreach ($validadores as $validador)
			
			<tr>
				{{ Form::hidden('usuario', $validador->usuario_id, array('id'=>'usuario'.$validador->usuario_id)) }}
				<td>{{{ $usuarios[$validador->usuario_id] }}}</td>
				<td>{{{ $validador->tipo }}}</td>
				<td>{{ Form::select('funcionarios', $funcionarios, explode(',', $validador->funcionarios_permissao), array('multiple', 'id'=> 'funcionarios'.$validador->usuario_id)) }}</td>
				<td><button type="button" data-tooltip="tooltip" class="btn btn-success validadores" id="{{ $validador->usuario_id}}">Salvar Funcionários</button></td>
			</tr>

		@endforeach	

		</tbody>
	</table>
</div>	
{{ Form::close() }}

@else
@include('accessdenied')
@endif

@stop
