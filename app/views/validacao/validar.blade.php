@extends('layouts.scaffold')

@section('main')

@if(User::checkPermission('validar_menu'))

<h1>Validar marcações</h1>

<h2><font color="red">É necessário ser validado por dois usuários diferentes</font></h2>

<div class="clear"><br></div>

{{ Form::open(array('route' => 'horarios.validar', 'method' => 'get')) }}

<div class="row">
	
	<div class="col-md-3">
		<div class="form-group">
			{{ Form::select('filter_funcionario', $filter_funcionarios, $funcionario) }}
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			{{ Form::select('filter_item_contab', $filter_itens_contabil , $item_contabil) }}
		</div>
	</div>

	<div class="form-group col-md-2">
        {{ Form::text('filter_dia', null, array('class'=>'form-control datepicker', 'size' => '9', 'id'=>'data', 'placeholder'=>'Escolha a Data')) }}
    </div>

		<div class="form-group">
			{{ Form::submit('Filtrar', array('class' => 'btn btn-success')) }}
		</div>
</div>

{{ Form::close()}}

{{ Form::open(array('route' => array('horarios.validar'), 'method' => 'post')) }}

<div class="clear"></div>

{{ $horarios->appends(array('filter_funcionario' => Input::get('filter_funcionario'),
					  		'filter_item_contab' => Input::get('filter_item_contab'),
					  		'filter_dia' => Input::get('filter_dia')))->links() }}

<div class="table-responsive panel panel-default">
    <table class="table table-striped table-bordered" style="width: 1800px">
	<thead>
		<tr>
			<th>Funcionário</th>
			<th>Unidade</th>			
			<th>Data</th>
			<th>Item Contabil</th>
			<th>Hora entrada</th>				
			<th>Intervalo inicio</th>
			<th>Intervalo fim</th>
			<th>Hora saida</th>
			<th>Horas Totais</th>
			<th>Horas Extra</th>
			<th>Horas Noturnas</th>
			<th>Tempo Exposição</th>
			<th>Atividade</th>
			<th>Validação</th>
			<th>Motivo</th>
		</tr>
	</thead>
	<tbody>

		@if ($horarios->count())

		@foreach ($horarios as $horario)

		<tr>
			<td>{{{ $horario->funcionario->nome }}}</td>
			<td>{{{ $horario->unidade }}}</td>			
			<td>{{{ $horario->data->format('d/m/Y') }}}</td>
			<td>{{{ $horario->item_contabil->CTD_DESC01 }}}</td>
			<td>{{{ $horario->hora_entrada->format('H:i') }}}</td>				
			<td>{{{ $horario->intervalo_inicio->format('H:i') }}}</td>
			<td>{{{ $horario->intervalo_fim->format('H:i') }}}</td>
			<td>{{{ $horario->hora_saida->format('H:i') }}}</td>
			<td>{{{ $horario->getWorkedHours()->format('H:i') }}}</td>
			<td>{{{ $horario->getExtraHours()->format('H:i') }}}</td>
			<td>{{{ $horario->getNightHours()->format('H:i') }}}</td>
			<td>{{{ $horario->tempo_exposicao }}}</td>
			<td>{{{ $horario->atividade }}}</td>
			
			{{ Form::hidden("cod[{$horario->cod}]", $horario->cod) }}

        	@if($permissao_validacao->tipo == 'Coordenador')

				@if($horario->validado == 0)
					<td>
						<div id="divleft" align="left">
						{{ Form::hidden("cod1[{$horario->cod}]", $horario->cod) }}
						{{ Form::checkbox("cod1[{$horario->cod}]", $horario->cod, 1, array('id' => 'id1_'.$horario->cod, 'class' => 'validar1')) }}
						</div>
					</td>	
					<td width="200px">	
						<div id="div1_{{ $horario->cod }}" class="motivo" align="right">		
						
		        		{{ Form::text("motivo1[{$horario->cod}]", $horario->motivo, array('placeholder' => 'motivo')) }}
		        		</div>
		        	</td>
		        @else
		        	<td colspan="2"><font color='green'>Validado</td>
	        	@endif

		    @endif 	


        	@if($permissao_validacao->tipo == 'Gerente')
	        	
	        	@if($horario->validado2 == 0)
		        	<td>
						<div id="divleft" align="left">
						{{ Form::hidden("cod2[{$horario->cod}]", $horario->cod) }}
						{{ Form::checkbox("cod2[{$horario->cod}]", $horario->cod, 1, array('id' => 'id2_'.$horario->cod, 'class' => 'validar2')) }}
						</div>
					</td>	
					<td width="200px">	
						<div id="div2_{{ $horario->cod }}" class="motivo" align="right">		
						
		        		{{ Form::text("motivo2[{$horario->cod}]", $horario->motivo, array('placeholder' => 'motivo')) }}
		        		</div>
		        	</td>
		        @else
		        	<td colspan="2"><font color='green'>Validado</td>
	        	@endif

		    @endif     

		</tr>

			@endforeach
		
			@else
			<tr> 
				<td colspan="14" class="center">
					Nenhum horario a ser validado.
				</td>
			</tr>
			@endif

   
		</tbody>
	</table>
</div>	
	<div class="clear"><br></div>
	
	{{ $horarios->appends(array('filter_funcionario' => Input::get('filter_funcionario'),
					  			'filter_item_contab' => Input::get('filter_item_contab'),
					  			'filter_dia' => Input::get('filter_dia')))->links() }}
	
	<div class="clear"><br></div>
	
	<table align="center" class="zebra2">
		
			<tr>
				<th colspan="2">Ações</th>
			</tr>
		
		<tbody>
			<tr>
				<td>				
					{{ Form::submit('Validar todas as marcações', array('class' => 'btn btn-info')) }}
				</td>
			</tr>
		</tbody>
	</table>

	{{ Form::close() }}

	@else
        @include('accessdenied')
    @endif

	@stop
