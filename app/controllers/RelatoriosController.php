<?php

use Carbon\Carbon;

class RelatoriosController extends BaseController
{

    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;    


    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(Horario $horario, Funcionario $funcionario, ItemContabil $item_contabil, Relatorio $relatorio)
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
        $this->relatorio    = $relatorio;
    }

    public function administrativo()
    {   
        $funcionarios_cod = $this->funcionario->where('administrativo', 1)
                                                ->whereNull('data_demissao_qt')
                                                ->whereNull('data_demissao_a5')            
                                                ->lists('cod'); 
        
        set_time_limit(0);

        $periodoInicio = Carbon::createFromFormat('Y-m-d H:i:s', DB::table('tb_periodo')->where('fechado', 0)->min('periodo_inicio'));
        $periodoFinal  = Carbon::createFromFormat('Y-m-d H:i:s', DB::table('tb_periodo')->where('fechado', 0)->max('periodo_fim'));

        $dias = $periodoInicio->diffInDays($periodoFinal);

        foreach ($funcionarios_cod as $cod) 
        {
            $horarios[$cod] = $this->horario->with('funcionario')
                                 ->whereBetween('data', array($periodoInicio, $periodoFinal))
                                 ->where('cod_funcionario', $cod)
                                 ->whereNull('deleted_at')
                                 ->get();

            foreach ($horarios[$cod] as $horariofunc)
            {
                $checkHorario[$cod][$horariofunc->data->format('Y-m-d')] = true;
            }                     
        }

        $funcionarios = $this->funcionario->where('administrativo', 1)
                                            ->whereNull('data_demissao_qt')
                                            ->whereNull('data_demissao_a5')
                                            ->orderBy('nome')
                                            ->lists('nome','cod');                        
           
        $data = array(
            'dias' => $dias,
            'funcionarios'  => $funcionarios,
            'periodoInicio' => $periodoInicio,
            'diainicio'     => Carbon::createFromFormat('Y-m-d H:i:s', DB::table('tb_periodo')->where('fechado', 0)->min('periodo_inicio')),
            'periodoFinal'  => $periodoFinal,
            'horarios'      => $horarios,
            'checkHorario' => $checkHorario
            );

        return View::make('relatorios.admin', $data); 
    }

    public function indexGeral()
    {   
        $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');

        $funcionarios = $this->funcionario->whereNull('data_demissao_qt')
                                          ->whereNull('data_demissao_a5')
                                          ->orderBy('nome')
                                          ->lists('nome','cod');                    

        $data['todos'] = [0 =>'Selecione um período'] + $todos;

        $data['funcionarios'] = [0 =>'Selecione os funcionários'] + $funcionarios;
               
        return View::make('relatorios.geral', $data); 
    }

    public function relatorioGeral()
    {
        $input = Input::all();

        if(isset($input['todos'])&&($input['todos']!=0))
        {
            set_time_limit(0);
            $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');

            if(isset($input['funcionarios']))
                $cods_funcionarios = $input['funcionarios'];
            else
                $cods_funcionarios = $this->funcionario->whereNull('data_demissao_qt')
                                                  ->whereNull('data_demissao_a5')
                                                  ->orderBy('nome')
                                                  ->lists('cod');   

            $horarioQuery = $this->horario->with('funcionario')
                                 ->whereBetween('data', array($datainicio, $datafinal))
                                 ->whereIn('cod_funcionario', $cods_funcionarios)
                                 ->whereNull('deleted_at')
                                 ->get();

            $funcionarios = $this->funcionario->whereNull('data_demissao_qt')
                                              ->whereNull('data_demissao_a5')
                                              ->whereIn('cod', $cods_funcionarios)
                                              ->orderBy('nome')
                                              ->get();                     

            $results = reset($horarioQuery);

            if($results==null)
                return Redirect::route('relatorios.geral')->withErrors(['não existem marcações para este período']);
            
            $HNqt = $this->relatorio->relatorioHN($results, 'qt', $datainicio);
            $HEqt = $this->relatorio->relatorioHE($results, 'qt', $datainicio);
            $ADNqt = $this->relatorio->relatorioADN($results, 'qt', $datainicio);
            $HFqt = $this->relatorio->relatorioHF($results, 'qt', $datainicio);

            $HNa5 = $this->relatorio->relatorioHN($results, 'a5', $datainicio);
            $HEa5 = $this->relatorio->relatorioHE($results, 'a5', $datainicio);
            $ADNa5 = $this->relatorio->relatorioADN($results, 'a5', $datainicio); 
            $HFa5 = $this->relatorio->relatorioHF($results, 'a5', $datainicio);

            foreach($HNqt as $key=> $valor)
            {
                if(isset($HEqt[$key]))
                    $HNqt[$key] = floatval($HNqt[$key]-$HEqt[$key]);
            }
                
            foreach($HNa5 as $key=> $valor)
            {
                if(isset($HEa5[$key]))
                    $HNa5[$key] = floatval($HNa5[$key]-$HEa5[$key]);
            }
                
            $atividades = array('Falta', 'Folga', 'Atestado', 'A Disposição do cliente', 'A Disposição', 'vt', 'am', 'va');
            foreach($atividades as $atividade)
            {
                if($atividade=='A Disposição do cliente')
                    $ADC = $this->relatorio->reportActivity($results, $atividade);
                elseif($atividade=='A Disposição')
                    $AD = $this->relatorio->reportActivity($results, $atividade);
                else
                    $$atividade = $this->relatorio->reportActivity($results, $atividade);
            }

            


            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
            $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal);

            $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');

            return View::make('relatorios.geralshow', compact('funcionarios', 'datainicio', 'datafinal',
             'HEqt', 'HEa5', 'HNqt','HNa5', 'HFqt', 'HFa5','Falta', 'Folga', 'Atestado', 'ADC', 'AD', 'vt',
              'va', 'am', 'ADNqt', 'ADNa5', 'todos'));
        }
        else
        {
            return Redirect::route('relatorios.geral')->withErrors(['Selecione um período!']);
        }
    }

    public function indexObs()
    {   
        $funcionarios = $this->funcionario->lists('nome', 'cod');       
        $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');
           
        $data = array(
            'funcionarios' => (array( 0 =>'Selecione um funcionario') + $funcionarios),
            'todos'     => (array( 0 =>'Selecione um período') + $todos),
            );

        return View::make('relatorios.observacoes', $data); 
    }

    public function observacoesReport()
    {
        $input = Input::all();

        if((isset($input['todos'])&&($input['todos']!=0))&&(isset($input['funcionarios'])&&($input['funcionarios']!=0)))
        {
            set_time_limit(0);
            $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');
                        
            $horarioQuery = $this->horario->with('funcionario')
                                 ->whereBetween('data', array($datainicio, $datafinal))
                                 ->where('deleted_at', null)
                                 ->whereNotNull('observacoes')
                                 ->where('observacoes','<>', ' ')
                                 ->where('cod_funcionario', $input['funcionarios'])
                                 ->orderBy('data')
                                 ->get();

            $horarios = reset($horarioQuery);

            if($horarios==null)
                return Redirect::route('relatorios.observacoes')->withErrors(['não existem marcações para este período']);
    
            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
            $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal);

            return View::make('relatorios.obsshow', compact('datainicio', 'datafinal', 'horarios'));
        }
        elseif(isset($input['todos'])&&($input['todos']!=0))
        {
            $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');
                        
            $horarioQuery = $this->horario->with('funcionario')
                                 ->whereBetween('data', array($datainicio, $datafinal))
                                 ->where('deleted_at', null)
                                 ->whereNotNull('observacoes')
                                 ->where('observacoes','<>', ' ')
                                 ->orderBy('data')
                                 ->get();

            $horarios = reset($horarioQuery);

            if($horarios==null)
                  return Redirect::route('relatorios.observacoes')->withErrors(['não existem marcações para este período']);
    
            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
            $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal);

            return View::make('relatorios.obsshow', compact('datainicio', 'datafinal', 'horarios'));
        }
        elseif(isset($input['funcionarios'])&&($input['funcionarios']!=0))
        {
            $horarioQuery = $this->horario->with('funcionario')
                                 ->where('deleted_at', null)
                                 ->whereNotNull('observacoes')
                                 ->where('cod_funcionario', $input['funcionarios'])
                                 ->where('observacoes','<>', ' ')
                                 ->orderBy('data')
                                 ->get();

            $horarios = reset($horarioQuery);

            if($horarios==null)
                return Redirect::route('relatorios.observacoes')->withErrors(['não existem marcações para este período']);
    
            return View::make('relatorios.obsshow', compact('horarios'));
        }
        else
        {
            return Redirect::route('relatorios.observacoes')->withErrors(['Selecione um período!']);
        }
    }

    public function indexPendente()
    {   
        $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->where('fechado', 0)
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');

        $data = array('todos' => (array( 0 =>'Selecione um período') + $todos),);
               
        return View::make('relatorios.pendentes', $data); 
    }

    public function pendenteReport()
    {   
        $input = Input::all();

        set_time_limit(0);

        $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
        $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');

        $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
        $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal);
        
        $daysdiff = $datainicio->diffInDays($datafinal)+1;
       
        $horarioQuery = "SELECT cod_funcionario, count(cod) as soma
                         FROM `tb_horario` 
                         WHERE data between '".$datainicio."' AND '".$datafinal."'
                         AND deleted_at is null 
                         GROUP BY cod_funcionario;";

        $connection = DB::connection();     
        $horarios = $connection->select($horarioQuery);
        
        if($horarios==null)
            return Redirect::route('relatorios.geral')->withErrors(['não existem marcações para este período']);
             
        $funcionarios = $this->funcionario->orderBy('nome')->get();

        return View::make('relatorios.pendenteshow', compact('horarios', 'funcionarios', 'daysdiff', 'datainicio', 'datafinal'));
        
    }

    public function indexEmpresa()
    {   
        $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');

        $data = array('todos' => (array( 0 =>'Selecione um período') + $todos),);
               
        return View::make('relatorios.empresa', $data); 
    }


    public function relatorioEmpresa()
    {
        $input = Input::all();

        if(isset($input['todos'])&&($input['todos']!=0))
        {
            set_time_limit(0);
            $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');
             
            $horarioQuery = $this->horario->with('funcionario')
                                 ->whereBetween('data', array($datainicio, $datafinal))
                                 ->whereNull('deleted_at')
                                 ->get();

            $results = reset($horarioQuery);

            if($results==null)
                return Redirect::route('relatorios.empresa')->withErrors(['não existem marcações para este período']);
             
            $HNqt = $this->relatorio->relatorioHN($results, 'qt', $datainicio);
            $HNa5 = $this->relatorio->relatorioHN($results, 'a5', $datainicio);

            $HEqt = $this->relatorio->relatorioHE($results, 'qt', $datainicio);
            $HEa5 = $this->relatorio->relatorioHE($results, 'a5', $datainicio);

            $ADNqt = $this->relatorio->relatorioADN($results, 'qt', $datainicio);
            $ADNa5 = $this->relatorio->relatorioADN($results, 'a5', $datainicio); 
                
            $HFqt = $this->relatorio->relatorioHF($results, 'qt', $datainicio);
            $HFa5 = $this->relatorio->relatorioHF($results, 'a5', $datainicio);

            foreach($HNqt as $key=> $valor)
            {
                if(isset($HEqt[$key]))
                    $HNqt[$key] = floatval($HNqt[$key]-$HEqt[$key]);
            }

            foreach($HNa5 as $key=> $valor)
            {
                if(isset($HEa5[$key]))
                    $HNa5[$key] = floatval($HNa5[$key]-$HEa5[$key]);
            }
            
            $atividades = array('Falta', 'Folga', 'Atestado', 'A Disposição do cliente', 'A Disposição', 'vt', 'am', 'va');
            foreach($atividades as $atividade)
            {
                if($atividade=='A Disposição do cliente')
                    $ADC = $this->relatorio->reportActivity($results, $atividade);
                elseif($atividade=='A Disposição')
                    $AD = $this->relatorio->reportActivity($results, $atividade);
                else
                    $$atividade = $this->relatorio->reportActivity($results, $atividade);
            }

            $funcionarios = $this->funcionario
                                    ->whereNull('data_demissao_qt')
                                    ->whereNull('data_demissao_a5')
                                    ->orderBy('nome')
                                    ->get();

            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
            $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal);

            return View::make('relatorios.empresashow', compact('funcionarios', 'datainicio', 'datafinal',
             'HEqt', 'HEa5', 'HNqt','HNa5', 'HFqt', 'HFa5','Falta', 'Folga', 'Atestado', 'ADC', 'AD', 'vt', 'va', 'am', 'ADNqt', 'ADNa5'));
        }
        else
        {
            return Redirect::route('relatorios.empresa')->withErrors(['Selecione um período!']);
        }
    }

    public function indexMarcacoes()
    {   
        $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');

        $data = array('todos' => (array( 0 =>'Selecione um período') + $todos),);
               
        return View::make('relatorios.marcacoes', $data); 
    }

    public function marcacoesReport()
    {   
        $input = Input::all();

        set_time_limit(0);
        $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
        $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');

        $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
        $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal)->addDay();
       
        $horarioQuery = "SELECT cast(created_at as date) as data, count(cast(created_at as date)) as somatorio 
                         FROM `tb_horario` 
                         WHERE created_at between '".$datainicio."' AND '".$datafinal."' 
                         GROUP BY(cast(created_at as date));";

        $connection = DB::connection();     
        $horarios = $connection->select($horarioQuery);

        if($horarios==null)
            return Redirect::route('relatorios.geral')->withErrors(['não existem marcações para este período']);
           

        $datasJson = array();
        $somatorioJson = array();

        foreach ($horarios as $horario)
        {
            $horario->data = Carbon::createFromFormat('Y-m-d', $horario->data)->format('d/m/Y');
            array_push($datasJson, $horario->data);
            array_push($somatorioJson, $horario->somatorio);
        }

        $datafinal->subDay();
         
        $periodo = ($datafinal->diffInDays($datainicio))+1;
        
        $datasJson = json_encode($datasJson);
        $somatorioJson = json_encode($somatorioJson);

        return View::make('relatorios.marcacoesshow', compact('horarios', 'datainicio', 'datafinal','datasJson','somatorioJson','periodo'));
    }

    public function indexMarcacoes2()
    {   
        $periodos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio','desc')
                        ->lists('periodo','cod');

        $uns = DB::table('tb_sistema')
                        ->select(DB::raw('codlocalizacao, concat (codlocalizacao," - ",desclocalizacao) as un,cod'))
                        ->lists('un','codlocalizacao');

        $data = array(
            'periodos' => (array( 0 =>'Períodos') + $periodos),
            'uns' => (array( 0 =>'Todas') + $uns),
        );
               
        return View::make('relatorios.marcacoes2', $data); 
    }
    public function marcacoesgetdatas(){
        $input = Input::all();
        $datainicio = DB::table('tb_periodo')->where('cod', $input['periodo'])->pluck('periodo_inicio'); 
        $datafinal  = DB::table('tb_periodo')->where('cod', $input['periodo'])->pluck('periodo_fim');
        return Response::json(array('success'=>true,'datainicio'=>$datainicio,'datafim'=>$datafinal));  
    }
    public function marcacoesReport2()
    {   
        $input = Input::all();


        set_time_limit(0);
        $datainicio = $input['dtini']; 
        $datafinal  = $input['dtfim'];

        $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
        $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal)->addDay();
        

        
        if($input['un'] == '0'){
            $and = '';
        }else{
            $and = ' AND unidade = \''.$input['un'].'\' ';
        }

        $horarioQuery = 'SELECT cast(created_at as date) as data, count(cast(created_at as date)) as somatorio'. 
                        ' FROM tb_horario '.
                        ' WHERE created_at between \''.$datainicio.'\' AND \''.$datafinal.'\' '.$and.
                        ' GROUP BY(cast(created_at as date))';

        $connection = DB::connection();     
        $horarios = $connection->select($horarioQuery);

        
        if($horarios==null)
            return Response::json(array('success'=>false,'mensagem' => 'não existe registro para essa consulta'));

        $datasJson = array();
        $somatorioJson = array();

        foreach ($horarios as $horario)
        {
            $horario->data = Carbon::createFromFormat('Y-m-d', $horario->data)->format('d/m/Y');
            array_push($datasJson, $horario->data);
            array_push($somatorioJson, $horario->somatorio);
        }
        $datafinal->subDay();
        //$periodo = ($datafinal->diffInDays($datainicio))+1;
        return Response::json(array('success'=>true,'horarios' => $horarios, 'datainicio'=>$datainicio, 'datafinal'=>$datafinal,'datasJson' => $datasJson,
            'somatorioJson' => $somatorioJson ));
    }

    public function marcacaoPorUnidade(){
        $input = Input::all();
        set_time_limit(0);
        $datainicio = $input['dtini']; 
        $datafinal  = $input['dtfim'];
        $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
        $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal)->addDay();

        $horarioQuery = 'SELECT unidade, count(cod) as somatorio'. 
                        ' FROM tb_horario '.
                        ' WHERE created_at between \''.$datainicio.'\' AND \''.$datafinal.'\' '.
                        ' GROUP BY unidade';
        $connection = DB::connection();     
        $horarios = $connection->select($horarioQuery);

        
        if($horarios==null)
            return Response::json(array('success'=>false,'mensagem' => 'não existe registro para essa consulta'));

        return Response::json(array('success'=>true,'horarios' => $horarios)); 
    }



    public function indexTodas()
    {   
        $todos     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->lists('periodo','cod');

        $data = array('todos' => (array( 0 =>'Selecione um período') + $todos),);
               
        return View::make('relatorios.todas', $data); 
    }


    public function todas()
    {
        $input = Input::all();

        if(isset($input['todos'])&&($input['todos']!=0))
        {
            set_time_limit(0);
            $datainicio = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('periodo_fim');
            $mes  = DB::table('tb_periodo')->where('cod', $input['todos'])->pluck('mes_referencia'); 
            $cod = $input['todos'];

            $horarioQuery = $this->horario->with(array('funcionario', 'item_contabil'))
                                 ->whereBetween('data', array($datainicio, $datafinal))
                                 ->whereNull('deleted_at')
                                 ->orderBy('cod_funcionario')
                                 ->orderBy('data', 'ASC')
                                 ->get();

            $horarios = reset($horarioQuery);

            if($horarios==null)
                return Redirect::route('relatorios.geral')->withErrors(['não existem marcações para este período']);
           
            
            $count  = count($horarios);

            if($horarios==null)
                return Redirect::route('relatorios.todas')->withErrors(['não existem marcações para este período']);

            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
            $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal);

            $nome = 'Marcações'.$mes.'-'.Carbon::now()->year;

            return View::make('relatorios.todashow', compact('datainicio', 'datafinal', 'horarios', 'count', 'cod'));
        }
        else
        {
            return Redirect::route('relatorios.todas')->withErrors(['Selecione um período!']);
        }
    }

    public function exportar($cod)
    {   
        set_time_limit(0);
        $datainicio = DB::table('tb_periodo')->where('cod', $cod)->pluck('periodo_inicio'); 
        $datafinal  = DB::table('tb_periodo')->where('cod', $cod)->pluck('periodo_fim');
        $mes  = DB::table('tb_periodo')->where('cod', $cod)->pluck('mes_referencia'); 
            
        $horarioQuery = $this->horario->with(array('funcionario', 'item_contabil'))
                            ->whereBetween('data', array($datainicio, $datafinal))
                            ->whereNull('deleted_at')
                            ->orderBy('cod_funcionario')
                            ->orderBy('data', 'ASC')
                            ->get();

        $horarios = reset($horarioQuery);
            
        if($horarios==null)
            return Redirect::route('relatorios.geral')->withErrors(['não existem marcações para este período']);
               
        $nome = 'Marcações'.$mes.'-'.Carbon::now()->year;
           
        $this->horario->allToExcel($nome, $horarios);
    }

}//end of reports controller


