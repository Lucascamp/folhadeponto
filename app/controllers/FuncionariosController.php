<?php

class FuncionariosController extends BaseController {

	/**
	 * Funcionario Repository
	 *
	 * @var Funcionario
	 */
	protected $funcionario;

	public function __construct(Funcionario $funcionario)
	{
		$this->funcionario = $funcionario;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$funcionarios = $this->funcionario->all();

		return View::make('funcionarios.index', compact('funcionarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('funcionarios.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Funcionario::$rules);

		if ($validation->passes())
		{
			$this->funcionario->create($input);

			return Redirect::route('funcionarios.index');
		}

		return Redirect::route('funcionarios.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$funcionario = $this->funcionario->findOrFail($id);

		return View::make('funcionarios.show', compact('funcionario'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$funcionario = $this->funcionario->find($id);

		if (is_null($funcionario))
		{
			return Redirect::route('funcionarios.index');
		}

		return View::make('funcionarios.edit', compact('funcionario'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Funcionario::$rules);

		if ($validation->passes())
		{
			$funcionario = $this->funcionario->find($id);
			$funcionario->update($input);

			return Redirect::route('funcionarios.show', $id);
		}

		return Redirect::route('funcionarios.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->funcionario->find($id)->delete();

		return Redirect::route('funcionarios.index');
	}

}
