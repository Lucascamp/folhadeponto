<?php

use Carbon\Carbon;

class HorariosController extends BaseController
{

    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;    


    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(Horario $horario, Funcionario $funcionario, ItemContabil $item_contabil)
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
    }

    public function editCollumn()
    {        
        $key   = Input::get('column');
        $value = Input::get('value');
        $id    = Input::get('id');
        $label    = Input::get('label');
        
        return $this->horario->updateField($key, $value, $id, $label);
    }
    

    public function getDatatable()
    {               
        $allowed = explode(',', Auth::user()->unidade_permissoes);

        $periodoInicio = DB::table('tb_periodo')->where('fechado', 0)->min('periodo_inicio');
        $periodoFinal  = DB::table('tb_periodo')->where('fechado', 0)->max('periodo_fim');

        $horarios = $this->horario->with(array('funcionario', 'item_contabil'))
        ->join('tb_funcionario', 'cod_funcionario', '=', 'tb_funcionario.cod')
        ->whereIn('tb_horario.unidade', $allowed)
        ->whereBetween('tb_horario.data', array($periodoInicio, $periodoFinal))
        ->orderBy('data')
        ->select('tb_horario.*');

        return Datatable::query($horarios)
        ->showColumns($this->horario->getColumnsKeys())
        ->searchColumns(array('cod_funcionario', 'tb_horario.unidade','data', 'cod_item_contab', 'tb_funcionario.nome'))
        ->addColumn('cod_funcionario', function($horario){
            return $horario->funcionario->nome;
        })
        ->addColumn('data', function($horario){
            return $horario->data->format('d/m/Y');
        })
        ->addColumn('hora_entrada', function($horario){
            return $horario->hora_entrada->format('H:i');
        })
        ->addColumn('hora_saida', function($horario){
            return $horario->hora_saida->format('H:i');
        })
        ->addColumn('intervalo_inicio', function($horario){
            return $horario->intervalo_inicio->format('H:i');
        })
        ->addColumn('intervalo_fim', function($horario){
            return $horario->intervalo_fim->format('H:i');
        })
        ->addColumn('cod_item_contab', function($horario){
            return $horario->item_contabil->CTD_DESC01;
        })
        ->addColumn('horas_trabalhadas', function($horario){
            return $horario->getWorkedHours()->format('H:i');
        })
        ->addColumn('hora_extra', function($horario){
            return $horario->getExtraHours()->format('H:i');
        })
        ->addColumn('adicional_noturno', function($horario){
            return $horario->getNightHours()->format('H:i');
        })
        ->addColumn('hora_faturada', function($horario){
            return $horario->hora_faturada->format('H:i');
        })         
        ->addColumn('tempo_exposicao', function($horario){
            return $horario->tempo_exposicao;
        }) 
        ->addColumn('observacoes', function($horario){
            return $horario->observacoes;
        })        
        ->orderColumns($this->horario->getColumnsKeys())
        ->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function testDatatable()
    {                            
        $format        = Input::get('format', 'html');

        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');
        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->lists('descricao','valor');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');

        $table = Datatable::table()
        ->addColumn($this->horario->getColumnsLabels())
        ->setUrl(route('datatable.horarios'))
        ->setOptions("sDom", "<'row top-buffer'<'span8'l><'span8'f>r>t<'row'<'span8'i><'span8'p>>")
        ->setOptions("bProcessing", true)
        ->setOptions("bSort", false)
        ->setOptions("sPaginationType", "bootstrap")        
        ->setOptions(array('oLanguage' => array('sUrl' => 'js/datatables/translate.json')))
        ->setOptions("deferRender", true)
        ->setOptions("aoColumns", $this->horario->aoColumns)
        ->noScript();


        JavaScript::put(array(
            'itens_contabil'=> $itens_contabil,
            'unidades' => $unidades,
            'contratadas' => $contratadas,
            'atividades' => $atividades
        ));

        switch ( $format ) {
            case 'json':
            return $horarios;
            break;
            default:
            return View::make('horarios.datatable', array('table'=> $table));
            break;
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $datainicio  =  DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_inicio');

        if(!isset($datainicio))
            $datainicio = Carbon::now()->startOfMonth();
            // return View::make('periodoFechado');   
        else
            $datainicio = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio)->format('Y-m-d');

        $demitidos_qt =  $this->funcionario->where('status_qt', 'D')->lists('cod', 'cod');

        $demitidos_a5 =  $this->funcionario->where('status_a5', 'D')->lists('cod', 'cod');

        $diff = array_diff($demitidos_a5, $demitidos_qt);

        $demitidos = $demitidos_qt + $diff;

        foreach ($demitidos as $cod) 
        {
            $dem_qt = false;
            $dem_a5 = false;

            $func = $this->funcionario->where('cod', $cod)->first();  

            if((isset($func['matricula_qt'])) and ($func['status_qt'] === 'D'))
            {
                $dem_qt = true;
            }

            if((isset($func['matricula_a5'])) and ($func['status_a5'] === 'D'))
            { 
                $dem_a5 = true;
            } 

            if(($dem_a5 === false) and (isset($func['matricula_a5'])))
                 unset($demitidos[$cod]);

            if(($dem_qt === false) and (isset($func['matricula_qt'])))
                 unset($demitidos[$cod]);     
        }

        $funcionarios = $this->funcionario->orderBy('nome')->lists('nome','cod');

        foreach ($demitidos as $cod)
        {
            unset($funcionarios[$cod]);
        }

        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');

        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->orderBy('sequencia')->lists('descricao','valor');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');

        $funcionarios   = (array('' => 'Selecione') + $funcionarios);
        $itens_contabil = (array('' => 'Selecione') + $itens_contabil);
        $unidades = (array('' => '') + $unidades);

        return View::make('horarios.create', compact('funcionarios','itens_contabil','unidades','contratadas','atividades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {   
        $input = Input::all(); 

        if(isset($input['ferias']))
           $input['atividade'] = $input['atividade'].'/'.$input['ferias'];

        if($input['hora_faturada']=='')
            $input['hora_faturada']='00:00';

        if(Carbon::createFromFormat('d/m/Y', $input['data']) > Carbon::now())
        {
            return Redirect::route('horarios.create')
            ->withInput()
            ->with('message', 'Data para cadastro maior que a data atual.');
        }

        $validation = Validator::make($input, Horario::$rules);

        if ($validation->passes())
        {
            $this->horario->create($input);

            return Redirect::route('horarios.create')->with('success', 'Horário na data '.$input['data'].' criado com sucesso!');
        }

        return Redirect::route('horarios.create')
        ->withInput()
        ->withErrors($validation)
        ->with('message', 'Existem erros de validação no formulário.');
    }

     public function ti()
    {   
       $func = '338';

       //197 Lucas,  338 Samuel , 264 Fernando

       $periodoInicio = Carbon::createFromFormat('Y-m-d H:i:s', DB::table('tb_periodo')->where('fechado', 0)->min('periodo_inicio'));
       $periodoFinal  = Carbon::createFromFormat('Y-m-d H:i:s', DB::table('tb_periodo')->where('fechado', 0)->max('periodo_fim'));
       $diffInDays = $periodoInicio->diffInDays($periodoFinal);

       foreach (range(1, $diffInDays) as $index)
       {
            if ($periodoInicio->dayOfWeek === Carbon::SATURDAY) 
            {
                var_dump('sabado');
            }

            elseif ($periodoInicio->dayOfWeek === Carbon::SUNDAY) 
            {
                var_dump('domingo');
            }
            else
            {
                $query = $this->horario->where('data', $periodoInicio)->where('cod_funcionario', $func)->first();
                var_dump($query['data']);
                if($query == null)
                {

                $rand_ent = rand(0, 15);
                if($rand_ent < 10)
                   $rand_ent = '0'.$rand_ent;     
                $rand_int = rand(0, 20);
                if($rand_int < 10)
                   $rand_int = '0'.$rand_int;
                $rand_sai = rand(0, 30);
                if($rand_sai < 10)
                   $rand_sai = '0'.$rand_sai;

                $input = [
                       'created_by' => '1',
                       'cod_funcionario' => $func,
                       'data' => $periodoInicio->format('d/m/Y'),
                       'unidade' => 'MG',
                       'atividade' => 'Hora Direta',
                       'cod_item_contab' => '107',
                       'hora_entrada' => '08:'.$rand_ent,
                       'intervalo_inicio' => '12:'.$rand_int,
                       'intervalo_fim' => '13:'.$rand_int,
                       'hora_saida' => '17:'.$rand_sai,
                       'hora_faturada' => '00:00',
                       'hora_contratada' => '8',
                       'tempo_exposicao' => '',
                       'observacoes' => ''
                ]; 

                $this->horario->create($input);
                
                }
            }

            $periodoInicio->addDay();
       }
       dd('fim');
    }


    public function createadm()
    {
        $hoje = Carbon::now();

        $marcacao = $this->horario
                         ->where('cod_funcionario', Auth::user()->cod_funcionario)
                         ->where('data', $hoje->format('Y-m/d'))
                         ->first();

        $hoje= $hoje->format('d/m/Y');                   

        return View::make('horarios.createadm', compact('hoje', 'marcacao'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeadm()
    {   
        $input = Input::all(); 

        if($input['hora_faturada']=='')
            $input['hora_faturada']='00:00';

        $validation = Validator::make($input, Horario::$rules);

        if ($validation->passes())
        {
            if(isset($input['cod']))
            {
                $horario = $this->horario->find($input['cod']);
                unset($input['cod']);
                $horario->update($input);

                return Redirect::route('horarios.createadm')->with('message1', 'Horário na data <b>'.$input['data'].'</b> alterado com sucesso!');
      
            }
            else
            {
                $this->horario->create($input);
               
                return Redirect::route('horarios.createadm')->with('message1', 'Horário na data <b>'.$input['data'].'</b> criado com sucesso!');
            }
        }

        return Redirect::route('horarios.createadm')
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createmassive()
    {
        $funcionarios   = $this->funcionario->orderBy('nome')->lists('nome','cod');
        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');

        $funcionarios   = (array('' => 'Selecione') + $funcionarios);
        $itens_contabil = (array('' => 'Selecione') + $itens_contabil);
        $unidades = (array('' => '') + $unidades);

        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->lists('descricao','valor');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');

        return View::make('horarios.createmassive', compact('funcionarios','itens_contabil','unidades', 'contratadas', 'atividades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function savemassive()
    {   
        $campos_ignorados = array('beneficio_tipo_va', 'beneficio_tipo_vt', 'beneficio_tipo_am', 'datafinal');
        
        $input = Input::all(); 

        $validation = Validator::make($input, Horario::$rules);

        $datainicio  = Carbon::createFromFormat('d/m/Y', $input['data']); 
        $datafinal   = Carbon::createFromFormat('d/m/Y', $input['datafinal']); 

        $datashow = Carbon::createFromFormat('d/m/Y', $input['data']); 

        $dias = $datainicio->diffInDays($datafinal);
        
        $input = array_diff_key($input, array_flip($campos_ignorados)); 

        if ($validation->passes())
        {
            for($i=0; $i<=$dias; $i++)
            {
                if($i == 0)
                    $this->horario->create($input);

                else
                {
                    $input['data'] = $datainicio->addDay();

                    $ano = $input['data']->year;

                    $mes = $input['data']->month;
                        if($mes<10)
                            $mes = '0'.$mes;

                    $dia = $input['data']->day;
                        if($dia<10)
                            $dia = '0'.$dia;

                    $input['data'] = $dia.'/'.$mes.'/'.$ano;
                    
                    $this->horario->create($input);
                }
                
            }
            
            return Redirect::route('horarios.criarvarios')
            ->with('success', 'Horários criados do dia '.$datashow->format('d/m/Y').' a '.$datafinal->format('d/m/Y').' criado com sucesso!');
        }

        return Redirect::route('horarios.criarvarios')
        ->withInput()
        ->withErrors($validation)
        ->with('message', 'Existem erros de validação no formulário.');
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createVariosFuncionarios()
    {
        $datainicio  =  DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_inicio');

        if(!isset($datainicio))
            $datainicio = Carbon::now()->startOfMonth();
            // return View::make('periodoFechado');   
        else
            $datainicio = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio)->format('Y-m-d');

        $demitidos =  $this->funcionario
                                ->where('data_demissao_qt','<', $datainicio)
                                ->where('data_demissao_a5','<', $datainicio)
                                ->orderBy('nome')
                                ->lists('nome','cod');
        
        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');

        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->orderBy('sequencia')->lists('descricao','valor');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');

        $itens_contabil = (array('' => 'Selecione') + $itens_contabil);
        $unidades = (array('' => '') + $unidades);

        return View::make('horarios.createfuncionarios', compact('itens_contabil','unidades','contratadas','atividades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function salvarPontoFuncionarios()
    {   
        $input = Input::all(); 
        
        $validation = Validator::make($input,
            array(
                'data'             => 'required|date_format:"d/m/Y"',
                'unidade'          => 'required',
                'cod_funcionarios' => 'required',
                'hora_entrada'     => 'required|date_format:"H:i"',
                'hora_saida'       => 'required|date_format:"H:i"',
                'intervalo_inicio' => 'required|date_format:"H:i"',
                'intervalo_fim'    => 'required|date_format:"H:i"',
                'hora_faturada'    => 'date_format:"H:i"',
                'hora_contratada'  => 'required',
                'cod_item_contab'  => 'required|integer',
                'atividade'        => 'required',
                'tempo_exposicao'  => 'date_format:"H:i:s"',
                'observacoes'      => 'min:0',
                )
        );

        if ($validation->passes())
        {
            foreach ($input['cod_funcionarios'] as $funcionario)
            {
                $input['cod_funcionario'] = $funcionario;

                $this->horario->create($input);
            }
                
            return Redirect::route('horarios.variosfuncionarios')
            ->with('success', 'Horários da equipe criado com sucesso!');
        }

        return Redirect::route('horarios.variosfuncionarios')
        ->with('message', 'Existem erros de validação no formulário.')
        ->withErrors($validation)
        ->withInput()
        ;
    }

    public function getFuncionariosUnidade()
    {
        $unidade = Input::get('unidade'); 

        $demitidos =  $this->funcionario
                                ->where('data_demissao_qt', '<' , Carbon::now())
                                ->orWhere('data_demissao_a5', '<' , Carbon::now())
                                ->orderBy('nome')
                                ->lists('nome','cod');
        
        $funcionarios = $this->funcionario->where('unidade', $unidade)->orderBy('nome')->lists('nome','cod');
        
        $funcionarios = array_diff($funcionarios, $demitidos);

        return Response::json($funcionarios);
    }




    public function createVariosFuncionarios2(){
        $datainicio  =  DB::table('tb_periodo')->where('fechado', 0)->pluck('periodo_inicio');

        if(!isset($datainicio))
            $datainicio = Carbon::now()->startOfMonth();
            // return View::make('periodoFechado');   
        else
            $datainicio = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio)->format('Y-m-d');

        $demitidos =  $this->funcionario
                                ->where('data_demissao_qt','<', $datainicio)
                                ->where('data_demissao_a5','<', $datainicio)
                                ->orderBy('nome')
                                ->lists('nome','cod');
        
        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');

        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->orderBy('sequencia')->lists('descricao','valor');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');

        $itens_contabil = (array('' => 'Selecione') + $itens_contabil);
        $unidades = (array('' => '') + $unidades);

        return View::make('horarios.createfuncionarios2', compact('itens_contabil','unidades','contratadas','atividades'));
    }

    public function salvarPontoFuncionarios2()
    {   
        $input = Input::all();  

        $validation = Validator::make($input,
            array(
                'data'             => 'required|date_format:"d/m/Y"',
                'unidade'          => 'required',
                'cod_funcionario' => 'required',
                'hora_entrada'     => 'required|date_format:"H:i"',
                'hora_saida'       => 'required|date_format:"H:i"',
                'intervalo_inicio' => 'required|date_format:"H:i"',
                'intervalo_fim'    => 'required|date_format:"H:i"',
                'hora_faturada'    => 'date_format:"H:i"',
                'hora_contratada'  => 'required',
                'cod_item_contab'  => 'required|integer',
                'atividade'        => 'required',
                'tempo_exposicao'  => 'date_format:"H:i:s"',
                'observacoes'      => 'min:0',
                )
        );

        if ($validation->passes()){
            $this->horario->create($input);
            return Response::json(['success' => true]);
        }
        return Response::json(['success' => false]);
        // return Redirect::route('horarios.variosfuncionarios2')
        // ->with('message', 'Existem erros de validação no formulário.')
        // ->withErrors($validation)
        // ->withInput();
    }

    public function getFuncionariosUnidade2()
    {
        $unidade = Input::get('unidade'); 

        $demitidos =  $this->funcionario
                                ->where('data_demissao_qt', '<' , Carbon::now())
                                ->orWhere('data_demissao_a5', '<' , Carbon::now())
                                ->orderBy('nome')
                                ->lists('nome','cod');
        
        $funcionarios = $this->funcionario->where('unidade', $unidade)->orderBy('nome')->lists('nome','cod');
        
        $funcionarios = array_diff($funcionarios, $demitidos);

        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->orderBy('sequencia')->lists('descricao','valor');
        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');

        return Response::json(array('funcionarios'=>$funcionarios, 'contratadas'=>  $contratadas, 'centrocusto' => $itens_contabil,'atividades'=> $atividades));
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
     public function show($id)
     {
        $horario = $this->horario->with('item_contabil')->findOrFail($id);

        return View::make('horarios.show', compact('horario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $horario = $this->horario->with('item_contabil')->find($id);  
        
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');
        $unidades = (array('' => '') + $unidades);

        $contratadas =  DB::table('tb_valores_comuns')->where('tipo', 'Contratadas')->lists('descricao','valor');
        $atividades =  DB::table('tb_valores_comuns')->where('tipo', 'Atividade')->lists('descricao','valor');


        if (is_null($horario))
        {
            return Redirect::route('horarios.index');
        }

        return View::make('horarios.edit', compact('horario','unidades','contratadas','atividades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');   

        $input['validado'] = 0;
        $input['validado2'] = 0;
        $input['validado_por'] = NULL;
        $input['validado_por2'] = NULL;
        $input['valido'] = 0;
        $input['motivo'] = '';
        $input['motivo2'] = '';

        $campos_ignorados = array('beneficio_tipo_va', 'beneficio_tipo_vt', 'beneficio_tipo_am');

        $input = array_diff_key($input, array_flip($campos_ignorados)); 

        $validation = Validator::make($input, Horario::$rules);

        if ($validation->passes())
        {
            $horario = $this->horario->find($id);
            $horario->update($input);

            return Redirect::route('horarios.show', $id);
        }

        return Redirect::route('horarios.edit', $id)
        ->withInput()
        ->withErrors($validation)
        ->with('message', 'Existem erros de validação no formulário.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->horario->where('cod', $id)->update(array('deleted_by' => Auth::user()->cod));
        $this->horario->find($id)->delete();

        return Redirect::route('horarios.index');
    }

    public function getDadosMarcacao(){
        $id_funcionario = Input::get('cod_funcionario');

        $funcionario = $this->funcionario->find($id_funcionario, array('beneficios','unidade'));
        $beneficios = explode(',', $funcionario->beneficios);
        $unidade = $funcionario->unidade;
        return Response::json(array('beneficios' => $beneficios,'unidade' => $unidade));
    }

    public function checkbox()
    {   
        $id_funcionario = Input::get('cod_funcionario');

        $funcionario = $this->funcionario->find($id_funcionario, array('beneficios'));
        $beneficios = explode(',', $funcionario->beneficios);
        return Response::json($beneficios);
    }

    public function holiday()
    {   
        $unidade = Input::get('cod');

        $feriados =  DB::table('tb_feriado')
        ->where('codlocalizacao', $unidade)
        ->orWhere('codlocalizacao', '')
        ->lists('feriado', 'cod');

        return Response::json($feriados);
    }
   
}//end of horarios controller