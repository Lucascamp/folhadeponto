<?php

use Carbon\Carbon;

class FeriasController extends BaseController
{
    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $ferias;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;

    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $cc;

    public function __construct(
        Ferias $ferias,
        Funcionario $funcionario,
        ItemContabil $cc
        )
    {
        $this->ferias          = $ferias;
        $this->funcionario      = $funcionario;
        $this->cc    = $cc;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $funcionario_filter = Input::get('filter_funcionario');
        $cc_filter = Input::get('filter_cc');

        $demitidos_qt =  $this->funcionario->where('status_qt', 'D')->lists('cod', 'cod');

        $demitidos_a5 =  $this->funcionario->where('status_a5', 'D')->lists('cod', 'cod');

        $diff = array_diff($demitidos_a5, $demitidos_qt);

        $demitidos = $demitidos_qt + $diff;

        foreach ($demitidos as $cod) 
        {
            $dem_qt = false;
            $dem_a5 = false;

            $func = $this->funcionario->where('cod', $cod)->first();  

            if((isset($func['matricula_qt'])) and ($func['status_qt'] === 'D'))
            {
                $dem_qt = true;
            }

            if((isset($func['matricula_a5'])) and ($func['status_a5'] === 'D'))
            { 
                $dem_a5 = true;
            } 

            if(($dem_a5 === false) and (isset($func['matricula_a5'])))
                 unset($demitidos[$cod]);

            if(($dem_qt === false) and (isset($func['matricula_qt'])))
                 unset($demitidos[$cod]);     
        }

        $funcionarios = $this->funcionario->orderBy('nome')->lists('nome','cod');

        foreach ($demitidos as $cod)
        {
            unset($funcionarios[$cod]);
        }

        $funcionarios_ferias = $this->ferias->whereNotIn('cod', $funcionarios)->groupBy('cod_funcionario'); 
        
        if ($funcionario_filter)
        {               
            $funcionarios_ferias->where('cod_funcionario', $funcionario_filter);
        } 

        if ($cc_filter)
        {              
            $funcs = $this->funcionario->where('centro_custo', $cc_filter)->lists('cod');

            $cc = array_intersect(array_keys($funcionarios), $funcs);

            $funcionarios_ferias->whereIn('cod_funcionario', $cc);
        }

        $funcionarios_ferias = $funcionarios_ferias->lists('cod_funcionario');

        foreach ($funcionarios_ferias as $func)
        {
            $ferias = $this->ferias->with('funcionario')->where('cod_funcionario', $func)->get();
            $count = count($ferias);
            $ferias_atual[$func] = $ferias[$count-1];
        }    

        $cc = $this->cc->lists('CTD_DESC01', 'cod'); 

        $data = [
                'funcionario_drop' => (array( 0 =>'Selecione um funcionário') + $funcionarios),
                'cc_drop' => (array( 0 =>'Selecione um centro de custo') + $cc),
                'funcionario_filter' => $funcionario_filter,
                'cc_filter' => $cc_filter,
                'funcionarios' => $funcionarios,
                'ferias_atual' => $ferias_atual,
                'cc' => $cc
                ];

        return View::make('ferias.index', $data);
    }

}