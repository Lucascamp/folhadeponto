<?php

use Carbon\Carbon;

class ExportacaoController extends BaseController
{

    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;    


    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(Horario $horario, Funcionario $funcionario, ItemContabil $item_contabil)
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
    }

   public function indexExportarProtheus()
    {   
        $fechado     = DB::table('tb_periodo')
                        ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                        ->orderBy('periodo_inicio')
                        ->where('fechado', 1)
                        ->lists('periodo','cod');

        $data = array('fechado' => (array( 0 =>'Selecione um período') + $fechado),);
               
        return View::make('exportacao.index', $data); 
    }
    
    public function exportacaoProtheus()
    {
        $input = Input::all();
        set_time_limit(0);
        if(isset($input['fechado'])&&($input['fechado']!=0))
        {
            $datainicio = substr(DB::table('tb_periodo')->where('cod', $input['fechado'])->pluck('periodo_inicio'),0,10); 
            $datafinal  = substr(DB::table('tb_periodo')->where('cod', $input['fechado'])->pluck('periodo_fim'),0,10);

            $mes = substr(DB::table('tb_periodo')->where('cod', $input['fechado'])->pluck('mes_referencia'),0,3); 

            $ano = DB::table('tb_periodo')->where('cod', $input['fechado'])->pluck('ano'); 
            
            $funcionarios_adm = $this->funcionario->select('cod')->whereNotNull('administrativo')->lists('cod', 'cod');

            $horarioQuery = $this->horario->with('funcionario')
                                 ->whereNotIn('cod_funcionario', $funcionarios_adm)
                                 ->whereBetween('data', array($datainicio, $datafinal))
                                 ->where('fechado', 1)
                                 ->whereNull('deleted_at')
                                 //->skip(1922)->take(1)
                                 ->get();

                                 //->lists('cod');

            $results = reset($horarioQuery);
            
            if(isset($results))
            {
            /*Horas Extras*/
            $file = export_folder().'/HE-'.$mes.'-'.$ano.'.txt';
            if(File::exists($file))
                File::delete($file);

           
            $this->horario->exportExtraHours($results, 'matricula_qt', $file, $datainicio);

            $this->horario->exportExtraHours($results, 'matricula_a5', $file, $datainicio);
           
            /*Horas Noturnas*/
            $file = export_folder().'/ADN-'.$mes.'-'.$ano.'.txt';
            if(File::exists($file))
                File::delete($file);


            $this->horario->exportNightHours($results, 'matricula_qt', $file);
            $this->horario->exportNightHours($results, 'matricula_a5', $file);
            
            $atividades = array('Falta', 'Folga', 'Atestado', 'vt', 'am', 'va');


            foreach($atividades as $atividade)
            {
                $file = export_folder().'/'.strtoupper($atividade).'-'.$mes.'-'.$ano.'.txt';
                if(File::exists($file))
                    File::delete($file);

                $this->horario->exportActivity($results, 'matricula_qt', $file, $atividade);
                $this->horario->exportActivity($results, 'matricula_a5', $file, $atividade);
            }

            $arquivos=File::files(export_folder());

            $zip = new ZipArchive();

            $filepath = export_folder().'/Arquivos-'.$mes.'-'.$ano.'.zip';

            if( $zip->open( $filepath, ZipArchive::CREATE )  === true)
            {
            	foreach($arquivos as $arquivo)
            	{
            		$new_filename = substr($arquivo, 22);
            		if(strpos($new_filename, $mes))
            			$zip->addFile($arquivo , $new_filename);
            	}     

            	$zip->close();
            }
        }
        header('Content-type: application/zip');
        header('Content-disposition: attachment; filename="Arquivos-'.$mes.'-'.$ano.'.zip"');
        readfile($filepath);
        unlink($filepath);
    }
    else
    {
        return Redirect::route('exportar')->with(['Selecione um período!']);
    }
}


}//end of horarios controller