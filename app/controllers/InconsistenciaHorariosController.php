<?php

use Carbon\Carbon;

class InconsistenciaHorariosController  extends BaseController
{

    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;

    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(Horario $horario, Funcionario $funcionario, ItemContabil $item_contabil)
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $format        = Input::get('format', 'html');
        $funcionario   = Input::get('filter_funcionario');
        
        $filter_funcionarios = $this->funcionario->orderBy('nome')->lists('nome', 'cod');
       
        //initialize query to DB
        $horariosQuery = $this->horario->with(array('funcionario', 'item_contabil'))
                              ->whereNull('deleted_at')
                              ->orderby('cod', 'ASC');

        if ($funcionario)
        {               
            $horariosQuery->where('cod_funcionario', $funcionario)
                          ->where(function ($query) {
                                $query->where('valido', 0)
                                      ->orWhere('motivo', '!=', '')
                                      ->orWhere('motivo2', '!=', '');
                            })
                          ->orderBy('data');
        }
        else
        {
            $horariosQuery->where('valido', 0)
                          ->orWhere('motivo', '<>', '')
                          ->orWhere('motivo2', '<>', '')
                          ->orderBy('cod_funcionario');
        }    
        

        $horarios = $horariosQuery->paginate(40);

        $data = array(
            'horarios'  => $horarios,
            'filter_funcionarios' => (array( 0 =>'Selecione um funcionario') + $filter_funcionarios),
            'funcionario' => $funcionario,
        );

        return View::make('horarios.inconsistencia', $data);
        
    }
}