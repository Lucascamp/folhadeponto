<?php
use Carbon\Carbon;
class PeriodosController  extends BaseController
{

    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;

    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(Horario $horario, Funcionario $funcionario, ItemContabil $item_contabil)
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
    }

    public static $rules = array(       
        'datainicio' => 'required|date_format:"d/m/Y"',
        'datafinal'  => 'required|date_format:"d/m/Y"',
        'ano'        => 'required',
        'mes'        => 'required',
        );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $now = Carbon::now();
        $yearArray = array();

        $anoatual= $now->year;

        for($i=0;$i<4;$i++)
        {
           $yearArray[$now->year+$i] = $now->year+$i;
        }
        
        $aberto      = DB::table('tb_periodo')
                                ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                                ->orderBy('periodo_inicio')
                                ->where('fechado', 0)
                                ->lists('periodo','cod');

        $fechado     = DB::table('tb_periodo')
                                ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                                ->orderBy('periodo_inicio')
                                ->where('fechado', 1)
                                ->lists('periodo','cod');

        $todos       = DB::table('tb_periodo')
                                ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                                ->orderBy('periodo_inicio')
                                ->lists('periodo','cod');

        $data = array(
                    'aberto'  => (array( 0 =>'Selecione um período') + $aberto),
                    'fechado' => (array( 0 =>'Selecione um período') + $fechado),
                    'todos'   => (array( 0 =>'Selecione um período') + $todos),
                    'ano' => $yearArray,
                    'anoatual' => $anoatual,
                    );

        return View::make('periodo.create', $data);
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $datainicio  = Carbon::createFromFormat('d/m/Y H:i:s', $input['datainicio'].' 00:00:00');
        $datafinal  = Carbon::createFromFormat('d/m/Y H:i:s', $input['datafinal'].' 00:00:00');

        $created = Carbon::now();
                 DB::table('tb_periodo')->insert(
                    array(
                        'periodo_inicio' => $datainicio,
                        'periodo_fim'    => $datafinal,
                        'ano'            => $input['ano'],
                        'mes_referencia' => $input['mes'],
                        'created_at'     => $created,
                        )
                    );

         return Redirect::route('periodos')->with('success', 'O período de '.$input['mes'].' de '.$input['ano'].' foi criado com sucesso!');
    }


    public function status()
    {
        $aberto     = Input::get('aberto');
        $fechado    = Input::get('fechado');

        if(isset($aberto)&&($aberto!=0))
        {
            $datainicio = DB::table('tb_periodo')->where('cod', $aberto)->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $aberto)->pluck('periodo_fim'); 
            
            DB::table('tb_periodo')->where('cod', $aberto)->update(array('fechado'=> 1));
            DB::table('tb_horario')->whereBetween('data', array($datainicio, $datafinal))->update(array('fechado'=> 1));

            return Redirect::route('periodos')->with('success', 'O período foi <b>FECHADO</b> com sucesso!');
        }
        elseif(isset($fechado)&&($fechado!=0))
        {
            $datainicio = DB::table('tb_periodo')->where('cod', $fechado)->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $fechado)->pluck('periodo_fim'); 

            DB::table('tb_periodo')->where('cod', $fechado)->update(array('fechado'=> 0));
            DB::table('tb_horario')->whereBetween('data', array($datainicio, $datafinal))->update(array('fechado'=> 0));

            return Redirect::route('periodos')->with('success', 'O período foi <b>REABERTO</b> com sucesso');
        }
       
       
   }

   public function edit()
    {
        $cod     = Input::get('todos');
       
        if($cod!=0)
        {
            $periodo = DB::table('tb_periodo')->where('cod', $cod)->first(); 
        
            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $periodo->periodo_inicio);
            $datafinal  = Carbon::createFromFormat('Y-m-d H:i:s', $periodo->periodo_fim);
        
            return View::make('periodo.edit', compact('periodo','datainicio' , 'datafinal'));
        }
        else
            return Redirect::route('periodos')->withErrors(['Escolha um período para Editar']);
    }

    public function update()
    {
        $cod = Input::get('cod');
        $datainicio = Input::get('datainicio');
        $datafinal  = Input::get('datafinal');
        
        $datainicio  = Carbon::createFromFormat('d/m/Y H:i:s', $datainicio.' 00:00:00');
        $datafinal  = Carbon::createFromFormat('d/m/Y H:i:s', $datafinal.' 00:00:00');
        
        DB::table('tb_periodo')->where('cod', $cod)->update(
                    array(
                        'periodo_inicio' => $datainicio,
                        'periodo_fim'    => $datafinal,
                         )
                    );       

        return Redirect::route('periodos')->with('success', 'As datas do período foram alterado com sucesso!');
    }
}