<?php

use Carbon\Carbon;

class BancoController extends BaseController
{

    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $banco;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;    

    public function __construct(Banco $banco, Funcionario $funcionario)
    {
        $this->banco    = $banco;
        $this->funcionario      = $funcionario;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $funcionarios = $this->funcionario->where('administrativo', 1)->orderBy('nome')->lists('nome', 'cod');

        $banco = $this->banco->get();

        $data = array(
            'funcionarios' => $funcionarios,
            'banco' => $banco,
            );

        return View::make('banco.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {   
        $input = array_except(Input::all(), '_token');

        if(isset($input['sub_from_funcionario']))
            $input['cod_funcionario'] = $input['sub_from_funcionario'];

        elseif(isset($input['add_to_funcionario']))
            $input['cod_funcionario'] = $input['add_to_funcionario'];

        $validation = Validator::make($input, Banco::$rules);

        if ($validation->passes())
        {
            $this->banco->create($input);

            return Redirect::route('banco')->with('message', 'criado com sucesso!');
        }

        return Redirect::route('banco')
        ->withInput()
        ->withErrors($validation)
        ->with('message', 'Existem erros');
    }

    public function createadm()
    {
        $hoje = Carbon::now();

        $marcacao = $this->horario
                         ->where('cod_funcionario', Auth::user()->cod_funcionario)
                         ->where('data', $hoje->format('Y-m/d'))
                         ->first();


        $hoje= $hoje->format('d/m/Y');                   

        return View::make('horarios.createadm', compact('hoje', 'marcacao'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeadm()
    {   
        $input = Input::all(); 

        if($input['hora_faturada']=='')
            $input['hora_faturada']='00:00';

        $validation = Validator::make($input, Horario::$rules);

        if ($validation->passes())
        {
            if(isset($input['cod']))
            {
                $horario = $this->horario->find($input['cod']);
                unset($input['cod']);
                $horario->update($input);

                return Redirect::route('horarios.createadm')->with('message1', 'Horário na data <b>'.$input['data'].'</b> alterado com sucesso!');
      
            }
            else
            {
                $this->horario->create($input);
               
                return Redirect::route('horarios.createadm')->with('message1', 'Horário na data <b>'.$input['data'].'</b> criado com sucesso!');
            }
        }

        return Redirect::route('horarios.createadm')
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createmassive()
    {
        $funcionarios   = $this->funcionario->orderBy('nome')->lists('nome','cod');
        $itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');

        $funcionarios   = (array('' => 'Selecione') + $funcionarios);
        $itens_contabil = (array('' => 'Selecione') + $itens_contabil);
        $unidades = (array('' => '') + $unidades);

        return View::make('horarios.createmassive', compact('funcionarios','itens_contabil','unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function savemassive()
    {   
        $campos_ignorados = array('beneficio_tipo_va', 'beneficio_tipo_vt', 'beneficio_tipo_am', 'datafinal');
        
        $input = Input::all(); 

        $validation = Validator::make($input, Horario::$rules);

        $datainicio  = Carbon::createFromFormat('d/m/Y', $input['data']); 
        $datafinal   = Carbon::createFromFormat('d/m/Y', $input['datafinal']); 

        $datashow = Carbon::createFromFormat('d/m/Y', $input['data']); 

        $dias = $datainicio->diffInDays($datafinal);
        
        $input = array_diff_key($input, array_flip($campos_ignorados)); 

        if ($validation->passes())
        {
            for($i=0; $i<=$dias; $i++)
            {
                if($i == 0)
                    $this->horario->create($input);

                else
                {
                    $input['data'] = $datainicio->addDay();

                    $ano = $input['data']->year;

                    $mes = $input['data']->month;
                        if($mes<10)
                            $mes = '0'.$mes;

                    $dia = $input['data']->day;
                        if($dia<10)
                            $dia = '0'.$dia;

                    $input['data'] = $dia.'/'.$mes.'/'.$ano;
                    
                    $this->horario->create($input);
                }
                
            }
            
            return Redirect::route('horarios.criarvarios')
            ->with('message', '<p class="bg-sucess"><b>Horários criados do dia '.$datashow->format('d/m/Y').' a '.$datafinal->format('d/m/Y').' criado com sucesso!</b></p>');
        }

        return Redirect::route('horarios.criarvarios')
        ->withInput()
        ->withErrors($validation)
        ->with('message', '<p class="bg-info"><b>Existem erros de validação no formulário.</b></p>');
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
     public function show($id)
     {
        $horario = $this->horario->with('item_contabil')->findOrFail($id);

        return View::make('horarios.show', compact('horario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $horario = $this->horario->with('item_contabil')->find($id);  
        
        $unidades =  DB::table('tb_sistema')->lists('codlocalizacao','codlocalizacao');
        $unidades = (array('' => '') + $unidades);

        if (is_null($horario))
        {
            return Redirect::route('horarios.index');
        }

        return View::make('horarios.edit', compact('horario','unidades'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');      

        $campos_ignorados = array('beneficio_tipo_va', 'beneficio_tipo_vt', 'beneficio_tipo_am');

        $input = array_diff_key($input, array_flip($campos_ignorados)); 

        $validation = Validator::make($input, Horario::$rules);

        if ($validation->passes())
        {
            $horario = $this->horario->find($id);
            $horario->update($input);

            return Redirect::route('horarios.show', $id);
        }

        return Redirect::route('horarios.edit', $id)
        ->withInput()
        ->withErrors($validation)
        ->with('message', '<p class="bg-info"><b>Existem erros de validação no formulário.</b></p>');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->horario->find($id)->delete();

        return Redirect::route('horarios.index');
    }

    public function checkbox()
    {   
        $id_funcionario = Input::get('cod_funcionario');

        $funcionario = $this->funcionario->find($id_funcionario, array('beneficios'));
        $beneficios = explode(',', $funcionario->beneficios);

        return Response::json($beneficios);
    }

    public function holiday()
    {   
        $unidade = Input::get('cod');

        $feriados =  DB::table('tb_feriado')
        ->where('codlocalizacao', $unidade)
        ->orWhere('codlocalizacao', '')
        ->lists('feriado', 'cod');

        return Response::json($feriados);
    }
   
}//end of horarios controller