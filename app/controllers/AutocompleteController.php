<?php

class AutoCompleteController extends BaseController {


	/**
	 * Funcionario Repository
	 *
	 * @var Funcionario
	 */
	protected $funcionario;

	public function __construct(Funcionario $funcionario)
	{
		$this->funcionario = $funcionario;
	}

	/**
	 * Return functionarios json for autocomplete option
	 * @return string data in json format
	 */
	public function funcionarios()
	{
		$query = Input::get('query');
		
		return $query 
			? $this->funcionario->where('nome', 'LIKE', "%$query%")->select('nome', 'cod')->get()
			: $this->funcionario->select('nome', 'cod')->get();    
	}

}