<?php

use Carbon\Carbon;

class ValidarHorariosController  extends BaseController
{
    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;

    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(Horario $horario, Funcionario $funcionario, ItemContabil $item_contabil)
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $funcionario   = Input::get('filter_funcionario');
        $item_contabil = Input::get('filter_item_contab');
        $dia           = Input::get('filter_dia');

        $permissao_validacao = DB::table('tb_validacao')->where('usuario_id', Auth::user()->cod)->first();

        if(!isset($permissao_validacao))
            return View::make('errovalidacao');

        $funcionarios_validacao = explode(',', $permissao_validacao->funcionarios_permissao);

        if(Auth::user()->cod == 10)
            $filter_funcionarios = $this->funcionario->orderBy('nome')->whereNull('deleted_at')->lists('nome', 'cod');
        else
            $filter_funcionarios = $this->funcionario->whereIn('cod', $funcionarios_validacao)->orderBy('nome')->lists('nome', 'cod');

        $filter_itens_contabil = $this->item_contabil->where('deleted_at', null)->lists('CTD_DESC01','cod');
        
        if(Auth::user()->cod == 10)
        {
            $horariosQuery = $this->horario->with(array('funcionario', 'item_contabil'))
                        ->where('validado', 0)
                        ->orderby('data', 'ASC');
        }

        elseif($permissao_validacao->tipo == 'Coordenador')
        {

            $horariosQuery = $this->horario->with(array('funcionario', 'item_contabil'))
                        ->whereIn('cod_funcionario', $funcionarios_validacao)
                        ->where('validado', 0)
                        ->orderby('data', 'ASC');
        } 
        
        elseif($permissao_validacao->tipo == 'Gerente')
        {
             $horariosQuery = $this->horario->with(array('funcionario', 'item_contabil'))
                        ->whereIn('cod_funcionario', $funcionarios_validacao)
                        ->where('validado2', 0)
                        ->orderby('data', 'ASC');
        }
       
        if ($funcionario)
        {               
            $horariosQuery->where('cod_funcionario', $funcionario);
        } 

        if ($item_contabil)
        {               
            $horariosQuery->where('cod_item_contab', $item_contabil);
        }
        
        if ($dia)
        {               
            $date = substr(Carbon::createFromFormat('d/m/Y', $dia),0,10);
            $horariosQuery->where('data', $date);
        }
        
        //$allowed = explode(',', Auth::user()->unidade_permissoes);

        $horariosQuery->where('valido', '0')->orderBy('data'); //->whereIn('unidade', $allowed)

        $horarios = $horariosQuery->paginate(100);

        $data = array(
            'horarios'  => $horarios,
            'filter_funcionarios' => (array( 0 =>'Selecione um funcionario') + $filter_funcionarios),
            'filter_itens_contabil' => (array( 0 =>'Selecione um item contábil') + $filter_itens_contabil),
            'funcionario' => $funcionario,
            'item_contabil' => $item_contabil,
            'dia' => $dia,
            'permissao_validacao' => $permissao_validacao,
        );

        return View::make('horarios.validar', $data);
        
    }


    /**
     * Update field validado in tb_horarios and set 1.
     * @return Response
     */
    public function update()
    {
        $cods1 = Input::get('cod1');
        $cods2 = Input::get('cod2');
        $allcods = Input::get('cod');               
        $motivos_input1 = Input::get('motivo1');
        $motivos_input2 = Input::get('motivo2');

        if(Auth::user()->cod == 10)
        {
            //super user da vanessa que valida tudo
            $this->horario->whereIn('cod', $cods1)->update(array(
                                                            'validado' => 1,
                                                            'validado_por' => 10, 
                                                            'motivo' => '',   
                                                            'validado2' => 1,
                                                            'validado_por2' => 10,
                                                            'motivo2' => '',  
                                                            'valido' => 1,   
                                                    ));

            return Redirect::route('horarios.validar')->with('success', 'Marcações validadas com sucesso');
        }

        if(isset($cods1))
        {
            $horarios1 = $this->horario->whereIn('cod', $cods1)->get();

            foreach ($horarios1 as $horario)
            {
                if(Auth::user()->cod != $horario->validado_por2)
                    $horario->where('cod', $horario->cod)
                            ->update(array('validado' => 1,
                                           'motivo' => isset($motivos_input1[$horario->cod]) ? $motivos_input1[$horario->cod] : null,
                                           'validado_por' => Auth::user()->cod));
            }
        }

        if(isset($cods2))
        {
            $horarios2 = $this->horario->whereIn('cod', $cods2)->get();

            foreach ($horarios2 as $horario)
            {   
                if(Auth::user()->cod != $horario->validado_por)
                    $horario->where('cod', $horario->cod)
                            ->update(array('validado2' => 1,
                                           'motivo2' => isset($motivos_input2[$horario->cod]) ? $motivos_input2[$horario->cod] : null,
                                           'validado_por2' => Auth::user()->cod));
            }

        }
        
        $horarios = $this->horario->whereIn('cod', $allcods)->get();

        foreach ($horarios as $horario)
        {  
            if(($horario->validado_por != $horario->validado_por2) and 
                    // $horario->validado_por != null and  verificar!
                    // $horario->validado_por2 != null and
                    $horario->validado == 1 and
                    $horario->validado2 == 1)  
                    $horario->update(array('valido' => 1));
        }       

        return Redirect::route('horarios.validar')->with('success', 'Marcações validadas com sucesso');
    }

    public function create()
    {
        $validadores = DB::table('tb_validacao')->get();
        $usuarios = DB::table('tb_usuario')->whereNull('deleted_at')->lists('nome', 'cod');
        $funcionarios   = $this->funcionario->orderBy('nome')->lists('nome','cod');

        $data = array(
            'validadores' => $validadores,
            'usuarios' => $usuarios,
            'funcionarios' => $funcionarios,
        );

        return View::make('validacao.validadores', $data);
    }

    public function salvarFuncionarios()
    {
        $usuario = Input::get('usuario');
        $func_array = Input::get('funcionarios');

        $funcionarios = implode(',', $func_array);

        DB::table('tb_validacao')->where('usuario_id', $usuario)->update(array('funcionarios_permissao' => $funcionarios));
    }


    public function validartodosadm()
    {
        $funcionarios_adm = $this->funcionario->where('administrativo', 1)->lists('cod');

        $horariosQuery = $this->horario
                        ->whereIn('cod_funcionario', $funcionarios_adm)
                        ->where('valido', 0)->update(array(
                                                            'validado' => 1,
                                                            'validado_por' => 10, 
                                                            'motivo' => '',   
                                                            'validado2' => 1,
                                                            'validado_por2' => 10,
                                                            'motivo2' => '',  
                                                            'valido' => 1,   
                                                            ));


        dd($horariosQuery);
    }
    
}