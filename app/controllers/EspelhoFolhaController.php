<?php

use Carbon\Carbon;

class EspelhoFolhaController extends BaseController
{
    /**
     * Horario Repository
     *
     * @var Horario
     */
    protected $horario;

    /**
     * Horario Folha
     *
     * @var Qualitec\Folha\Horario
     */
    protected $horario_folha;

    /**
     * Funcionario Repository
     *
     * @var Funcionario
     */
    protected $funcionario;

    /**
     * Item contabil Repository
     *
     * @var Item contabil
     */
    protected $item_contabil;

    public function __construct(
        Horario $horario,
        Funcionario $funcionario,
        ItemContabil $item_contabil,
        HorarioFolha $horario_folha
        )
    {
        $this->horario          = $horario;
        $this->funcionario      = $funcionario;
        $this->item_contabil    = $item_contabil;
        $this->horario_folha    = $horario_folha;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $funcionarios = $this->funcionario->lists('nome', 'cod');       
        $periodos     = DB::table('tb_periodo')
                          ->select(DB::raw('concat (mes_referencia," - ",ano) as periodo,cod'))
                          ->orderBy('periodo_inicio')
                          ->lists('periodo','cod');
        
       
        $data = array(
            'funcionarios' => (array( 0 =>'Selecione um funcionario') + $funcionarios),
            'periodos'     => (array( 0 =>'Selecione um período') + $periodos),
            );

        return View::make('espelhofolha.index', $data);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
     public function show()
     {
        $codfunc     = Input::get('funcionario');
        $datainicio  = Input::get('datainicio'); 
        $datafinal   = Input::get('datafinal');
        $codperiodo  = Input::get('periodo');

        if($codperiodo != 0)
        {
            $datainicio = DB::table('tb_periodo')->where('cod', $codperiodo)->pluck('periodo_inicio'); 
            $datafinal  = DB::table('tb_periodo')->where('cod', $codperiodo)->pluck('periodo_fim');              
            $datainicio  = Carbon::createFromFormat('Y-m-d H:i:s', $datainicio); 
            $datafinal   = Carbon::createFromFormat('Y-m-d H:i:s', $datafinal); 
        }
        elseif(($datainicio || $datafinal)!= null)
        {
            $datainicio  = Carbon::createFromFormat('d/m/Y H:i:s', $datainicio.' 00:00:00'); 
            $datafinal   = Carbon::createFromFormat('d/m/Y H:i:s', $datafinal.' 00:00:00'); 
        }
        
        if($codfunc != 0)
        {
            $funcionario = $this->funcionario->find($codfunc);
        }
        $horarios = $this->horario->getPrevisaoFolha($codfunc, $datainicio, $datafinal);
      
        $data =  array( 
            'horarios'    => $horarios,                     
            'datainicio'  => $datainicio,
            'datafinal'   => $datafinal,
            'funcionario' => $funcionario,
            'periodo'     => (($datafinal->diffInDays($datainicio))+1),
        );

    return View::make('espelhofolha.show', compact('data'));
         
    }
    
}