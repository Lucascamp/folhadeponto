<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFuncionariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_funcionario', function(Blueprint $table) {
			$table->increments('cod');
			$table->string('matricula')->index()->unique();
			$table->string('nome');
			$table->string('funcao');
			$table->string('mope');
			$table->string('asnt');
			$table->string('cnen');
			$table->string('ftbs');
			$table->string('snqc');
			$table->string('unidade');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_funcionario');
	}

}
