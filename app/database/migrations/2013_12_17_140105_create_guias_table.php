<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuiasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_go', function(Blueprint $table) {
			$table->increments('cod');
			$table->string('projeto');
			$table->string('proj_localizacao');
			$table->string('equi_func1_mat')->nullable();
			$table->string('equi_func2_mat')->nullable();
			$table->string('equi_func3_mat')->nullable();
			$table->string('equi_func4_mat')->nullable();
			$table->string('equi_inspn2_mat')->nullable();
			$table->timestamps();
			$table->softDeletes();			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_go');
	}

}
