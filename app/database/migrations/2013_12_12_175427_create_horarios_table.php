<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_horario', function(Blueprint $table) {
			
			$table->increments('cod');
			$table->integer('cod_funcionario')->unsigned();	
						
			$table->date('data');

			$table->datetime('hora_entrada');
			$table->datetime('hora_saida');

			$table->datetime('intervalo_inicio');
			$table->datetime('intervalo_fim');

			$table->integer('hora_faturada');
			$table->integer('hora_contratada');
			
			$table->boolean('fechado')->default(0);;

			$table->string('atividade');
			
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('cod_funcionario')
				->references('cod')->on('tb_funcionario')
				->onDelete('restrict')
				->onUpdate('cascade');
		});	
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_horario');
	}

}
