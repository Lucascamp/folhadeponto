<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotFuncionarioGuiaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_funcionario_go', function(Blueprint $table) {
			$table->increments('id');

			$table->integer('funcionario_cod')->unsigned()->index();
			$table->integer('go_cod')->unsigned()->index();

			$table->foreign('funcionario_cod')->references('cod')->on('tb_funcionario')->onDelete('cascade');
			$table->foreign('go_cod')->references('cod')->on('tb_go')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_funcionario_go');
	}

}
