<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBeneficiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_beneficio', function(Blueprint $table) {
			$table->increments('cod');			
			$table->integer('cod_funcionario')->unsigned();
			$table->date('data');
			$table->float('va_vr');
			$table->time('tempo_exposicao');
			$table->float('aux_moradia');
			$table->string('contabilidade_vr');
			$table->float('plano_saude');
			$table->float('plano_saude_dep');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('cod_funcionario')
	      		->references('cod')->on('tb_funcionario')
	      		->onDelete('restrict')
	      		->onUpdate('cascade');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_beneficio');
	}

}
