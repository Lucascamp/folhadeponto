<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCodItemContabToTbHorariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tb_horario', function(Blueprint $table) {
			$table->string('cod_item_contab', 30);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_horario', function(Blueprint $table) {
			$table->dropCollum('cod_item_contab');
		});
	}

}
