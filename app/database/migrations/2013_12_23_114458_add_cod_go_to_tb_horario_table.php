<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCodGoToTbHorarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tb_horario', function(Blueprint $table) {

			$table->integer('cod_go')->unsigned();

			$table->foreign('cod_go')
				->references('cod')->on('tb_go')
				->onDelete('restrict')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_horario', function(Blueprint $table) {
			$table->dropForeign('cod_go');
		});
	}

}
