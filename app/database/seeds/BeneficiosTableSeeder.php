<?php

class BeneficiosTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
	    //DB::table('tb_beneficio')->truncate();

		$beneficios = array(
			array(				
				'cod_funcionario' => '1',
				'data' => '21/12/2013',
				'va_vr' => '50,00',
				'tempo_exposicao' => '04:00:00',
				'aux_moradia' => '50,00',
				'contabilidade_vr' => '50,00',
				'plano_saude' => '50,00',
				'plano_saude_dep' => '50,00',
				),
			array(				
				'cod_funcionario' => '2',
				'data' => '22/12/2013',
				'va_vr' => '50,00',
				'tempo_exposicao' => '04:00:00',
				'aux_moradia' => '50,00',
				'contabilidade_vr' => '50,00',
				'plano_saude' => '50,00',
				'plano_saude_dep' => '50,00',
				),

			array(				
				'cod_funcionario' => '3',
				'data' => '23/12/2013',
				'va_vr' => '50,00',
				'tempo_exposicao' => '04:00:00',
				'aux_moradia' => '50,00',
				'contabilidade_vr' => '50,00',
				'plano_saude' => '50,00',
				'plano_saude_dep' => '50,00',
				),
			array(				
				'cod_funcionario' => '4',
				'data' => '24/12/2013',
				'va_vr' => '50,00',
				'tempo_exposicao' => '04:00:00',
				'aux_moradia' => '50,00',
				'contabilidade_vr' => '50,00',
				'plano_saude' => '50,00',
				'plano_saude_dep' => '50,00',
				),
			array(				
				'cod_funcionario' => '5',
				'data' => '25/12/2013',
				'va_vr' => '50,00',
				'tempo_exposicao' => '04:00:00',
				'aux_moradia' => '50,00',
				'contabilidade_vr' => '50,00',
				'plano_saude' => '50,00',
				'plano_saude_dep' => '50,00',
				)



			);

		// Uncomment the below to run the seeder
		DB::table('tb_beneficio')->insert($beneficios);
	}
}
