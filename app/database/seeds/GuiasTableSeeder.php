<?php

class GuiasTableSeeder extends Seeder {

	public function getRandomMatricula()
	{	
		$func = Funcionario::find(rand(1,20), array('matricula'));
		return $func->matricula;
	}

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		//DB::table('tb_go')->truncate();
		
		$faker = Faker\Factory::create();		

		foreach (range(1, 20) as $index) {

			Guia::create([
				'projeto' 			=> $faker->catchPhrase,
				'proj_localizacao' 	=> $faker->state,
				'equi_func1_mat' 	=> $this->getRandomMatricula(),
				'equi_func2_mat' 	=> $this->getRandomMatricula(),
				'equi_func3_mat' 	=> $this->getRandomMatricula(),
				'equi_func4_mat' 	=> $this->getRandomMatricula(),
				'equi_inspn2_mat' 	=> $this->getRandomMatricula(),
			]);
		}
		
	}

}
