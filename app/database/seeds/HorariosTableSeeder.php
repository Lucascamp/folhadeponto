<?php

class HorariosTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //DB::table('tb_horario')->truncate();

        $faker = Faker\Factory::create();

        foreach (range(1, 20) as $index) {

            $date = date('d/m/Y');
            $hour = \Carbon\Carbon::createFromFormat('H:i:s', $faker->time);

            Horario::create([
                    'cod_funcionario'   => rand(1,20),
                    'data'              => $date,
                    'hora_entrada'      => $hour->format('H:i:s'),

                    'intervalo_inicio'  => $hour->addHours(rand(2,4))->format('H:i:s'),
                    'intervalo_fim'     => $hour->addHours(rand(1,2))->format('H:i:s'),

                    'hora_saida'        => $hour->addHours(rand(5,8))->format('H:i:s'),                                                

                    'hora_faturada'     => $faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12]),
                    'hora_contratada'   => $faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12]),

                    'atividade'         => $faker->randomElement([
                        'Hora Direta',
                        'A Disposição do cliente',
                        'A Disposição',
                        'Domingo',
                        'Falta',
                        'Folga',
                    ]),
            ]);                    
        }

    }

}
