<?php

class FuncionariosTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		//DB::table('tb_funcionario')->truncate();
		
		
		$faker = Faker\Factory::create();
		
		foreach (range(1, 20) as $index) {

			Funcionario::create([
				'matricula' => $faker->randomNumber(5),
				'nome' 		=> $faker->name,
				'funcao' 	=> $faker->company,
				'mope' 		=> $faker->randomNumber(5),
				'asnt' 		=> $faker->randomNumber(5),
				'cnen' 		=> $faker->randomNumber(5),
				'ftbs' 		=> $faker->randomNumber(5),
				'snqc' 		=> $faker->randomNumber(5),
				'unidade' 	=> $faker->stateAbbr,
			]);
		}

	}

}
