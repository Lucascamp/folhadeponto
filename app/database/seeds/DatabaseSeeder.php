<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		$this->call('FuncionariosTableSeeder');
		$this->call('BeneficiosTableSeeder');
		$this->call('GuiasTableSeeder');
		$this->call('HorariosTableSeeder');	
		$this->call('PivotFucionariosGuiasTableSeederTableSeeder');
	}

}