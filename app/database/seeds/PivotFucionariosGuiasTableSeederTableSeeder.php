<?php

class PivotFucionariosGuiasTableSeederTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('pivotfucionariosguias')->truncate();
		
		$pivotfucionariosguias = [];

        foreach (range(1, 20) as $go_cod) {

        	foreach (range(1, 4) as $index) {
	        	
	        	$pivotfucionariosguias[] = [	        		
	        		'funcionario_cod' => rand(1,20),
	        		'go_cod'          => $go_cod,
	        	];

        	}
        }

		// Uncomment the below to run the seeder
		DB::table('tb_funcionario_go')->insert($pivotfucionariosguias);
	}

}