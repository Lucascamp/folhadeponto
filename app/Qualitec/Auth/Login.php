<?php namespace Qualitec\Auth;

/**
 * Class login
 * handles the user's login and logout process
 */
class Login
{
    /**
     * @var object The database connection
     */
    private $db_connection = null;
    /**
     * @var array Collection of error messages
     */
    public $errors = array();
    /**
     * @var array Collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct()
    {
        // create/read session, absolutely necessary
        session_start();
        session_name('rx_session');

        // check the possible login actions:
        // if user tried to log out (happen when user clicks logout button)
        if (isset($_GET["logout"])) {
            $this->doLogout();
        }
        // login via post data (if user just submitted a login form)
        elseif (isset($_POST["login"])) {
            $this->dologinWithPostData();
        }
    }

    /**
     * log in with post data
     */
    private function dologinWithPostData()
    {
        // check login form contents
        if (empty($_POST['login'])) {
            $this->errors[] = "O login deve ser preenchido.";
        } elseif (empty($_POST['senha'])) {
            $this->errors[] = "O password deve ser preenchido.";
        } elseif (!empty($_POST['login']) && !empty($_POST['senha'])) {

            // create a database connection, using the constants from config/db.php (which we loaded in index.php)
            $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to utf8 and check it
            if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;
            }

            // if no connection errors (= working database connection)
            if (!$this->db_connection->connect_errno) {

                // escape the POST stuff
                $login = $this->db_connection->real_escape_string($_POST['login']);
    
                // database query, getting all the info of the selected user (allows login via email address in the
                // username field)
                $sql = "SELECT  *
                        FROM tb_usuario
                        WHERE (login = '" . $login . "' OR email = '" . $login . "')
                            AND deleted_at is null
                        ";

                $result_of_login_check = $this->db_connection->query($sql);                

                // if this user exists
                if ($result_of_login_check && $result_of_login_check->num_rows == 1) {

                    // get result row (as an object)
                    $result_row = $result_of_login_check->fetch_object();

                    // using PHP 5.5's password_verify() function to check if the provided password fits
                    // the hash of that user's password
                    if ($_POST['senha'] == $result_row->senha) {

                        // write user data  as a serialized object into PHP COOKIE (a file on the client)                        
                        $_SESSION['rx_user'] = serialize($result_row);

                    } else {
                        $this->errors[] = "Usuário ou senha incorretos, tente novamente.";
                    }
                } else {
                    $this->errors[] = "Usuário ou senha incorretos, tente novamente.";
                }
            } else {
                $this->errors[] = "Erro na conexão com o banco.";
            }
        }
    }

    /**
     * perform the logout
     */
    
    public function doLogout()
    {
        // delete the session of the user        
        session_destroy();

        // return a little feeedback message
        $this->messages[] = "You have been logged out.";

    }

    /**
     * Return the urser Data as a object
     */
    public function getUserData()
    {
        if (isset($_COOKIE['rx_user']))
            return unserialize($_COOKIE['rx_user']);

        if (isset($_SESSION['rx_user']))
            return unserialize($_SESSION['rx_user']);

        return null;
    }

    /**
     * simply return the current state of the user's login
     * @return boolean user's login status
     */
    public function isUserLoggedIn()
    {
        $user = $this->getUserData();

        if ($user AND $user->status == 'ATIVO') {
            return true;
        }
        // default return
        return false;
    }
}
