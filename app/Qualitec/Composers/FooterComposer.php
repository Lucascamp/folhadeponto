<?php namespace Qualitec\Composers;

class FooterComposer 
{
	
	public function compose($view)
	{
		$view->with('version', \Config::get('app.version'));
		$view->with('codinstalacao', \Config::get('app.codinstalacao'));
	}

}