<?php namespace Qualitec\Composers;

class HeaderComposer 
{
	
	public function compose($view)
	{
		$view->with('version', \Config::get('app.version'));
	}

}