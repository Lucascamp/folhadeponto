<?php 

namespace Qualitec\Html;

use Illuminate\Support\ServiceProvider;

class FormBuilderProvider extends ServiceProvider 
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'formbuilder' instance container to our FormBuilder object
        $this->app->bindShared('qualitec.formbuilder', function($app)
        {
            $form = new Form($app['html'], $app['url'], $app['session.store']->getToken());

            return $form->setSessionStore($app['session.store']);
        });
    }
}