<?php namespace Qualitec\Html;

/**
* Custom Form Fields
*/
class Form extends \Illuminate\Html\FormBuilder
{	

	//we cam costumize our forms by override or adding new functionalitys by here
	
	/**
	 * Create a select box field.
	 *
	 * @param  string  $name
	 * @param  array   $list
	 * @param  string  $selected
	 * @param  array   $options
	 * @return string
	 */
	public function select($name, $list = array(), $selected = null, $options = array())
	{

		$options['class'] =  (isset($options['class']) ? $options['class'] : 'chosen-select form-control');

		return parent::select($name, $list, $selected, $options);
	}

	/**
	 * Create a text input field.
	 *
	 * @param  string  $name
	 * @param  string  $value
	 * @param  array   $options
	 * @return string
	 */
	public function text($name, $value = null, $options = array())
	{
		$options['class'] =  (isset($options['class']) ? $options['class'] : 'form-control');

		return parent::text($name, $value, $options);
	}


	/**
	 * Open up a new HTML form.
	 *
	 * @param  array   $options
	 * @return string
	 */
	public function open(array $options = array())
	{
		$options['role'] =  (isset($options['role']) ? $options['role'] : 'form');

		return parent::open($options);
	}
}