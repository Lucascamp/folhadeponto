<?php 

/**
 * Generate a URL to the RX login sys.
 *
 * @param  string  $name
 * @param  array   $parameters
 * @return string
 */
function rx_url($name = '/', $parameters = array())
{
	return \Config::get('app.urls.rx') . $name;
}

/**
 * Generate a URL to the Modulo Ponto sys.
 *
 * @param  string  $name
 * @param  array   $parameters
 * @return string
 */
function ponto_url($name = '/', $parameters = array())
{
	return \Config::get('app.urls.ponto') . $name;
}

/**
 * Generate the login url.
 *
 * @return string
 */
function rxlogin_url()
{		
	return rx_url(\Config::get('app.login_uri'));
}

/**
 * Generate export url.
 *
 * @return string
 */
function export_folder()
{		
	return Config::get('app.export_folder');
}

