<?php
use Carbon\Carbon;	
/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	\Debugbar::disable();
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	
	if (Auth::check() == false) 
	{
		$login = new Qualitec\Auth\Login();
		$user = $login->getUserData();	
		if($user)
		{
			Auth::loginUsingId($user->cod);
		}
	}
 
	if (Auth::guest()) return Redirect::away('/sistemarx/login.php');
});
/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to(rxlogin_url());
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
|--------------------------------------------------------------------------
| Check Folha
|--------------------------------------------------------------------------
|
| Verify if all the needed fields are filled
|
*/

Route::filter('checkCriarFolha', function()
{
	$input = Input::all(); 
	if(($input['datainicio'] && $input['datafinal'])!=null)
	{
		$datainicio  = Carbon::createFromFormat('d/m/Y H:i:s', $input['datainicio'].' 00:00:00'); 
	    $datafinal   = Carbon::createFromFormat('d/m/Y H:i:s', $input['datafinal'].' 00:00:00'); 
		
		if($datainicio->diffInDays($datafinal) > 32)
		{
			return Redirect::back()->withErrors(['O período deve ser menor que 31 dias'])->withInput();
		}
	}

	if($input['funcionario'] == 0)
    {
        return Redirect::back()->withErrors(['Escolha um funcionário!'])->withInput();
    }
    if(($input['datainicio'] && $input['datafinal'])==null)
    {	
    	if($input['periodo'] == 0)
    	{
    		return Redirect::back()->withErrors(['O periodo deve ser selecionado!'])->withInput();
    	}
    }
});

/*
|--------------------------------------------------------------------------
| Check Periodo
|--------------------------------------------------------------------------
|
| Verify if all the needed fields are filled to create a new period
|
*/

Route::filter('checkCriarPeriodo', function()
{
	$input = Input::all();
	if($input['datainicio']==null)
	{
		return Redirect::back()->withErrors(['O período deve ter uma data inicial'])->withInput();
	}
	if($input['datafinal']==null)
	{
		return Redirect::back()->withErrors(['O período deve ter uma data de termino'])->withInput();
	} 
	if(($input['datainicio'] && $input['datafinal'])!=null)
	{
		$datainicio  = Carbon::createFromFormat('d/m/Y H:i:s', $input['datainicio'].' 00:00:00'); 
	    $datafinal   = Carbon::createFromFormat('d/m/Y H:i:s', $input['datafinal'].' 00:00:00'); 
		
		if($datainicio->diffInDays($datafinal) > 32)
		{
			return Redirect::back()->with('danger', 'O período deve ser menor que 31 dias')->withInput();
	
		}
	}

	$verifica = DB::table('tb_periodo')->where('ano', $input['ano'])->where('mes_referencia', $input['mes'])->get(); 
	
	if($verifica)
	{
		return Redirect::back()->with('danger', 'O período do mês de '.$input['mes'].' de '.$input['ano'].' já existe!')->withInput();
	}
		
});

/*
|--------------------------------------------------------------------------
| Check Editar Periodo
|--------------------------------------------------------------------------
|
| Verify the rules to update a period
|
*/

Route::filter('checkEditarPeriodo', function()
{
	$input = Input::all();
	if($input['datainicio']==null)
	{
		return Redirect::route('periodos')->with('danger', 'O período deve ter uma data inicial')->withInput();
	}
	if($input['datafinal']==null)
	{
		return Redirect::route('periodos')->with('danger', 'O período deve ter uma data de término')->withInput();
	} 
	if(($input['datainicio'] && $input['datafinal'])!=null)
	{
		$datainicio  = Carbon::createFromFormat('d/m/Y H:i:s', $input['datainicio'].' 00:00:00'); 
	    $datafinal   = Carbon::createFromFormat('d/m/Y H:i:s', $input['datafinal'].' 00:00:00'); 
		
		if($datainicio->diffInDays($datafinal) > 32)
		{
			return Redirect::route('periodos')->with('danger', 'Não foi possivel editar, data maior que 31 dias!')->withInput();
		}
	}
});

/*
|--------------------------------------------------------------------------
| Check criar marcação
|--------------------------------------------------------------------------
|
| Verify if allready exists data for that day for that user, and if lunch time is correct
|
*/

Route::filter('checkCriarPonto', function()
{
	$input = Input::all();
	
	if($input['data']!='')
	{
		$data = substr(Carbon::createFromFormat('d/m/Y', $input['data']), 0, 10);

		$verifica = DB::table('tb_horario')
						->where('data', $data)
						->where('cod_funcionario', $input['cod_funcionario'])
						->whereNull('deleted_at')
						->get(); 
		
		if($verifica)
		{
			return Redirect::back()->with('danger', 'Já existe uma marcação na data '.$input['data'].' para este funcionário!')->withInput();
		}

		$periodo = DB::table('tb_periodo')
						->where('fechado', 0)
						->whereNull('deleted_at')
						->get();

		$periodo = reset($periodo);


		

		if(!$periodo)
		{
			$periodo = DB::table('tb_periodo')
						->whereNull('deleted_at')
						->orderBy('cod')
						->first();
		}
		// 	return Redirect::back()->with('danger', 'Não existe período aberto para marcações no momento, Favor verificar com o RH')->withInput();	

		$inicio = substr($periodo->periodo_inicio, 0, 10);
		// $fim = substr($periodo->periodo_fim, 0, 10);

		if($inicio > $data)
			return Redirect::back()->with('message', 'Data inferior ao início do período vigente!')->withInput();	
		// if($fim < $data)
		// 	return Redirect::back()->with('info', 'Data superior ao final do período vigente!')->withInput();	
		
	}
});

/*
|--------------------------------------------------------------------------
| Check criar marcação
|--------------------------------------------------------------------------
|
| Verify if allready exists data for that day for that user, and if lunch time is correct
|
*/

Route::filter('checkMassive', function()
{
	$input = Input::all();
	
	if($input['data']!='')
	{
		$data = substr(Carbon::createFromFormat('d/m/Y', $input['data']), 0, 10);
		$datafinal  = substr(Carbon::createFromFormat('d/m/Y', $input['datafinal']), 0, 10);

		$verifica = DB::table('tb_horario')
						->whereBetween('data', array($data, $datafinal))
						->where('cod_funcionario', $input['cod_funcionario'])
						->whereNull('deleted_at')
						->get(); 
		
		if($verifica)
		{
			return Redirect::back()->with('danger', 'Já existe alguma marcação neste intervalo para este funcionário!')->withInput();
		}

		$periodo = DB::table('tb_periodo')
						->where('fechado', 0)
						->whereNull('deleted_at')
						->get();

		$periodo = reset($periodo);

		if(!$periodo)
		{
			$periodo = DB::table('tb_periodo')
						->whereNull('deleted_at')
						->orderBy('cod')
						->first();
		}
		// 	return Redirect::back()->with('danger', 'Não existe período aberto para marcações no momento, Favor verificar com o RH')->withInput();	


		$inicio = substr($periodo->periodo_inicio, 0, 10);
		// $fim = substr($periodo->periodo_fim, 0, 10);

		if($inicio > $data || $inicio > $datafinal)
			return Redirect::back()->with('info', 'Data inferior ao início do período vigente!')->withInput();	
		// if($fim < $data || $fim < $datafinal)
		// 	return Redirect::back()->with('info', 'Data superior ao final do período vigente!')->withInput();	
		if($data > $datafinal)
			return Redirect::back()->with('info', 'A data inicial deve ser menor que a final!')->withInput();	
		
	}
});


Route::filter('checkPontoJson', function()
{
	$input = Input::all();
	
	if($input['data']!='')
	{
		$data = substr(Carbon::createFromFormat('d/m/Y', $input['data']), 0, 10);

		$verifica = DB::table('tb_horario')
						->where('data', $data)
						->where('cod_funcionario', $input['cod_funcionario'])
						->whereNull('deleted_at')
						->get(); 
		
		if($verifica)
		{
			return Response::json(['exist' => true]);
		}

		$periodo = DB::table('tb_periodo')
						->where('fechado', 0)
						->whereNull('deleted_at')
						->get();

		$periodo = reset($periodo);

		if(!$periodo)
		{
			$periodo = DB::table('tb_periodo')
						->whereNull('deleted_at')
						->orderBy('cod')
						->first();
		}
		// 	return Redirect::back()->with('danger', 'Não existe período aberto para marcações no momento, Favor verificar com o RH')->withInput();	

		$inicio = substr($periodo->periodo_inicio, 0, 10);
		// $fim = substr($periodo->periodo_fim, 0, 10);

		if($inicio > $data)
			return Response::json(['superior' => true]);
		// if($fim < $data)
		// 	return Redirect::back()->with('info', 'Data superior ao final do período vigente!')->withInput();	
		
	}
});



