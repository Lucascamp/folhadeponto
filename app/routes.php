<?php
use Carbon\Carbon;

Route::get('/logout', function(){
	Auth::logout();
	$logout = new Qualitec\Auth\Login();
	$user = $logout->doLogout();

	return Redirect::away('/sistemarx/login.php');
});

Route::group(array('before' => 'auth'), function()
{
	/*Home*/
	Route::get('/', function(){
		return View::make('home.index');
	});

	Route::get('atualizapontos', array('as' => 'atualizapontos', function(){
		Artisan::call('ponto:sync');
		return Redirect::back();
	}));

	Route::get('atualizatempoexposicao', array('as' => 'atualizatempoexposicao', function(){
		Artisan::call('ponto:updateTFE');
		return Redirect::back();
	}));

	/*Datables*/
	Route::get('horarios/data', array('as' => 'datatable.horarios', 'uses' => 'HorariosController@getDatatable'));
	Route::post('horarios/edit', array('as' => 'datatable.edit', 'uses' => 'HorariosController@editCollumn'));
	/*End Datables*/

	/*rotas de exportação para o protheus*/
	Route::get('exportar', 'ExportacaoController@indexExportarProtheus');
	Route::post('exportar', array('as' => 'exportar', 'uses' => 'ExportacaoController@exportacaoProtheus'));

	/*Rotas para validar horarios*/
	Route::get('horarios/validar', 'ValidarHorariosController@index');
	Route::get('validadores/cadastrar', array('as' => 'validadores.cadastrar', 'uses' => 'ValidarHorariosController@cadastrar'));
	Route::post('horarios/validar', array('as' => 'horarios.validar', 'uses' => 'ValidarHorariosController@update'));
	Route::get('horarios/validartodosadm', 'ValidarHorariosController@validartodosadm');

	Route::get('validadores', 'ValidarHorariosController@create');
	Route::get('validadores/cadastrar', array('as' => 'validadores.cadastrar', 'uses' => 'ValidarHorariosController@cadastrar'));
	Route::post('salvarValidadores', 'ValidarHorariosController@salvarFuncionarios');

	/*Rotas para validar horarios*/
	Route::get('horarios/inconsistencia', 'InconsistenciaHorariosController@index');
	Route::post('horarios/inconsistencia', array('as' => 'horarios.inconsistencia', 'uses' => 'InconsistenciaHorariosController@update'));

	/*Rotas para criar periodos de pontos*/
	Route::get('periodos', 'PeriodosController@index');
	Route::post('periodos', array('as' => 'periodos', 'before' => 'checkCriarPeriodo', 'uses' => 'PeriodosController@store'));
	Route::post('periodostatus', array('as' => 'statusperiodo', 'uses' => 'PeriodosController@status'));
	Route::post('periodoedit', array('as' => 'editperiodo', 'uses' => 'PeriodosController@edit'));
	Route::post('periodoupdate', array('as' => 'updateperiodo', 'before' => 'checkEditarPeriodo', 'uses' => 'PeriodosController@update'));
	
	/*Folha de ponto*/
	Route::get('folha', 'FolhaController@index');
	Route::post('folha', array('as' => 'criarfolha', 'before' => 'checkCriarFolha', 'uses' => 'FolhaController@show'));

	/*previsão folha de ponto*/
	Route::get('espelho', 'EspelhoFolhaController@index');
	Route::post('espelho', array('as' => 'espelhofolha', 'before' => 'checkCriarFolha', 'uses' => 'EspelhoFolhaController@show'));

	/*CRUDS*/
	/* datatable / index */
	Route::get('horarios', array('as' => 'horarios.index', 'uses' => 'HorariosController@testDatatable'));

	/* criação de pontos */
	Route::get('horarios/create', array('as' => 'horarios.create', 'uses' => 'HorariosController@create'));
	Route::post('horarios/checkbox', array('uses' => 'HorariosController@checkbox'));
	Route::post('horarios/getdadosmarcacao', array('uses' => 'HorariosController@getDadosMarcacao'));
	Route::post('horarios/holiday', array('uses' => 'HorariosController@holiday'));
	Route::post('horarios', array('as' => 'horarios.store', 'before' => 'checkCriarPonto', 'uses' => 'HorariosController@store'));


	Route::get('horarios/ti', array('as' => 'horarios.ti', 'uses' => 'HorariosController@ti'));

	/*ponto para varios funcionarios*/
	// Route::get('horarios/variosfuncionarios', array('as' => 'horarios.variosfuncionarios', 'uses' => 'HorariosController@createVariosFuncionarios'));
	// Route::post('horarios/variosfuncionarios', array('as' => 'horarios.salvarpontofuncionarios', 'uses' => 'HorariosController@salvarPontoFuncionarios'));
	// Route::post('horarios/getfuncionariosunidade', array('uses' => 'HorariosController@getFuncionariosUnidade'));

	/*ponto para varios funcionarios 2*/
	Route::get('horarios/variosfuncionarios2',array('as' => 'horarios.variosfuncionarios2', 'uses' => 'HorariosController@createVariosFuncionarios2'));
	Route::post('horarios/variosfuncionarios2', array('as' => 'horarios.salvarpontofuncionarios2', 'before' => 'checkPontoJson', 'uses' => 'HorariosController@salvarPontoFuncionarios2'));
	Route::post('horarios/getfuncionariosunidade2', array('uses' => 'HorariosController@getFuncionariosUnidade2'));

	/*ponto administrativo*/
	Route::get('marcacao/administrativo', array('as' => 'horarios.createadm', 'uses' => 'HorariosController@createadm'));
	Route::post('marcacao/administrativo', array('as' => 'horarios.storeadm', 'uses' => 'HorariosController@storeadm'));
	
	/*ponto em um periodo*/
	Route::get('horarios/criarvarios', array('as' => 'horarios.criarvarios', 'uses' => 'HorariosController@createmassive'));
	Route::post('horarios/criarvarios', array('as' => 'horarios.salvarvarios', 'before' => 'checkMassive', 'uses' => 'HorariosController@savemassive'));

	/*edição*/
	Route::get('horarios/{horarios}', array('as' => 'horarios.show', 'uses' => 'HorariosController@show'));
	Route::get('horarios/{horarios}/edit', array('as' => 'horarios.edit', 'uses' => 'HorariosController@edit'));
	Route::put('horarios/{horarios}', array('as' => 'horarios.update', 'uses' => 'HorariosController@update'));
	Route::patch('horarios/{horarios}', array('uses' => 'HorariosController@update'));
	
	/*exclusao*/
	Route::delete('horarios/{horarios}', array('as' => 'horarios.destroy','uses' => 'HorariosController@destroy'));
	Route::get('horarios', array('as' => 'horarios.index', 'uses' => 'HorariosController@testDatatable'));

	/*banco de horas*/
	Route::get('banco',  array('as' => 'banco', 'uses' => 'BancoController@index'));
	Route::post('banco', array('as' => 'banco.store', 'uses' => 'BancoController@store'));

	/*Relatorios*/
	Route::get('relatorios', function(){
		return View::make('relatorios.index');
	});
	Route::get('relatorios/geral', 'RelatoriosController@indexGeral');
	Route::post('relatorios/geral', array('as' => 'relatorios.geral', 'uses' => 'RelatoriosController@relatorioGeral'));
	Route::get('relatorios/empresa', 'RelatoriosController@indexEmpresa');
	Route::post('relatorios/empresa', array('as' => 'relatorios.empresa', 'uses' => 'RelatoriosController@relatorioEmpresa'));
	Route::get('relatorios/observacoes', 'RelatoriosController@indexObs');
	Route::post('relatorios/observacoes', array('as' => 'relatorios.observacoes', 'uses' => 'RelatoriosController@observacoesReport'));
	Route::get('relatorios/pendentes', 'RelatoriosController@indexPendente');
	Route::post('relatorios/pendentes', array('as' => 'relatorios.pendentes', 'uses' => 'RelatoriosController@pendenteReport'));
	Route::get('relatorios/marcacoes', 'RelatoriosController@indexMarcacoes');
	Route::post('relatorios/marcacoes', array('as' => 'relatorios.marcacoes', 'uses' => 'RelatoriosController@marcacoesReport'));
	Route::get('relatorios/todas', 'RelatoriosController@indexTodas');
	Route::post('relatorios/todas', array('as' => 'relatorios.todas', 'uses' => 'RelatoriosController@todas'));

	Route::get('relatorios/marcacoes2', 'RelatoriosController@indexMarcacoes2');
	Route::post('relatorios/marcacoes2', array('as' => 'relatorios.marcacoes2', 'uses' => 'RelatoriosController@marcacoesReport2'));
	Route::post('relatorios/marcacoesgetdatas', 'RelatoriosController@marcacoesgetdatas');

	Route::post('relatorios/marcacoesporunidade', 'RelatoriosController@marcacaoPorUnidade');

	Route::get('relatorios/exportar/{cod}', array('as' => 'relatorios.exportar', 'uses' => 'RelatoriosController@exportar'));

	Route::get('relatorios/administrativo', 'RelatoriosController@administrativo');


	/*Férias*/
	Route::get('ferias', 'FeriasController@index');
	Route::post('ferias', array('as' => 'ferias', 'uses' => 'FeriasController@index'));

//Route::resource('horarios', 'HorariosController');
});

/*Autocomplete*/
Route::group(array('prefix' => 'autocomplete'), function()
{
	Route::get('funcionarios', 'AutocompleteController@funcionarios');
});
