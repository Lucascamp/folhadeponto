<?php

return array(
	'debug' => true,
	'root' => '/',

	'urls' => array (
		'rx' => 'http://localhost:8000',
		'ponto' => 'http://localhost:8000',
	),
	
	'login_uri' => '/rx-login/index.php',
	'export_folder' => '/vagrant/ponto_export'
);